import path from "path";
import { defineConfig } from "vite";
import Vue from "@vitejs/plugin-vue";
import eslintPlugin from "vite-plugin-eslint";
import vueDevTools from "vite-plugin-vue-devtools";

const { DEV_PORT = 3000 } = process.env;

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [Vue(), eslintPlugin(), vueDevTools()],
  build: {
    emptyOutDir: false,
    outDir: path.resolve(__dirname, "generated/libdist"),
    lib: {
      entry: path.resolve(__dirname, "./src/lib/main.ts"),
      name: "@incytestudios/ddv",
      fileName: (format: string) => `ddv.${format}.js`
    },
    rollupOptions: {
      external: ["vue"],
      output: {
        globals: {
          vue: "Vue"
        }
      }
    }
  },
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "src")
    }
  },
  server: {
    port: Number(DEV_PORT)
  }
});
