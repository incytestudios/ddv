/* eslint-env node */
require("@rushstack/eslint-patch/modern-module-resolution");

module.exports = {
  root: true,
  env: {
    node: true
  },
  plugins: ["prettier"],
  extends: [
    "prettier",
    // "plugin:vue/vue3-essential",
    "plugin:vue/vue3-recommended",
    // "plugin:vue/vue3-strongly-recommended",
    "@vue/eslint-config-typescript/recommended",
    "@vue/eslint-config-prettier"
  ],
  overrides: [
    {
      files: ["cypress/e2e/**/*.{cy,spec}.{js,ts,jsx,tsx}"],
      extends: ["plugin:cypress/recommended"]
    }
  ],
  parser: "vue-eslint-parser",
  parserOptions: {
    ecmaVersion: "latest",
    parser: "@typescript-eslint/parser",
    vueFeatures: {
      filter: false,
      interpolationAsNonHTML: false
    }
  },
  rules: {
    "prettier/prettier": [
      "error",
      {
        usePrettierrc: false,
        trailingComma: "none",
        arrowParens: "avoid",
        htmlWhitespaceSensitivity: "ignore"
      }
    ],
    "vue/comma-dangle": ["error", "never"],
    "vue/no-setup-props-destructure": "off",
    "vue/no-multiple-template-root": "error",
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    camelcase: "off",
    "vue/attribute-hyphenation": "off",
    "vue/script-setup-uses-vars": 2,
    "@typescript-eslint/consistent-type-imports": "warn",
    "@typescript-eslint/no-explicit-any": "warn",
    "comma-dangle": ["error", "never"],
    "vue/multi-word-component-names": [
      "error",
      {
        ignores: ["Axis", "Crosshairs", "Home", "Playground"]
      }
    ]
  },
  ignorePatterns: ["src/generated/*.ts"],
  globals: {
    defineProps: "readonly",
    defineEmits: "readonly",
    defineExpose: "readonly",
    withDefaults: "readonly"
  }
};
