import { type Domain, type NumericDomain, isDiscreteDomain } from "./domain";
import type { CurveType } from "./curveInterpolators";
import type { KDTreeNode } from "./KDTree";
import type { BTreeNode } from "./BTree";
import type { AxisTickFormatter, ValueFormatter } from "./formatting";

export type Value = number | string | Date;
/**
 * Thanks, Euclid!
 * Pretty much what you'd expect. Call it a 2D vector or a point, it's 2 numbers in an object.
 */
export interface Point {
  x: Value;
  y: Value;
}

export interface NumericPoint extends Point {
  x: number;
  y: number;
}

export interface DatePoint extends Point {
  x: Date;
  y: number;
}

export interface DiscretePoint extends Point {
  x: string;
  y: number;
}

export const isNumeric = (p: Point): p is NumericPoint =>
  typeof p.x === "number" && typeof p.y === "number";
export const isDatePoint = (p: Point): p is DatePoint =>
  p.x instanceof Date && typeof p.y === "number";
export const isDiscretePoint = (p: Point): p is DiscretePoint =>
  typeof p.x === "string" && typeof p.y === "number";

export const isNumericSeries = (points: Point[]): points is NumericPoint[] =>
  points.every(isNumeric);
export const isDateSeries = (points: Point[]): points is DatePoint[] =>
  points.every(isDatePoint);
export const isDiscreteSeries = (points: Point[]): points is DiscretePoint[] =>
  points.every(isDiscretePoint);

export const valueToNumber = (v: Value, domain?: Domain): number => {
  const numericVal =
    typeof v === "string"
      ? domain && isDiscreteDomain(domain)
        ? domain.values.indexOf(v)
        : 0
      : typeof v === "number"
        ? v
        : v instanceof Date
          ? v.getTime()
          : v;
  return numericVal;
};

export const getUniqueXValues = (points: DiscretePoint[]): string[] =>
  points.map(p => p.x).filter(uniqueFilter);

/**
 * The result of either a `BTree` or `KDTree` search
 */
export type IndexSearchResult = {
  node: KDTreeNode | BTreeNode;
  screenDistanceSq: number;
};

/**
 * Thresholds are used to represent static values or ranges on a particular series.
 * They could be used for safety limits, known good/bad values etc...
 */
export type Threshold = {
  /**
   * If defined, draws near threshold
   */
  label?: string;
  /** color override, accepts html color names and hex values like '#FF9900' */
  color?: string;
  /**
   * value can be a single number which will produce a line, or a `Domain` value representing a range
   */
  value: number | Domain;
};

/**
 * `useChart` returns this object showing the current observable DOM dimensions as well as the fully
 * calculated reserved space. The plotWidth and plotHeight are screen space pixel dimensions of the
 * plot area.
 */
export type Layout = {
  plotWidth: number;
  plotHeight: number;
  elementWidth: number;
  elementHeight: number;
  computedHeight: number;
  margins: Margins;
};

/**
 * Margins represents screen-space pixel units in the four cardinal directions
 */
export type Margins = {
  top: number;
  bottom: number;
  left: number;
  right: number;
};

/**
 * Each `DataSet` represents a single series of 2D data as well as associated meta data. The only required attribute is `points`
 */
export type DataSet = {
  title?: string;
  color?: string;
  points: Point[];
  xAxisFormatter?: AxisTickFormatter;
  yAxisFormatter?: AxisTickFormatter;
  xValueFormatter?: ValueFormatter;
  yValueFormatter?: ValueFormatter;
  xAxisLabel?: string;
  yAxisLabel?: string;
  xDomain?: NumericDomain; // leave undefined for auto
  yDomain?: NumericDomain; // leave undefined for auto
  curveType?: CurveType;
  reverseXAxis?: boolean;
  reverseYAxis?: boolean;
  thresholds?: {
    x?: Threshold[];
    y?: Threshold[];
  };
};

/**
 * The `DDV` component provides a `DOMState` object to all of its children.
 * This object represents the outside world to the internals of ddv.
 */
export type DOMState = {
  /** width of plottable area in pixels */
  plotWidth: number;
  /** with of plottable area in pixels */
  plotHeight: number;
  /** container element width in pixels */
  elementWidth: number;
  /** container-element height in pixels */
  elementHeight: number;
  /** computed height based on elementWidth and aspectRatio */
  computedHeight: number;
};

/**
 * `DDV` components provide a `MouseState` object to all of their children.
 * This object represents the outside world's mouse to the internals of ddv.
 */
export type MouseState = {
  /** mouse coordinates relative to 0,0 as top left of plot area */
  mouse: NumericPoint;
  normalizedMouse: NumericPoint;
  globalMouse: NumericPoint; // mouse coordinates relative to the top left of the document.body
  isHovered: boolean; // if mouse is inside container element (maybe over axis or title)
  isPlotHovered: boolean; // if mouse is inside plot area
};

/**
 * @raw ::: details example space reservation
Here's an example new component that would be rendered inside a DDVChart component
 * ```ts
import type { SpaceReservations } from "@incytestudios/ddv/types";
export default defineComponent({
  setup() {
    // grab the provided reservations
    const spaceReservations = inject("spaceReservations") as SpaceReservations;
    ...
    // register outselves by some stable id, usually this components `key` or `id`
    onMounted(() => spaceReservations[myId] = {
      top: 28,
      left: 0,
      right: 0,
      bottom: 0
    })
    // unregister ourselves when we're being unmounted
    onBeforeUnmounted(() => delete spaceReservations[myId])
    return {
      ...
    }
  }
})
```
 */
export type SpaceReservations = {
  [index: string]: Margins;
};

/**
 * `ScaleFunc<T>` is a generic interface representing scaling a value in 1 range onto it's scaled value in another range. For instance graphing x point `10` from the range `0..100` onto the range `0..200` would yield `20`. ScaleFuncs are generally used for individual dimensions of a plot mapping data points into screen points, one dimension at a time.
 */
export interface ScaleFunc<T> {
  (val: T): T; // eslint-disable-line no-unused-vars
}
export interface ScaleFuncDiff<T_IN, T_OUT> {
  (val: T_IN): T_OUT; // eslint-disable-line no-unused-vars
}

export type LinearScaler = {
  scale: ScaleFuncDiff<Value, number>;
  normalize?: ScaleFuncDiff<Value, number>;
  interpolate?: ScaleFunc<number>;
  invert?: ScaleFunc<number>;
  dereference?: ScaleFuncDiff<number, string>;
  sourceDomain: () => Domain;
};

export type DiscreteScaler = {
  scale: ScaleFunc<number>;
  normalize?: ScaleFunc<number>;
  interpolate?: ScaleFunc<number>;
  // invert?: ScaleFunc<number>;
};

export type CartesianScaler = {
  xScaler: () => LinearScaler;
  yScaler: () => LinearScaler;
  scale: ScaleFuncDiff<Point, NumericPoint>;
};

export type ToolTipSeries = {
  title?: string;
  xLabel?: string;
  yLabel?: string;
  isActive: boolean;
  nearestPoint: IndexSearchResult | null;
  xFormatter: ValueFormatter;
  yFormatter: ValueFormatter;
};

export type ToolTipInfo = ToolTipSeries[];

export const uniqueFilter = (value: unknown, index: number, self: unknown[]) =>
  self.indexOf(value) === index;

export type StackedYVal = {
  value: Value;
  dataSeriesNumber: number;
  pointIndex: number;
};

export type StackedPointsByX = Map<Value, StackedYVal[]>;
export type IdentifiedNumericPoint = NumericPoint & {
  dataSeriesNumber: number;
  pointIndex: number;
  stackIndex: number;
};
export type ProjectedStackedPointsByX = Map<Value, IdentifiedNumericPoint[]>;

export type ProjectedPointPair = {
  dataSeriesNumber: number; // which dataSet did this come from?
  dataPoint: Point; // where is it in data space?
  screenPoint: NumericPoint; // where is it on screen?
};

export type ProjectedDataSet = {
  title?: string;
  xAxisLabel?: string;
  yAxisLabel?: string;
  dataSeriesNumber: number;
  pointsByXValue: ProjectedStackedPointsByX;
  pointPairs: ProjectedPointPair[];
};
