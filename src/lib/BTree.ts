import type { Point, NumericPoint } from "../lib/common";

/**
 * a node in the `BTree` k
 */
export type BTreeNode = {
  /** How many branches down the tree is this leaf? 0 == tree root */
  depth: number;
  /** value of this node in data-space */
  dataPoint: Point;
  /** value of this node in screen-space */
  screenPoint: NumericPoint;
  /** single axis value of screenPoint */
  screenValue: number;
  /** original position of this point in the source data */
  originalIndex: number;
  /** index of the dataSet this point came from */
  dataSeriesNumber: number;
};

/**
 * A 1-dimensional binary tree, used to index data and screenspace points for fast querying
 */
export type BTree = {
  node: BTreeNode;
  left: BTree | null;
  right: BTree | null;
};

/**
 * A search result from a `BTree`, returns the tree node that is closest as well as the squared distance in screen-space
 */
export type BTreeSearchResult = {
  node: BTreeNode;
  screenDistanceSq: number;
};

/**
 * Generic interface for sorting nodes in the `BTree`
 */
export interface BTreeSorter {
  (a: BTreeNode, b: BTreeNode): number; // eslint-disable-line no-unused-vars
}
const defaultSorter: BTreeSorter = (a, b): number =>
  a.screenValue - b.screenValue;

/**
 * Constructs a new `BTree` given an array of unsorted nodes. This function is recursive.
 */
export const buildBTree = (
  /** usually you'd map datapoints to this structure */
  nodes: BTreeNode[],
  /** current recursive depth to build. leave this at 0 unless you're doing something "interesting" */
  depth = 0,
  /** see sorter docs, a function for ranking nodes */
  nodeSorter: BTreeSorter | null = defaultSorter
): BTree | null => {
  if (nodes.length === 0) {
    // nothing here, turn back
    return null;
  } else if (nodes.length === 1) {
    // leaf
    return {
      node: nodes[0],
      left: null,
      right: null
    };
  }
  const sorter = nodeSorter ?? defaultSorter;
  const sortedNodes = nodes.sort(sorter);

  const median = Math.floor(sortedNodes.length / 2);
  const pivot = sortedNodes[median];
  const leftNodes = sortedNodes.slice(0, median);
  const rightNodes = sortedNodes.slice(median + 1);

  return {
    node: pivot,
    left: buildBTree(leftNodes, depth + 1, sorter),
    right: buildBTree(rightNodes, depth + 1, sorter)
  };
};

// export const dumpIndex = (tree: KDTree | null, depth = 0) => {
//   if (depth === 0) console.groupCollapsed("TREE ROOT");
//   if (tree) {
//     console.log("NODE", tree.node.screenPoint);
//     if (tree.left) {
//       console.groupCollapsed("LEFT", depth);
//       dumpIndex(tree.left, depth + 1);
//       console.groupEnd();
//     }
//     if (tree.right) {
//       console.groupCollapsed("RIGHT", depth);
//       dumpIndex(tree.right, depth + 1);
//       console.groupEnd();
//     }
//   }
//   if (depth === 0) console.groupEnd();
// };

export const searchBTree = (
  queryValue: number,
  index: BTree | null,
  maxDistance = 100, // furthest distance in screen space to consider a match
  depth = 0,
  bestMatch: BTreeSearchResult | null = null
): BTreeSearchResult | null => {
  if (!index || !index.node || !queryValue) {
    return null;
  }

  // squared distance from the query to this node's screenpoint
  const distance = Math.abs(queryValue - index.node.screenValue);
  // console.log("-".repeat(depth), "d", Math.sqrt(distance));

  const curValue = index.node.screenValue;

  if (
    !bestMatch ||
    distance < Math.min(bestMatch.screenDistanceSq || maxDistance)
  ) {
    bestMatch = {
      node: index.node,
      screenDistanceSq: distance ** 2
    };
  }

  const newMatch = searchBTree(
    queryValue,
    queryValue < curValue
      ? index.left
      : queryValue > curValue
        ? index.right
        : null,
    curValue,
    depth + 1,
    bestMatch
  );

  if (newMatch && newMatch.screenDistanceSq < distance ** 2) {
    bestMatch = newMatch;
  }

  if (depth === 0) {
    // this is the final result we're returning, make sure it's close enough
    return bestMatch && bestMatch.screenDistanceSq < maxDistance ** 2
      ? bestMatch
      : null;
  } else {
    return bestMatch;
  }
};
