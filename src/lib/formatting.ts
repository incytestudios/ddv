import type { Value } from "./common";
import { useLogger } from "./logger";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("formatting");

/**
 * A function to format axis tick values into string label
 */
export type AxisTickFormatter = (
  /** data-space value of this tick */
  value: Value,
  /** the 0-indexed number for which tick this represents */
  tickIndex?: number
) => string;

/**
 * A function to format values into strings for display
 */
export type ValueFormatter = (value: Value, originalIndex?: number) => string;

// generic locale-based rounded numbers
export const formatNumber = Intl.NumberFormat(undefined, {
  minimumFractionDigits: 0,
  maximumFractionDigits: 2,
  notation: "compact"
}).format;
export const formatDate = Intl.DateTimeFormat(undefined, {
  dateStyle: "short"
}).format;

export const getDefaultValueFormatter = (): ValueFormatter => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  return (val: Value, originalIndex?: number) => {
    if (val instanceof Date) {
      return formatDate(val); // TODO:
    } else if (typeof val === "number") {
      return formatNumber(val);
    }
    return val.toString();
  };
};

export const getDefaultTickFormatter = (): AxisTickFormatter => {
  return (val: Value) => {
    if (val instanceof Date) {
      log("this is a date", val);
      return formatDate(val); // TODO:
      // } else if (typeof val === "string") {
    } else if (typeof val === "number") {
      return formatNumber(val);
    }
    return val.toString();
  };
};
