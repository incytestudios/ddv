import type { Value } from "./common";
import { uniqueFilter } from "./common";
import { useLogger } from "./logger";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("domain");

/**
 * Domains are used to represent a range of values, usually in data-space,
 * but can be used to represent screen-space ranges as well
 *
 * Note even with negative ranges the higher negative value shoudld still
 * be set on `min`. like so... `{min: -822, max: -400}`
 */
export type NumericDomain = {
  min: number;
  max: number;
};

export type DateDomain = {
  min: Date;
  max: Date;
};

export interface DiscreteDomain extends NumericDomain {
  values: string[];
}

export type Domain = NumericDomain | DateDomain | DiscreteDomain;

export const isNumberDomain = (d: Domain): d is NumericDomain =>
  typeof d.min === "number";
export const isDateDomain = (d: Domain): d is DateDomain =>
  d.min instanceof Date;
export const isDiscreteDomain = (d: Domain): d is DiscreteDomain =>
  "values" in d && Array.isArray(d.values);

export const fromValues = (values: string[]): DiscreteDomain => {
  const uniqueValues = values.filter(uniqueFilter);
  return {
    min: 0,
    max: uniqueValues.length ? uniqueValues.length - 1 : 0,
    values: uniqueValues
  };
};
export const toNumericDomain = (d: DateDomain): NumericDomain => ({
  min: d.min.getTime(),
  max: d.max.getTime()
});
export const fromTimeFrame = (vals: [Date, Date]): DateDomain => ({
  min: vals[0],
  max: vals[1]
});

/**
 * returns a new Domain given a range and optional offset.
 * `fromRange(100, 20) == {min: 20, max: 120}`
 */
export const fromRange = (
  /** distance between the minimum and maximum values */
  range: number,
  /** where the range begins (defaults to 0) */
  offset?: number
): NumericDomain => ({
  min: offset || 0,
  max: (offset || 0) + range
});

/**
 * creates a new domain by finding the numeric min and max in a given array of numbers
 *
 * getExtents([1,2,3,-10]) == {min:-10, max: 3}
 */
export const getExtents = (values: number[]): NumericDomain => ({
  min: Math.min(...values),
  max: Math.max(...values)
});

/**
 * like `getExtents` but ensures 0 is part of the resulting range
 */
export const getAbsoluteExtents = (values: number[]): NumericDomain => {
  const domain: NumericDomain = { min: 0, max: 0 };
  const extents = getExtents(values);
  domain.min = extents.min < domain.min ? extents.min : domain.min;
  domain.max = extents.max > domain.max ? extents.max : domain.max;
  return domain;
};

/**
 * gets the distance between a domain's min and max while considering negative values
 */
export const getRange = (domain: Domain) =>
  isDateDomain(domain)
    ? Math.abs(domain.max.getTime() - domain.min.getTime())
    : Math.abs(domain.max - domain.min);

/**
 * scales a domain by a given factor
 */
export const scale = (domain: Domain, offset: number, factor = 1): Domain => {
  const range = getRange(domain);
  // log("range", range);

  const zoomedRange = range * factor;
  const shiftAmount = range - zoomedRange;
  const minShift = shiftAmount * offset;
  const maxShift = shiftAmount * (1 - offset);

  if (isDateDomain(domain)) {
    return {
      min: new Date(domain.min.getTime() - minShift),
      max: new Date(domain.max.getTime() + maxShift)
    };
  }
  const out = {
    min: domain.min - minShift,
    max: domain.max + maxShift,
    ...(isDiscreteDomain(domain) ? { values: domain.values } : {})
  };
  // log("out", out);
  return out;
};

/**
 * returns true if `value` is between a domain's min and max (inclusively of min and max)
 */
export const valueInside = (value: Value, domain: Domain): boolean => {
  return value >= domain.min && value <= domain.max;
};

export const normalize = (value: number | Date, domain: Domain): number => {
  if (value instanceof Date && isDateDomain(domain)) {
    return (value.getTime() - domain.min.getTime()) / getRange(domain);
  } else if (typeof value === "number" && !isDateDomain(domain)) {
    return value - domain.min / getRange(domain);
  }
  return -1;
};
