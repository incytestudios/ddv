import type { NumericPoint } from "./common";
import { plot as CM } from "./catmullRom";

export type CurveType = "straight" | "catmull-rom" | "stair-step";

export const plotStraightLine = (points: NumericPoint[]): string => {
  let path = "";
  points.reduce(
    (_, p) =>
      (path +=
        (path === "" ? "M" : " L") + `${p.x.toFixed(1)} ${p.y.toFixed(1)} `),
    path
  );
  return path;
};

export const plotCatmullRom = (points: NumericPoint[], alpha: 0.5 | 1) =>
  CM(points, alpha);

export const plotStairStep = (points: NumericPoint[], topRatio = 0.5): string =>
  points.reduce((path: string, p: NumericPoint, i: number) => {
    if (i === 0) {
      path += `M ${p.x.toFixed(1)} ${p.y.toFixed(1)} `;
    } else {
      const lastPoint = points[i - 1];
      const xDist = (p.x - lastPoint.x) * topRatio;
      const newX = lastPoint.x + xDist;
      // draw the horizontal line then the vertical line to the next point
      // line from last point to last point's Y, but this point's X
      path += `L ${newX.toFixed(1)} ${lastPoint.y.toFixed(1)}  `;
      // now up to this point's Y
      path += `L ${newX.toFixed(1)} ${p.y.toFixed(1)} `;
      if (i === points.length - 1) {
        // last point
        path += `L ${p.x} ${p.y}`;
      }
    }
    return path;
  }, "");
