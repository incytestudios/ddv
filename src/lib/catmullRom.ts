import type { NumericPoint } from "./common";

const p2s = (p: NumericPoint, decimals = 2): string =>
  `${p.x.toFixed(decimals)}, ${p.y.toFixed(decimals)}`;

export const plot = (points: NumericPoint[], alpha: 0.5 | 1): string => {
  if (points.length < 4) {
    console.warn(
      "DDV: can't fit with fewer than 4 points, was sent",
      points.length
    );
    return "";
  }
  let out = ""; // the output SVG path
  let p0: NumericPoint, p1: NumericPoint, p2: NumericPoint, p3: NumericPoint; // relative points to the algorightm as we move through `points`
  let dLeft: number, dMiddle: number, dRight: number; // distances between the 4 points
  let dLeftA: number, dLeft2A: number; // distances raised to alpha and twice-alpha
  let dMiddleA: number, dMiddle2A: number; // distances raised to alpha and twice-alpha
  let dRightA: number, dRight2A: number; // distances raised to alpha and twice-alpha
  let A: number, B: number, M: number, N: number; // Control point factors per point and dimension
  let bp1: NumericPoint, bp2: NumericPoint; // curve to/from for bezier output
  const len = points.length;

  for (let i = 0; i < len - 1; i++) {
    p0 = i === 0 ? points[0] : points[i - 1];
    p1 = points[i];
    p2 = points[i + 1];
    p3 = i + 2 < len ? points[i + 2] : p2;

    dLeft = Math.sqrt(Math.pow(p0.x - p1.x, 2) + Math.pow(p0.y - p1.y, 2));
    dMiddle = Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    dRight = Math.sqrt(Math.pow(p2.x - p3.x, 2) + Math.pow(p2.y - p3.y, 2));

    dLeftA = Math.pow(dLeft, alpha);
    dLeft2A = Math.pow(dLeft, 2 * alpha);
    dMiddleA = Math.pow(dMiddle, alpha);
    dMiddle2A = Math.pow(dMiddle, 2 * alpha);
    dRight2A = Math.pow(dRight, 2 * alpha);
    dRightA = Math.sqrt(dRight2A);

    A = 2 * dLeft2A + 3 * dLeftA * dMiddleA + dRight2A;
    B = 2 * dRight2A + 3 * dRightA * dMiddleA + dMiddle2A;
    N = 3 * dLeftA * (dLeftA + dMiddleA);
    if (N > 0) {
      N = 1 / N;
    }
    M = 3 * dRightA * (dRightA + dMiddleA);
    if (M > 0) {
      M = 1 / M;
    }

    bp1 = {
      x: (-dMiddle2A * p0.x + A * p1.x + dLeft2A * p2.x) * N,
      y: (-dMiddle2A * p0.y + A * p1.y + dLeft2A * p2.y) * N
    };

    bp2 = {
      x: (dRight2A * p1.x + B * p2.x - dMiddle2A * p3.x) * M,
      y: (dRight2A * p1.y + B * p2.y - dMiddle2A * p3.y) * M
    };

    if (bp1.x === 0 && bp1.y === 0) {
      bp1 = p1;
    }
    if (bp2.x === 0 && bp2.y === 0) {
      bp2 = p2;
    }

    if (i === 0) {
      out += `M${p2s(p1)} `;
    } else if (i === 1) {
      out += `C${p2s(bp1)} ${p2s(bp2)} ${p2s(p2)} `;
    } else {
      out += `S${p2s(bp2)} ${p2s(p2)} `;
    }
  }
  //console.log("Path generated", out)
  return out;
};
