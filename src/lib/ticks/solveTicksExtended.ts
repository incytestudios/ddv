import type { Tick, TickSolution } from ".";
import type { LinearScaler } from "../common";
import type { NumericDomain } from "../domain";
import { getRange } from "../domain";
import type { AxisTickFormatter } from "../formatting";
import { niceNum } from "../nicenum";
import { findBestSize } from "./legibility";

const flooredMod = (a: number, n: number) => a - n * Math.floor(a / n);

const getDensity = (r: number, rt: number) => 2 - Math.max(r / rt, rt / r);

const getMaxDensity = (r: number, rt: number) => (r >= rt ? 2 - r / rt : 1);

const getCoverage = (dmin: number, dmax: number, lmin: number, lmax: number) =>
  1 -
  (0.5 * (Math.pow(dmax - lmax, 2) + Math.pow(dmin - lmin, 2))) /
    (0.1 * (dmax - dmin) * (0.1 * (dmax - dmin)));

const getMaxCoverage = (dmin: number, dmax: number, span: number) => {
  const range = dmax - dmin;
  if (span > range) {
    const half = (span - range) / 2;
    return (
      1 -
      0.5 *
        ((half * half + half * half) /
          (0.1 * (dmax - dmin) * (0.1 * (dmax - dmin))))
    );
  } else {
    return 1;
  }
};

const getSimplicity = (
  q: number,
  Q: number[],
  j: number,
  lmin: number,
  lmax: number,
  lstep: number
) => {
  const eps = 1e-10;
  const n = Q.length;
  const i = Q.indexOf(q) + 1;
  const v = flooredMod(lmin, lstep) < eps && lmin <= 0 && lmax >= 0 ? 1 : 0;
  if (n <= 1) {
    return 1 - j + v;
  } else {
    return 1 - (i - 1) / (n - 1) - j + v;
  }
};

const getMaxSimplicity = (q: number, Q: number[], j: number) => {
  const n = Q.length;
  const i = Q.indexOf(q) + 1;
  const v = 1;
  if (n === 1) return 1 - j + v;
  else return 1 - (i - 1) / (n - 1) - j + v;
};

export const solveTicksExtended = (
  axisLength: number, // pixels
  density: number, // this comes from new algo
  domain: NumericDomain,
  scaler: LinearScaler,
  isVertical: boolean,
  tickLabeller: AxisTickFormatter,
  measuredFontSizes: [string, number][]
) => {
  // log("solve ticks extended", domain);
  const Q = [1, 5, 2, 2.5, 4, 3];
  // weights should add up to 1.0
  const weights = {
    simplicity: 0.2,
    coverage: 0.25,
    density: 0.5,
    legibility: 0.05
  };
  const weightedScore = (
    simplicity: number,
    coverage: number,
    density: number,
    legibility: number
  ): number =>
    weights.simplicity * simplicity +
    weights.coverage * coverage +
    weights.density +
    density +
    weights.legibility * legibility;

  if (!domain || axisLength < 1 || domain.max === domain.min) {
    return null;
  }

  const range = getRange(domain);
  let best: TickSolution | null = null;
  let currentScore = 0;
  let bestScore = -2;

  let j = 1;
  while (j < Number.MAX_SAFE_INTEGER) {
    for (const q of Q) {
      const maxSimplicity = getMaxSimplicity(q, Q, j);

      currentScore = weightedScore(maxSimplicity, 1, 1, 1);
      if (currentScore < bestScore) {
        j = Number.MAX_SAFE_INTEGER - 1;
        break;
      }

      let k = 2;
      while (k < Number.MAX_SAFE_INTEGER) {
        const maxDensity = getMaxDensity(k / axisLength, density);
        currentScore = weightedScore(maxSimplicity, 1, maxDensity, 1);
        if (currentScore < bestScore) {
          break;
        }

        const delta = range / (k + 1) / (j * q);
        let z = Math.ceil(Math.log10(delta));

        while (z < Number.MAX_SAFE_INTEGER) {
          const step = 10 ** z * j * q;
          const maxCoverage = getMaxCoverage(0, range, step * (k - 1));

          currentScore = weightedScore(
            maxSimplicity,
            maxCoverage,
            maxDensity,
            1
          );
          if (currentScore < bestScore) {
            break;
          }

          for (
            let start = Math.floor(range / step - (k - 1)) * j;
            start <= Math.ceil(0 / step) * j;
            start++
          ) {
            const lmin = start * (step / j);
            const lmax = lmin + step * (k - 1);

            const s = getSimplicity(q, Q, j, lmin, lmax, step);
            const d = getDensity(k / axisLength, density);
            const c = getCoverage(0, range, lmin, lmax);

            currentScore = weightedScore(s, c, d, 1);
            if (currentScore < bestScore) {
              continue;
            }

            const labels = Array.from(
              { length: k },
              (_, i) => lmin + i * step
            ).map(
              (value: number, i: number): Pick<Tick, "label" | "value"> => ({
                value: value / range,
                label: tickLabeller(value, i)
              })
            );

            const bestSize = findBestSize(
              labels,
              axisLength,
              measuredFontSizes
            );

            // log("bestSize", bestSize);

            const l = bestSize.score;
            currentScore = weightedScore(s, c, d, l);

            const niceRange = niceNum(range, false);
            const spacing = niceNum(niceRange / k, true);
            const niceDomain: NumericDomain = {
              min: Math.floor(domain.min / spacing) * spacing,
              max: Math.ceil(domain.min / spacing) * spacing + niceRange
            };

            const ticks = Array.from(
              { length: k + 2 },
              (_, i) => lmin + i * step
            ).map(
              (value: number, i: number): Tick => ({
                label: tickLabeller(niceDomain.min + value, i),
                value: niceDomain.min + value,
                scaled: scaler.scale(niceDomain.min + value),
                labelLengthPx:
                  bestSize.fontSize[1] *
                  tickLabeller(niceDomain.min + value, i).length
              })
            );

            const option: TickSolution = {
              niceDomain,
              range: Math.abs(lmax - lmin),
              ticks,
              bestFontSize: bestSize.fontSize[0] || measuredFontSizes[0][0],
              extendedMetrics: {
                density: d,
                coverage: c,
                simplicity: s,
                legibility: l,
                score: currentScore
              }
            };

            if (
              option.extendedMetrics?.score &&
              option.extendedMetrics.score > bestScore
            ) {
              bestScore = option.extendedMetrics.score;
              // option.score = score;
              best = option;
            }
          }

          z = z + 1;
        }
        k = k + 1;
      }
    }
    j = j + 1;
  }
  return best;
};
