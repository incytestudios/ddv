import type { Duration } from "date-fns";
import {
  format,
  eachQuarterOfInterval,
  eachYearOfInterval,
  eachWeekOfInterval,
  add,
  eachDayOfInterval,
  eachMonthOfInterval,
  eachHourOfInterval,
  eachMinuteOfInterval
} from "date-fns";
import { type Tick, type TickSolution } from ".";
import type { LinearScaler } from "../common";
import { type DateDomain } from "../domain";
import { useLogger } from "../logger";
import type { MeasuredCSSLength } from "../utils";
import { findBestSize } from "./legibility";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("solveTicksDateDomain");

export type DurationType = (typeof durations)[number];
export const isDurationType = (val: string): val is DurationType =>
  !!durations.find(d => d === val);

const durations = <const>[
  "minute",
  "hour",
  "day",
  "week",
  "month",
  "quarter",
  "year"
];

const measureDuration = (scaler: LinearScaler, duration: Duration): number => {
  const timestamp = new Date().getTime();
  const scaledNow = scaler.scale(timestamp);
  const scaledAfter = scaler.scale(add(timestamp, duration).getTime());
  return Math.abs(scaledAfter - scaledNow);
};

export const solveTicksDateDomain = (
  axisLength: number, // pixels
  domain: DateDomain,
  scaler: LinearScaler,
  measuredFontSizes: MeasuredCSSLength[]
): TickSolution => {
  // log("solving ticks for dateDomain", domain);
  const scaledSizeOfTimeUnits: Record<DurationType, number> = {
    minute: measureDuration(scaler, { minutes: 1 }),
    hour: measureDuration(scaler, { hours: 1 }),
    day: measureDuration(scaler, { days: 1 }),
    week: measureDuration(scaler, { weeks: 1 }),
    month: measureDuration(scaler, { months: 1 }),
    quarter: measureDuration(scaler, { months: 3 }),
    year: measureDuration(scaler, { years: 1 })
  };
  const interval = {
    start: Math.min(domain.min.getTime(), domain.max.getTime()),
    end: Math.max(domain.min.getTime(), domain.max.getTime())
  };
  // log({ interval, scaledSizeOfTimeUnits });
  const desiredLabelWidth = 60;
  const filteredAndSorted = Object.entries(scaledSizeOfTimeUnits)
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    .filter(([_unit, distance]) => distance >= desiredLabelWidth) // only consider units big enough to show
    .sort(
      (
        a: [string, number],
        b: [string, number] // sort smallest first
      ) => (a[1] < b[1] ? -1 : 1)
    );
  const bestUnit: DurationType | undefined =
    filteredAndSorted.length && isDurationType(filteredAndSorted[0][0])
      ? filteredAndSorted[0][0]
      : undefined;

  if (!bestUnit) {
    // log("no best time unit found", domain);
    return { range: 0, niceDomain: domain };
  }

  // log({ bestUnit });

  let labels: Pick<Tick, "value" | "label">[] = [];
  switch (bestUnit) {
    case "year":
      labels = eachYearOfInterval(interval).map(d => ({
        value: d.getTime(),
        label: format(d, "yyyy")
      }));
      break;
    case "quarter":
      labels = eachQuarterOfInterval(interval).map(d => ({
        value: d.getTime(),
        label: format(d, "QQQ yy")
      }));
      break;
    case "month":
      labels = eachMonthOfInterval(interval).map(d => ({
        value: d.getTime(),
        label: format(d, "MMM yyyy")
      }));
      break;
    case "week":
      labels = eachWeekOfInterval(interval).map(d => ({
        value: d.getTime(),
        label: `${format(d.getTime(), "dd MMM yy")}`
      }));
      break;
    case "day":
      labels = eachDayOfInterval(interval).map(d => ({
        value: d.getTime(),
        label: format(d.getTime(), "dd MMM yy")
      }));
      break;
    case "hour":
      labels = eachHourOfInterval(interval).map(d => ({
        value: d.getTime(),
        label: format(d.getTime(), "hh:mm a")
      }));
      break;
    case "minute":
      labels = eachMinuteOfInterval(interval).map(d => ({
        value: d.getTime(),
        label: format(d.getTime(), "hh:mm a")
      }));
      break;
    default:
      labels = eachDayOfInterval(interval).map(d => ({
        value: d.getTime(),
        label: format(d.getTime(), "do MMM yy")
      }));
  }

  const bestSize = findBestSize(labels, axisLength, measuredFontSizes);

  const ticks: Tick[] = labels.map(l => ({
    ...l,
    scaled: scaler.scale(l.value),
    labelLengthPx: bestSize.fontSize[1] * l.label.length
  }));

  return {
    range: 0,
    bestFontSize: bestSize.fontSize[0] || measuredFontSizes[0][0],
    ticks,
    niceDomain: domain
  };
};
