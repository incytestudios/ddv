import { type Tick } from ".";
import { useLogger } from "../logger";
import type { MeasuredCSSLength } from "../utils";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("legibility");

export const getLegibility = (
  targetFontSize: [string, number],
  currentFontSize: [string, number],
  labels: Pick<Tick, "label">[],
  axisLength: number
): number => {
  const fontSizeScore =
    currentFontSize[0] === targetFontSize[0]
      ? 1 // if we're at the target size, the score is 1
      : currentFontSize[1] < targetFontSize[1] // when we're smaller, adjust score
        ? 0.2 * ((currentFontSize[1] + 1) / targetFontSize[1])
        : -Infinity; // do not go bigger than the target

  const orientation = 1;
  const longestLabelCharacters = labels.reduce(
    (longest, label) =>
      (longest = label.label.length > longest ? label.label.length : longest),
    0
  );
  const overlapRatio =
    (currentFontSize[1] * longestLabelCharacters * labels.length) / axisLength;
  const overlapScore = overlapRatio > 1 ? -Infinity : 1;
  const legibilityScore = (fontSizeScore + orientation + overlapScore) / 3;
  // log("leg", {
  //   fontSize: currentFontSize[0],
  //   legibilityScore,
  //   longestLabelCharacters,
  //   labels,
  //   overlapRatio,
  //   fontSizeScore,
  //   overlapScore,
  // });
  return legibilityScore;
};

export const findBestSize = (
  labels: Pick<Tick, "label">[],
  axisLengthPx: number,
  measuredFontSizes: MeasuredCSSLength[]
): { fontSize: MeasuredCSSLength; score: number } => {
  const defaultSize: { fontSize: MeasuredCSSLength; score: number } = {
    fontSize: ["10px", 9.5],
    score: 0.74
  };
  const fs = measuredFontSizes
    .map(currentSize => ({
      fontSize: currentSize,
      score: getLegibility(
        measuredFontSizes[0],
        currentSize,
        labels,
        axisLengthPx
      )
    }))
    .filter(item => item.score > -Infinity)
    .sort((A, B) => B.score - A.score)
    .shift();
  return fs || defaultSize;
};
