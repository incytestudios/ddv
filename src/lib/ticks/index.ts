import { isDateDomain, type Domain } from "../domain";
import type { LinearScaler, NumericPoint } from "../common";
import { type AxisTickFormatter } from "../formatting";

import { solveTicksExtended } from "./solveTicksExtended";
export { solveTicksExtended } from "./solveTicksExtended";
import { solveTicksDateDomain } from "./solveTicksDateDomain";
export { solveTicksDateDomain } from "./solveTicksDateDomain";
import { useLogger } from "../logger";
import type { MeasuredCSSLength } from "../utils";

//#region Tick
export type Tick = {
  label: string;
  value: number;
  scaled: number;
  labelLengthPx: number;
};
//#endregion Tick

export type PolarTick = Omit<Tick, "scaled"> & {
  scaled: NumericPoint;
};

export type ExtendedMetrics = {
  density: number;
  simplicity: number;
  coverage: number;
  legibility: number;
  score: number;
};

//#region TickSolution
export type TickSolution = {
  ticks?: Tick[];
  niceDomain: Domain;
  range: number;
  bestFontSize?: string;
  extendedMetrics?: ExtendedMetrics;
};
//#endregion TickSolution

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("ticks.ts");

const DEFAULT_TICK_SOLUTION: TickSolution = {
  niceDomain: { min: 0, max: 0 },
  ticks: [],
  range: 0
};

/** Implements the axis labeling routine described in
 *  Talbot, Lin, and Hanrahan. An Extension of Wilkinson’s Algorithm for Positioning Tick Labels on Axes, Infovis 2010.
 */
export const solveTicks = (
  axisLength: number, // pixels
  density: number, // this comes from new algo
  domain: Domain,
  isVertical: boolean,
  tickLabeller: AxisTickFormatter,
  scaler: LinearScaler,
  measuredFontSizes: MeasuredCSSLength[]
): TickSolution => {
  // console.time("solving ticks");
  if (isDateDomain(domain)) {
    const s = solveTicksDateDomain(
      axisLength,
      domain,
      scaler,
      measuredFontSizes
    );
    // console.log("date solution", s);
    // console.timeEnd("solving ticks");
    return s;
  } else {
    const s = solveTicksExtended(
      axisLength,
      density,
      domain,
      scaler,
      isVertical,
      tickLabeller,
      measuredFontSizes
    );
    if (s) {
      // log("extended solution", s);
      // console.timeEnd("solving ticks");
      return s;
    }
  }
  // log("DEFAULT solution", DEFAULT_TICK_SOLUTION);
  // console.timeEnd("solving ticks");
  return DEFAULT_TICK_SOLUTION;
};
