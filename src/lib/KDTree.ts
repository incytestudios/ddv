import type { Point, NumericPoint } from "./common";
import { useLogger } from "./logger";

/**
 * An entry in the KDTree, should probably be made more generic to support
 * indexing more information, as these nodes are returned on queries as well
 */
export type KDTreeNode = {
  depth: number; // depth in the index
  dataPoint: Point; // x and y values in data domain
  screenPoint: NumericPoint; // x andy values in screen domain
  originalIndex: number; // original array position of indexed item
  dataSeriesNumber: number; // original data set this point came from
};

/**
 * Binary space parition of KDTreeNodes which are used to index
 * dataSet points for mouse over detection
 */
export type KDTree = {
  node: KDTreeNode;
  left: KDTree | null;
  right: KDTree | null;
};

/**
 * the result type for `search`, returns the closest Node that matches as well as the squared screen distance to it
 */
export type KDTreeSearchResult = {
  node: KDTreeNode;
  screenDistanceSq: number;
};

interface NodeSorter {
  (a: KDTreeNode, b: KDTreeNode): number; // eslint-disable-line no-unused-vars
}
const defaultXSorter: NodeSorter = (a, b): number =>
  a.screenPoint.x - b.screenPoint.x;
const defaultYSorter: NodeSorter = (a, b): number =>
  a.screenPoint.y - b.screenPoint.y;

export const buildIndex = (
  nodes: KDTreeNode[],
  depth = 0,
  xSorter: NodeSorter | null = defaultXSorter,
  ySorter: NodeSorter | null = defaultYSorter
): KDTree | null => {
  if (nodes.length === 0) {
    // nothing here, turn back
    return null;
  } else if (nodes.length === 1) {
    // leaf
    return {
      node: nodes[0],
      left: null,
      right: null
    };
  }

  const sortX = xSorter || defaultXSorter;
  const sortY = ySorter || defaultYSorter;

  // each depth iterates through the index dimensions
  const useXAxis = depth % 2 === 0;

  // sort the remaining nodes by the current depth's axis
  const sortedNodes = nodes.sort((a, b) =>
    useXAxis ? sortX(a, b) : sortY(a, b)
  );
  // console.log(
  //   "SORTED",
  //   sortedNodes.map(sn => sn.screenPoint)
  // );
  const median = Math.floor(sortedNodes.length / 2);
  const pivot = sortedNodes[median];
  const leftNodes = sortedNodes.slice(0, median);
  const rightNodes = sortedNodes.slice(median + 1);

  // const getAxisValue = (n: KDTreeNode | null) => {
  //   return useXAxis ? n?.screenPoint.x : n?.screenPoint.y;
  // };

  // console.log(
  //   "-".repeat(depth),
  //   useXAxis ? "X" : "Y",
  //   "pivot",
  //   pivot.screenPoint,
  //   "left",
  //   getAxisValue(leftNodes[0]),
  //   "-",
  //   getAxisValue(leftNodes[leftNodes.length - 1]),
  //   "right",
  //   getAxisValue(rightNodes[0]),
  //   "-",
  //   getAxisValue(rightNodes[rightNodes.length - 1])
  // );

  return {
    node: pivot,
    left: buildIndex(leftNodes, depth + 1),
    right: buildIndex(rightNodes, depth + 1)
  };
};

export const dumpIndex = (tree: KDTree | null, depth = 0) => {
  if (depth === 0) console.groupCollapsed("TREE ROOT");
  if (tree) {
    if (tree.left) {
      console.groupCollapsed("LEFT", depth);
      dumpIndex(tree.left, depth + 1);
      console.log(tree.left.node);
      console.groupEnd();
    }
    if (tree.right) {
      console.groupCollapsed("RIGHT", depth);
      dumpIndex(tree.right, depth + 1);
      console.log(tree.right.node);
      console.groupEnd();
    }
  }
  if (depth === 0) {
    console.log("center?");
    console.groupEnd();
  }
};

const distanceSq = (a: NumericPoint, b: NumericPoint): number =>
  Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2);

const axisValue = (p: NumericPoint | undefined | null, depth: number) =>
  p ? (depth % 2 === 0 ? p.x : p.y) : Infinity;

const filteredPoint = (p: NumericPoint | undefined | null): NumericPoint => ({
  x: p?.x || 0,
  y: p?.y || 0
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("KDTree.nearestNeighbor");

export const nearestNeighbor = (
  queryPoint: NumericPoint,
  index: KDTree | null,
  maxDistance = 100, // furthest distance in screen space to consider a match
  depth = 0,
  bestMatch: KDTreeSearchResult | null = null
): KDTreeSearchResult | null => {
  // log("inside nearest neighbor", { depth, dataSeriesNumber });
  if (!index || !index.node || !queryPoint) {
    return null;
  }
  const filteredQuery = filteredPoint(queryPoint);
  const filteredCurrent = filteredPoint(index.node.screenPoint);
  // log("q", filteredQuery, "node", filteredCurrent);

  // squared distance from the query to this node's screenpoint
  const dSq = distanceSq(filteredQuery, filteredCurrent);
  // log("test point distance", Math.sqrt(dSq));
  const isCloseEnough =
    bestMatch && dSq < Math.min(bestMatch.screenDistanceSq || maxDistance ** 2);

  if (!bestMatch || isCloseEnough) {
    bestMatch = {
      node: index.node,
      screenDistanceSq: dSq
    };
  }

  const curVal = axisValue(filteredCurrent, depth);
  const queryVal = axisValue(filteredQuery, depth);

  const newMatch = nearestNeighbor(
    queryPoint,
    queryVal < curVal ? index.left : queryVal > curVal ? index.right : null,
    curVal,
    depth + 1,
    bestMatch
  );

  if (newMatch && newMatch.screenDistanceSq < dSq) {
    bestMatch = newMatch;
  }

  if (depth === 0) {
    // this is the final result we're returning, make sure it's close enough
    if (bestMatch && bestMatch.screenDistanceSq < maxDistance ** 2) {
      // log({
      //   depth,
      //   matchX: bestMatch.node.screenPoint.x,
      //   matchY: bestMatch.node.screenPoint.y,
      //   distance: Math.sqrt(dSq),
      //   d2: Math.sqrt(bestMatch.screenDistanceSq)
      // });
      return bestMatch;
    }
    return null;
  } else {
    return bestMatch;
  }
};
