import { useLogger } from "./logger";
import type { DebuggerEvent } from "vue";

export type MeasuredCSSLength = [cssLength: string, pixels: number];

const log = useLogger("utils");

export const computedDebug = (title: string) => {
  return {
    onTrigger: (e: DebuggerEvent) =>
      log(`${title}:${e.key}`, {
        oldValue: e.oldValue,
        newValue: e.newValue,
        updateType: e.type
      })
  };
};

export const clamp = (val: number, min: number, max: number): number => {
  return Math.min(Math.max(val, min), max);
};

export const measureText = (
  text: string,
  fontSize: string,
  isVertical: boolean
): number => {
  if (typeof document === undefined) return 16 * text.length;
  const span = document.createElement("span");
  span.textContent = text;
  span.classList.add("ddv");
  span.setAttribute("style", `font-size: ${fontSize};`);
  document.body?.appendChild(span);
  // console.log(span);
  const rect = span.getBoundingClientRect();
  document.body.removeChild(span);
  return isVertical ? rect?.height : rect?.width;
};

export const measuredCSSDistance = (distance: string): number => {
  if (typeof document === undefined) return 64;
  const div = document.createElement("div");
  div.classList.add("ddv");
  div.setAttribute("style", `width: ${distance};`);
  document.body?.appendChild(div);
  const rect = div.getBoundingClientRect();
  // console.log(distance, "===", rect.width);
  document.body.removeChild(div);
  return rect.width;
};
