/**
 * niceNum attempts to find numbers close to range that are pleasing to look at and would make
 * good tick labels. This is mainly to cause domains like [0.00125125, 99.99021] to get mapped to
 * more pleasing alternatives like [0, 100] without impacting accuracy of the interpolation
 *
 * @param range size of domain
 * @param round
 * @returns number
 */
export function niceNum(range: number, round: boolean): number {
  let inverted = false;
  if (range <= 0) {
    range = range * -1;
    inverted = true;
  }
  /** exponent of range */
  const exponent = Math.floor(Math.log10(range));
  /** fractional part of range */
  const fraction = range / Math.pow(10, exponent);
  /** nice, rounded fraction */
  let niceFraction: number;

  if (round) {
    if (fraction < 1.5) niceFraction = 1;
    else if (fraction < 3) niceFraction = 2;
    else if (fraction < 7) niceFraction = 5;
    else niceFraction = 10;
  } else {
    if (fraction <= 1) niceFraction = 1;
    else if (fraction < 1.25) niceFraction = 1.25;
    else if (fraction <= 2) niceFraction = 2;
    else if (fraction <= 5) niceFraction = 5;
    else if (fraction <= 5.5) niceFraction = 5.5;
    else if (fraction <= 6) niceFraction = 6;
    else if (fraction <= 8) niceFraction = 8;
    else niceFraction = 10;
  }
  const out = niceFraction * Math.pow(10, exponent);
  return inverted ? out * -1 : out; // TODO: should this always be positive on the way out? Ancient Astronaut Theorists say "yes".
}
