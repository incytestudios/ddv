<script setup lang="ts">
import {
  computed,
  onMounted,
  ref,
  onBeforeUnmount,
  watch,
  toRefs,
  onBeforeMount
} from "vue";
import type { Layout, Margins } from "../common";
import { valueToNumber, type LinearScaler } from "../common";
import { getDefaultTickFormatter, type AxisTickFormatter } from "../formatting";
import { useTicks } from "../composables/useTicks";
import { isDiscreteDomain, type Domain } from "../domain";
import type { Tick } from "../ticks";
import { useLogger } from "../logger";
import type { MeasuredCSSLength } from "../utils";
import { measureText } from "../utils";

const log = useLogger("Axis"); // eslint-disable-line @typescript-eslint/no-unused-vars

export interface AxisProps {
  layout: Layout;
  /**
   * A scaler function that maps a data value to its screen value
   */
  scaler: LinearScaler;
  /**
   * The min and max numbers in data-space representing the min and max of the axis
   */
  domain: Domain;
  /**
   * Sets density of ticks using 1-tick per distance where this prop controls the distance.
   * You can set any valid css length here as a string such as `10px`, `0.5em`, or `30vh`.
   */
  tickFrequency?: string;
  /**
   * If present, draw a label for this axis (uses more screenspace)
   */
  label?: string | null;
  /**
   * Axis components default to drawing horizontally (for an x-axis), set this to make it draw vertically (a y-axis)
   */
  isVertical?: boolean;
  /**
   * Whether to draw on the left or the right of center when vertical only used when `isVertical` is true
   */
  isRight?: boolean;
  /**
   * length in pixels from the axis to the plot (or whatever is next to it), use negative values to reverse padding direction)
   */
  plotMarginPx?: number;
  /**
   * length in pixels of each tick mark on the axis, use negative values to reverse tick direction
   */
  tickLengthPx?: number;
  /**
   * length in pixels from axis label to ticks
   */
  labelMarginPx?: number;
  /**
   * when true draws axis lines in the plot area for each tick mark
   */
  drawGridLines?: boolean;
  /**
   * A method for turning axis tick values into string labels
   */
  axisTickFormatter?: AxisTickFormatter;
  /**
   * How many unlabelled ticks to put between each labelled pair of ticks
   */
  subTicks?: number;

  x?: number;
  y?: number;
}

const props = withDefaults(defineProps<AxisProps>(), {
  label: null,
  isVertical: false,
  isRight: false,
  plotMarginPx: 0,
  tickLengthPx: 8,
  labelMarginPx: 12,
  drawGridLines: false,
  axisTickFormatter: getDefaultTickFormatter(),
  explicitLabels: () => [],
  subTicks: 0,
  tickFrequency: "0",
  x: 0,
  y: 0
});

const emit = defineEmits<{
  (e: "update:space", id: string, margins: Margins): void;
}>();

const {
  domain,
  scaler: scale,
  axisTickFormatter,
  isVertical,
  subTicks,
  tickFrequency
} = toRefs(props);

const axisId = Math.round(Math.random() * 100_000);
const axisLengthPx = computed(() =>
  props.isVertical ? props.layout.plotHeight : props.layout.plotWidth
);

// TODO: prop this list, keep as default
const fontSizes = [
  "var(--axisTickFontSize)", // 🤯 this puts the style on a span to measure our css var
  "18px",
  "16px",
  "12px",
  "11px",
  "10px"
];

const isMounted = ref(false);
const measuredFontSizes = computed<MeasuredCSSLength[]>(() =>
  isMounted.value
    ? fontSizes.map(fs => {
        return [fs, measureText("W", fs, isVertical.value)];
      })
    : []
);

const solution = useTicks(
  axisLengthPx,
  domain,
  tickFrequency,
  scale,
  isVertical,
  measuredFontSizes,
  axisTickFormatter
);

const ticks = computed<Tick[]>(() => {
  let out: Tick[] = [];
  if (isDiscreteDomain(domain.value)) {
    out = domain.value.values.map(
      (v): Tick => ({
        label: v,
        labelLengthPx: 20,
        value: valueToNumber(v, domain.value),
        scaled: props.scaler.scale(valueToNumber(v, domain.value))
      })
    );
  } else if (solution.value?.ticks?.length) {
    out = solution.value.ticks.filter(
      t => t.scaled <= axisLengthPx.value && t.scaled >= 0
    );
  }
  return out;
});

const ticksInSVGOrder = computed(() => {
  const first = ticks.value[0]?.scaled || 0;
  const last = ticks.value[ticks.value.length - 1]?.scaled || 0;
  return first < last; // it's reversed if the first tick's Y point is lower than the last ones
});

const getDY = (tick: Tick, index: number): number => {
  let dy = 0;
  if (props.isVertical) {
    if (index === ticks.value?.length - 1) {
      dy = 4 * (ticksInSVGOrder.value ? -1 : 1);
    } else if (index === 0) {
      dy = 4 * (ticksInSVGOrder.value ? 1 : -1);
    }
  }
  return dy;
};

const longestTickLabelPx = computed(() => {
  const out = ticks.value.reduce(
    (pT, cT) => (cT.labelLengthPx > pT.labelLengthPx ? cT : pT),
    { value: 0, label: "0", labelLengthPx: 0 }
  );
  return out.labelLengthPx;
});

const renderedSize = computed(() => {
  if (!props.layout) return [0, 0];
  let w = props.layout.plotWidth;
  let h = props.layout.plotHeight;
  const assumedAxisLabelHeight = 20;
  if (props.isVertical) {
    w = props.plotMarginPx + props.tickLengthPx + 4 + longestTickLabelPx.value;
    if (props.label) {
      w += props.labelMarginPx + assumedAxisLabelHeight;
    }
  } else {
    h = props.plotMarginPx + props.tickLengthPx + assumedAxisLabelHeight;
    if (props.label) h += props.labelMarginPx + assumedAxisLabelHeight;
  }
  return [w, h];
});

const updateReservation = () => {
  const res = {
    top: 0,
    bottom: props.isVertical ? 0 : renderedSize.value[1],
    left: props.isVertical && !props.isRight ? renderedSize.value[0] : 0,
    right: props.isVertical && props.isRight ? renderedSize.value[0] : 0
  };
  // log("will update space reservation", res);
  emit("update:space", `axis-${axisId}`, res);
};

watch(renderedSize, (newSize, oldSize) => {
  if (!oldSize || newSize[0] !== oldSize[0] || newSize[1] !== oldSize[1]) {
    updateReservation();
  }
});
watch([() => props.isRight, () => props.isVertical], () => updateReservation());

onBeforeMount(() => {
  isMounted.value = true;
});
onMounted(() => {
  updateReservation();
});
onBeforeUnmount(() => {
  // log("before unmount, need to free reservation");
  emit("update:space", `axis-${axisId}`, {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0
  });
});

const subTickOffsets = computed<number[]>(() => {
  let out: number[] = [];
  if (ticks.value?.length >= 2) {
    const startTick = ticks.value[0];
    const nextTick = ticks.value[1];
    if (isFinite(startTick?.scaled) && nextTick?.scaled) {
      const distance = nextTick.scaled - startTick.scaled;
      out = Array.from({ length: subTicks.value }).map(
        (_, i) => (distance / (subTicks.value + 1)) * (i + 1)
      );
    }
  }
  return out;
});

const axisClipPath = computed(() => ({
  x: props.isVertical ? (props.isRight ? 0 : -renderedSize.value[0]) : 0,
  y: props.isVertical ? 0 : props.layout.plotHeight,
  width: renderedSize.value[0],
  height: renderedSize.value[1]
}));

const plotRect = computed(() => ({
  x: props.isVertical
    ? props.isRight
      ? (props.layout.margins.left || 0) - props.x
      : 0
    : 0,
  y: 0,
  width: props.layout.plotWidth,
  height: props.layout.plotHeight
}));
</script>

<template>
  <svg
    v-if="ticks.length && layout"
    :x="x"
    :y="y"
    :class="{ isVertical, isRight }"
    class="axis"
  >
    <defs>
      <clipPath
        v-if="axisClipPath.height && plotRect.height"
        :id="`axis-clip-${axisId}`"
      >
        <rect v-bind="axisClipPath" />
        <rect v-bind="plotRect" />
      </clipPath>
    </defs>
    <g
      :style="{
        clipPath: `url(#axis-clip-${axisId})`
      }"
    >
      <g v-if="drawGridLines" class="gridlines">
        <g
          v-for="(t, i) in ticks"
          :key="`axis-${axisId}-grid-${i}`"
          :class="{ zero: t.value === 0 }"
        >
          <template v-if="isVertical">
            <line
              :x1="(layout.margins.left || 0) - x + layout.plotWidth"
              :x2="(layout.margins.left || 0) - x"
              :y1="t.scaled"
              :y2="t.scaled"
            />
            <line
              v-for="offset of subTickOffsets"
              :key="`subgrid-${i}-${offset}`"
              class="subtick"
              :x1="0"
              :x2="layout.plotWidth * (isRight ? -1 : 1)"
              :y1="t.scaled + offset"
              :y2="t.scaled + offset"
            />
          </template>
          <template v-else>
            <line
              :x1="t.scaled"
              :x2="t.scaled"
              :y1="0"
              :y2="layout.plotHeight"
            />
            <line
              v-for="offset of subTickOffsets"
              :key="`subgrid-${i}-${offset}`"
              class="subtick"
              :x1="t.scaled + offset"
              :x2="t.scaled + offset"
              :y1="0"
              :y2="layout.plotHeight"
            />
          </template>
        </g>
      </g>
      <g class="boundary">
        <line
          v-if="isVertical"
          :x1="isRight ? plotMarginPx : -plotMarginPx"
          :x2="isRight ? plotMarginPx : -plotMarginPx"
          :y1="0"
          :y2="layout.plotHeight"
        />
        <line
          v-else
          :x1="0"
          :x2="layout.plotWidth"
          :y1="layout.plotHeight + plotMarginPx"
          :y2="layout.plotHeight + plotMarginPx"
        />
      </g>
      <g class="ticks">
        <g
          v-for="(t, i) in ticks"
          :key="`axis-${axisId}-tick-${i}`"
          class="tick"
          :class="{
            isVertical,
            isRight,
            isReversed: ticksInSVGOrder,
            first: i === 0,
            last: i === ticks.length - 1
          }"
        >
          <template v-if="isVertical">
            <template v-if="isRight">
              <line
                :x1="plotMarginPx"
                :x2="plotMarginPx + tickLengthPx"
                :y1="t.scaled"
                :y2="t.scaled"
              />
              <line
                v-for="offset of subTickOffsets"
                :key="`subtick-${i}-${offset}`"
                :x1="plotMarginPx"
                :x2="plotMarginPx + tickLengthPx * 0.75"
                :y1="t.scaled + offset"
                :y2="t.scaled + offset"
                class="subtick"
              />
              <text
                :x="
                  plotMarginPx + tickLengthPx + 6 // TODO: make a prop for distance from tick end to label start
                "
                :y="t.scaled"
                :dy="getDY(t, i)"
                :style="`--auto-suggested-font-size: ${solution?.bestFontSize}`"
              >
                {{ t.label }}
              </text>
            </template>
            <template v-else>
              <line
                :x1="0 - plotMarginPx - tickLengthPx"
                :x2="0 - plotMarginPx"
                :y1="t.scaled"
                :y2="t.scaled"
              />
              <line
                v-for="offset of subTickOffsets"
                :key="`subtick-${i}-${offset}`"
                :x1="0 - plotMarginPx - tickLengthPx * 0.75"
                :x2="0 - plotMarginPx"
                :y1="t.scaled + offset"
                :y2="t.scaled + offset"
                class="subtick"
              />
              <text
                :x="0 - plotMarginPx - tickLengthPx - 6"
                :y="t.scaled"
                :dy="getDY(t, i)"
                :style="`--auto-suggested-font-size: ${solution?.bestFontSize}`"
              >
                {{ t.label }}
              </text>
            </template>
          </template>
          <template v-else>
            <line
              :x1="t.scaled"
              :x2="t.scaled"
              :y1="layout.plotHeight + plotMarginPx"
              :y2="layout.plotHeight + plotMarginPx + tickLengthPx"
            />
            <line
              v-for="offset of subTickOffsets"
              :key="`subtick-${i}-${offset}`"
              :x1="t.scaled + offset"
              :x2="t.scaled + offset"
              :y1="layout.plotHeight + plotMarginPx"
              :y2="layout.plotHeight + plotMarginPx + tickLengthPx * 0.75"
              class="subtick"
            />
            <text
              :x="t.scaled"
              :y="
                layout.plotHeight + plotMarginPx + tickLengthPx + 6 // TODO: make 4 a prop
              "
              :style="`--auto-suggested-font-size: ${solution?.bestFontSize}`"
            >
              {{ t.label }}
            </text>
            />
          </template>
        </g>
      </g>
    </g>

    <template v-if="isVertical">
      <template v-if="isRight">
        <text
          class="isVertical isRight axis-label"
          :x="
            plotMarginPx +
            tickLengthPx +
            longestTickLabelPx +
            12 + // TODO: replace with font height?
            labelMarginPx
          "
          :y="3"
        >
          {{ label }}
        </text>
      </template>
      <template v-else>
        <text
          class="isVertical axis-label"
          :x="
            0 - plotMarginPx - tickLengthPx - longestTickLabelPx - labelMarginPx
          "
          :y="3"
        >
          {{ label }}
        </text>
      </template>
    </template>
    <template v-else>
      <text
        :x="0"
        :y="
          layout.plotHeight + plotMarginPx + tickLengthPx + 12 + labelMarginPx
        "
        class="axis-label"
      >
        {{ label }}
      </text>
    </template>
  </svg>
</template>

<style lang="scss" scoped>
.axis {
  overflow: visible;
  shape-rendering: geometricPrecision;

  .boundary {
    line {
      stroke-miterlimit: 2;
      stroke: var(--boundaryStroke);
      stroke-width: var(--boundaryStrokeWidth);
      stroke-linecap: square;
      stroke-linejoin: round;
    }
  }

  .gridlines {
    line {
      stroke: var(--gridStroke);
      stroke-width: var(--gridStrokeWidth);
      stroke-dasharray: var(--gridStrokeDashArray);
      &.subtick {
        stroke: var(--subGridStroke);
        stroke-width: var(--subGridStrokeWidth);
        stroke-dasharray: var(--subGridStrokeDashArray);
      }
    }
    g.zero line:not(.subtick) {
      stroke: var(--gridOriginStroke, var(--gridStroke));
      stroke-dasharray: none;
    }
  }
  .ticks {
    .tick {
      line {
        stroke: var(--tickStroke);
        stroke-width: var(--tickStrokeWidth);
        &.subtick {
          stroke: var(--subTickStroke);
          stroke-width: var(--subTickStrokeWidth);
          //stroke-dasharray: var(--subGridStrokeWidth);
        }
      }
      text {
        stroke: none;
        fill: var(--axisTickFontColor);
        font-family: var(--axisTickFontFamily);
        font-size: var(--auto-suggested-font-size, --axisTickFontSize);
        font-weight: var(--axisTickFontWeight);
        dominant-baseline: hanging;
        text-anchor: middle;
      }

      &.isVertical {
        text {
          text-anchor: end;
          dominant-baseline: middle;
        }
        &.isRight {
          text {
            text-anchor: start;
          }
        }
      }

      &.first {
        &:not(.isVertical) {
          text {
            text-anchor: start;
          }
        }
        line:not(.subtick) {
          stroke: var(--tickStrokeFirst, var(--tickStroke));
        }
      }
      &.last {
        text {
          text-anchor: end;
        }
        line:not(.subtick) {
          stroke: var(--tickStrokeLast, var(--tickStroke));
        }
      }
    }
  }

  .axis-label {
    font-family: var(--axisLabelFontFamily);
    font-size: 13px;
    font-weight: 400;
    fill: var(--axisLabelColor, #333);
    stroke: none;
    dominant-baseline: hanging;
    &.isVertical {
      transform: rotate(90deg);
    }
  }
  &.isVertical {
    .tick text {
      text-anchor: end;
      dominant-baseline: middle;
    }
  }
}
</style>
../composables/useSpaceReservations
