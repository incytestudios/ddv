<script setup lang="ts">
import { useThrottleFn } from "@vueuse/core";
import { computed, ref, toRefs, watch } from "vue";
import type {
  MouseState,
  ToolTipInfo,
  Point,
  Layout,
  ToolTipSeries
} from "../common";

export interface ToolTipProps {
  layout: Layout;
  mouseState: MouseState;
  ignoreX?: boolean;
  ignoreY?: boolean;
  toolTipInfo: ToolTipInfo;
  combineSharedX?: boolean;
  combineSharedY?: boolean;
}

const props = withDefaults(defineProps<ToolTipProps>(), {
  ignoreX: false,
  ignoreY: false
});
const {
  layout,
  mouseState,
  toolTipInfo,
  ignoreX,
  ignoreY,
  combineSharedX,
  combineSharedY
} = toRefs(props);

const anchor = computed(() => {
  if (
    toolTipInfo.value &&
    toolTipInfo.value.length === 1 &&
    (ignoreX.value || ignoreY.value)
  ) {
    return (
      toolTipInfo.value[0].nearestPoint?.node.screenPoint ||
      mouseState.value.globalMouse
    );
  }
  return mouseState.value.globalMouse;
});

const toolTipContent = ref<HTMLDivElement>();

// Store the bounding rectangle of the toolTip content
// and throttle updates for performance
const toolTipBoundingRect = ref(toolTipContent.value?.getBoundingClientRect());
watch(
  mouseState,
  useThrottleFn(() => {
    toolTipBoundingRect.value = toolTipContent.value?.getBoundingClientRect();
  }, 100)
);

// Compute the amount of pixels that the toolTip WOULD be out of bounds
// if positioned in the default position
const toolTipOOB = computed(() => {
  if (mouseState.value.isPlotHovered) {
    if (toolTipBoundingRect.value) {
      return {
        right:
          mouseState.value.globalMouse.x +
          toolTipBoundingRect.value.width -
          window.scrollX -
          window.innerWidth,
        bottom:
          mouseState.value.globalMouse.y +
          toolTipBoundingRect.value.height -
          window.scrollY -
          window.innerHeight
      };
    }
  }
  return {
    right: 0,
    bottom: 0
  };
});

const positionStyle = computed(() => {
  type Attrs = "top" | "bottom" | "left" | "right" | "maxWidth";
  const style = {} as { [key in Attrs]?: string }; // eslint-disable-line no-unused-vars
  const padding = 16;
  const A = anchor.value;
  // TODO: move anchor to page location
  if (mouseState.value.isPlotHovered) {
    if (toolTipOOB.value.right <= 0) {
      // ToolTip in X bounds, achor to the right of the cursor
      style.left = `${A.x + padding}px`;
    } else {
      // ToolTip is out of bounds, anchor to the left of the cursor
      style.right = `${window.innerWidth - A.x + padding}px`;
    }

    if (toolTipOOB.value.bottom <= 0) {
      // ToolTip in Y bounds, achor below the cursor
      style.top = `${A.y + padding}px`;
    } else {
      // ToolTip is out of bounds, achor above the cursor
      style.bottom = `${window.innerHeight - A.y - padding}px`;
    }
  }
  style.maxWidth = `${layout.value.plotWidth}px`;
  return style;
});

type InternalTT = {
  dataSeriesNumber: number;
  seriesTitle: string | undefined;
  xLabel: string;
  yLabel: string;
  dataPoint: Point | null;
  formattedX: string | undefined;
  formattedY: string | undefined;
};

const formattedNearestPoints = computed<InternalTT[]>(() => {
  if (!toolTipInfo.value || toolTipInfo.value.length < 1) return [];

  const out = toolTipInfo.value.reduce(
    (
      tti: InternalTT[],
      ds: ToolTipSeries,
      dataSeriesNumber: number
    ): InternalTT[] => {
      if (ds?.nearestPoint?.node) {
        const dp = ds.nearestPoint?.node.dataPoint || null;
        tti.push({
          dataSeriesNumber,
          seriesTitle: ds.title,
          xLabel: ds.xLabel || "X (auto)",
          yLabel: ds.yLabel || "Y (auto)",
          dataPoint: dp,
          formattedX: dp
            ? ds.xFormatter(dp.x, ds.nearestPoint?.node.originalIndex)
            : undefined,
          formattedY: dp
            ? ds.yFormatter(dp.y, ds.nearestPoint?.node.originalIndex)
            : undefined
        });
      }
      return tti;
    },
    []
  );
  // console.log("formatted points", out);
  return out;
});
const allPointsShareX = computed(() => {
  if (!combineSharedX.value) return false;
  const activeSetsWithNearbyPoints = toolTipInfo.value.filter(
    s => s && s.isActive && s.nearestPoint && s.nearestPoint.node
  );
  return (
    activeSetsWithNearbyPoints.length > 1 &&
    activeSetsWithNearbyPoints.every(
      (_, i, arr) =>
        arr[0].nearestPoint?.node.dataPoint.x ===
        arr[i].nearestPoint?.node.dataPoint.x
    )
  );
});

const allPointsShareY = computed(() => {
  if (!combineSharedY.value) return false;
  const activeSetsWithNearbyPoints = toolTipInfo.value.filter(
    s => s && s.isActive && s.nearestPoint && s.nearestPoint.node
  );
  return (
    activeSetsWithNearbyPoints.length > 1 &&
    activeSetsWithNearbyPoints.every(
      (_, i, arr) =>
        arr[0].nearestPoint?.node.dataPoint.y ===
        arr[i].nearestPoint?.node.dataPoint.y
    )
  );
});

const isEnabled = computed(
  () =>
    props.mouseState.isPlotHovered &&
    props.toolTipInfo &&
    toolTipInfo.value.some(v => v.isActive && v.nearestPoint?.node)
);
</script>

<template>
  <div
    v-show="isEnabled"
    ref="toolTipContent"
    :style="positionStyle"
    class="ddv-tooltip"
  >
    <slot
      name="default"
      :tool-tip-info="toolTipInfo"
      :margins="layout.margins"
      :mouse-state="mouseState"
    >
      <div v-if="allPointsShareX" class="shared-header">
        {{ toolTipInfo[0]?.xLabel }}
        <br />
        <span class="val">
          {{ formattedNearestPoints[0].formattedX || 0 }}
        </span>
      </div>
      <div v-else-if="allPointsShareY" class="shared-header">
        {{ toolTipInfo[0]?.yLabel }}
        <br />
        <span class="val">
          {{ formattedNearestPoints[0].formattedY || 0 }}
        </span>
      </div>

      <div
        v-for="{
          dataSeriesNumber,
          seriesTitle,
          xLabel,
          yLabel,
          dataPoint,
          formattedX,
          formattedY
        } in formattedNearestPoints"
        :key="dataSeriesNumber"
        :data-series-number="dataSeriesNumber"
        class="series"
        :class="{ shared: allPointsShareX }"
      >
        <template v-if="dataPoint">
          <div class="label">{{ seriesTitle }}</div>
          <template v-if="allPointsShareX">
            <div class="coord">
              <div>
                {{ formattedY }}
                <span class="light">{{ yLabel }}</span>
              </div>
            </div>
          </template>
          <template v-else-if="allPointsShareY">
            <div class="coord">
              <div class="x">
                {{ formattedX }}
                <span class="light">{{ xLabel }}</span>
              </div>
              <div class="y"></div>
            </div>
          </template>
          <template v-else>
            <div class="coord">
              <div class="x">
                <span class="light">{{ xLabel }}</span>
                {{ formattedX }}
              </div>
              <div class="y">
                {{ formattedY }}
                <span class="light">{{ yLabel }}</span>
              </div>
            </div>
          </template>
        </template>
      </div>
    </slot>
  </div>
</template>

<style lang="scss" scoped>
.ddv-tooltip {
  @keyframes appearAndStay {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  position: absolute;
  background: var(--tooltipSurface);
  opacity: 0;
  max-width: var(--tooltipWidth);
  animation: appearAndStay 500ms;
  animation-delay: var(--tooltipAppearDelay);
  animation-fill-mode: forwards;
  box-shadow: 0px 2px 3px 0.5px rgba(0, 0, 0, 0.18);
  border: var(--tooltipBorder);
  padding: 0.5em 0.5em;
  font-size: 12px;
  z-index: 1000;
  :slotted(.series) {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: center;
    border-bottom: 1px solid var(--activePlotColor);
    margin: 0.25em 0;

    &.shared {
      .coord {
        div {
          flex-basis: 100%;
          text-align: left;
        }
      }
    }

    .label {
      color: var(--activePlotColor);
      font-family: var(--tooltipLabelFontFamily);
      font-weight: 350;
      margin-bottom: 0.15em;
    }
    .coord {
      min-width: 150px;
      padding: 0.25em;
      display: flex;
      flex-wrap: wrap;
      justify-content: space-between;
      align-items: center;
      flex-basis: 100%;
      color: var(--legendFontColor);
      font-family: var(--tooltipCoordinateFontFamily);
      div {
        flex-basis: 50%;
        .light {
          font-weight: 300;
        }
        &.y {
          text-align: right;
        }
      }
    }
  }

  :slotted(.shared-header) {
    flex-grow: 1;
    width: 100%;
    font-weight: 600;
    background: var(--tooltipLabelFontFamily);
    border-bottom: var(--tooltipBorder);
    .val {
      font-family: var(--tooltipCoordinateFontFamily);
    }
  }
}
</style>
