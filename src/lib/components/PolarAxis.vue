<script setup lang="ts">
import { computed, onMounted, ref, watch, toRefs, onBeforeMount } from "vue";
import type { Layout, Margins, NumericPoint } from "../common";
import { getDefaultTickFormatter, type AxisTickFormatter } from "../formatting";
import type { PolarSystem } from "../composables/usePolarInterpolator";
import { describeArc } from "../composables/usePolarInterpolator";
import { valueInside, type Domain } from "../domain";
import { useTicks } from "../composables/useTicks";
import { type PolarTick } from "../ticks";
import type { MeasuredCSSLength } from "../utils";
import { measureText } from "../utils";
import { useLogger } from "../logger";

export type PolarAxisProps = {
  layout: Layout;
  /**
   * the min and max values being mapped to the rotation axis
   */
  angleDomain: Domain;
  /**
   * the min and max values being mapped to the magnitude axis
   */
  magnitudeDomain: Domain;
  /**
   * must be provided by `usePolarInterpolator`
   */
  polarSystem: PolarSystem;
  // optionals...
  /**
   * DO NOT USE
   */
  angleTickFrequency?: string;
  /**
   * DO NOT USE
   */
  magnitudeTickFrequency?: string;
  /**
   * padding in pixels for axis edge to the plot area (can be negative)
   */
  plotMarginPx?: number;
  /**
   * length in pixels for a tick on an axis edge (can be negative)
   */
  tickLengthPx?: number;
  /**
   * margin in pixels for the tick label from the tick itself
   */
  labelMarginPx?: number;
  /**
   * when true draws grid lines at each tick for the rotational axis (radial lines)
   */
  drawAngleGridLines?: boolean;
  /**
   * when true draws grid lines at each magnitude tick (concentric circles)
   */
  drawMagnitudeGridLines?: boolean;
  /**
   * when true draws labels on each angle tick
   */
  drawAngleLabels?: boolean;
  /**
   * when true draws labels on each magnitude tick
   */
  drawMagnitudeLabels?: boolean;
  /**
   * The function to turn ticks objects into final tick label strings
   */
  axisTickFormatter?: AxisTickFormatter;
};

const props = withDefaults(defineProps<PolarAxisProps>(), {
  angleTickFrequency: "0",
  magnitudeTickFrequency: "0",
  plotMarginPx: 0,
  tickLengthPx: 8,
  labelMarginPx: 12,
  drawAngleGridLines: false,
  drawMagnitudeGridLines: false,
  drawAngleLabels: false,
  drawMagnitudeLabels: false,
  axisTickFormatter: getDefaultTickFormatter()
});

const emit = defineEmits<{
  (e: "update:space", id: string, margins: Margins): void;
}>();

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("PolarAxis");
const {
  angleTickFrequency,
  magnitudeTickFrequency,
  angleDomain,
  magnitudeDomain,
  axisTickFormatter,
  polarSystem: polar
} = toRefs(props);
const axisId = ref(Math.round(Math.random() * 100_000));
const magnitudeLength = computed(
  () => polar.value.radiusMax - polar.value.radiusMin
);

const perimeter = computed(() => polar.value.perimeter);

// TODO: prop this list, keep as default
const fontSizes = [
  "var(--axisTickFontSize)", // 🤯 this puts the style on a span to measure our css var
  "18px",
  "16px",
  "12px",
  "11px",
  "10px"
];

const isMounted = ref(false);
const measuredFontSizes = computed<MeasuredCSSLength[]>(() =>
  isMounted.value
    ? fontSizes.map(fs => {
        return [fs, measureText("W", fs, false)];
      })
    : []
);

const angleSolution = useTicks(
  perimeter,
  angleDomain,
  angleTickFrequency,
  polar.value.angleScaler,
  false,
  measuredFontSizes,
  axisTickFormatter
);

const magnitudeSolution = useTicks(
  magnitudeLength,
  magnitudeDomain,
  magnitudeTickFrequency,
  polar.value.magnitudeScaler,
  false,
  measuredFontSizes,
  axisTickFormatter
);

const angleTicks = computed(() => {
  let out: PolarTick[] = [];
  if (!magnitudeSolution.value || !angleSolution.value) return out;
  if (angleSolution.value.ticks?.length) {
    out = angleSolution.value.ticks.map(t => ({
      ...t,
      scaled: polar.value.cartesianScaler.scale({
        x: t.value,
        y: magnitudeDomain.value.max
      })
    }));
  }
  return out.filter(({ value }) => valueInside(value, angleDomain.value));
});

const magnitudeTicks = computed(() => {
  let out: PolarTick[] = [];
  if (!magnitudeSolution.value || !angleSolution.value) return out;

  if (magnitudeSolution.value.ticks?.length) {
    out = magnitudeSolution.value.ticks.map(t => ({
      ...t,
      scaled: polar.value.cartesianScaler.scale({
        x: angleDomain.value.min,
        y: t.value
      })
    }));
  }
  return out.filter(({ value }) => valueInside(value, magnitudeDomain.value));
});

const vectorToPoint = (
  angleInDegrees: number,
  distance: number
): NumericPoint => {
  const radians = (angleInDegrees * Math.PI) / 180.0;
  return {
    x: distance * Math.sin(radians),
    y: distance * Math.cos(radians)
  };
};

const startAngle = computed<number>(
  () => 180 + 180 * (1 - polar.value.perimeterRatio)
);
const endAngle = computed<number>(
  () => 180 - 180 * (1 - polar.value.perimeterRatio)
);

const bottomLeft = computed(() =>
  polar.value.cartesianScaler.scale({
    x: angleDomain.value.min,
    y: magnitudeDomain.value.min
  })
);
const bottomRight = computed(() =>
  polar.value.cartesianScaler.scale({
    x: angleDomain.value.max,
    y: magnitudeDomain.value.min
  })
);

const borderPath = computed(() => {
  let out: string[] = [];
  if (magnitudeSolution.value) {
    out = out.concat([
      // "move bottom left, draw to top left"
      `M ${bottomLeft.value.x} ${bottomLeft.value.y}`,
      // draw an arc across the top
      describeArc(
        polar.value.center,
        polar.value.magnitudeScaler.scale(magnitudeDomain.value.max),
        startAngle.value,
        endAngle.value,
        true,
        Math.abs(startAngle.value - endAngle.value) < 180
      ),
      // draw to bottom right
      `L ${bottomRight.value.x} ${bottomRight.value.y}`,
      // draw an arc back across the bottom to lower left (notice swapped angles below)
      describeArc(
        polar.value.center,
        polar.value.magnitudeScaler.scale(magnitudeDomain.value.min),
        endAngle.value,
        startAngle.value,
        false,
        Math.abs(startAngle.value - endAngle.value) < 180
      )
    ]);
  }
  return out.join(" ");
});

const updateReservation = () =>
  emit("update:space", `axis-${axisId.value}`, {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  });

const renderedSize = computed(() => {
  return [props.layout.plotWidth, props.layout.computedHeight];
});
watch(renderedSize, (newSize, oldSize) => {
  if (!oldSize || newSize[0] !== oldSize[0] || newSize[1] !== oldSize[1]) {
    updateReservation();
  }
});
onBeforeMount(() => {
  isMounted.value = true;
});
onMounted(() => {
  updateReservation();
});
</script>

<template>
  <svg class="polar-axis">
    <path v-if="polar.perimeterRatio < 1" class="bg" :d="borderPath" />
    <circle
      v-else
      class="bg"
      :cx="polar.center.x"
      :cy="polar.center.y"
      :r="polar.magnitudeScaler.scale(magnitudeDomain.max)"
    />

    <g v-if="drawAngleGridLines" class="gridlines">
      <g
        v-for="(t, i) in angleTicks"
        :key="`axis-${axisId}-anglegrid-${i}`"
        :class="{
          zero: t.value === 0,
          first: i === 0,
          last: i === angleTicks.length - 1
        }"
      >
        <line
          :x1="t.scaled.x"
          :y1="t.scaled.y"
          :x2="
            polar.cartesianScaler.scale({ x: t.value, y: magnitudeDomain.min })
              .x
          "
          :y2="
            polar.cartesianScaler.scale({ x: t.value, y: magnitudeDomain.min })
              .y
          "
        />
      </g>
    </g>
    <g v-if="drawMagnitudeGridLines" class="gridlines">
      <g
        v-for="(t, i) in magnitudeTicks"
        :key="`axis-${axisId}-magnitudegrid-${i}`"
        :class="{
          zero: t.value === 0,
          first: i === 0,
          last: i === magnitudeTicks.length - 1
        }"
      >
        <path
          v-if="polar.perimeterRatio < 1"
          :d="
            `M ${t.scaled.x} ${t.scaled.y}` +
            describeArc(
              polar.center,
              polar.magnitudeScaler.scale(t.value),
              startAngle,
              endAngle,
              true,
              Math.abs(startAngle - endAngle) < 180
            )
          "
        />
        <circle
          v-else
          :cx="polar.center.x"
          :cy="polar.center.y"
          :r="polar.magnitudeScaler.scale(t.value)"
        />
      </g>
    </g>

    <path v-if="polar.perimeterRatio < 1" class="border" :d="borderPath" />
    <circle
      v-else
      class="border"
      :cx="polar.center.x"
      :cy="polar.center.y"
      :r="polar.magnitudeScaler.scale(magnitudeDomain.max)"
    />

    <g v-if="drawAngleLabels" class="angle-ticks">
      <g
        v-for="(t, i) in angleTicks"
        :key="`axis-${axisId}-tick-${i}`"
        class="tick"
        :class="{
          first: i === 0,
          last: i === angleTicks.length - 1
        }"
      >
        <line
          :x1="t.scaled.x"
          :y1="t.scaled.y"
          :x2="
            t.scaled.x +
            vectorToPoint(polar.angleScaler.scale(t.value), tickLengthPx).x
          "
          :y2="
            t.scaled.y -
            vectorToPoint(polar.angleScaler.scale(t.value), tickLengthPx).y
          "
        />
        <text
          :x="
            t.scaled.x +
            vectorToPoint(
              polar.angleScaler.scale(t.value),
              tickLengthPx + labelMarginPx
            ).x
          "
          :y="
            t.scaled.y -
            vectorToPoint(
              polar.angleScaler.scale(t.value),
              tickLengthPx + labelMarginPx
            ).y
          "
          v-text="t.label"
        />
      </g>
    </g>

    <g v-if="drawMagnitudeLabels" class="magnitude-ticks">
      <g
        v-for="(t, i) in magnitudeTicks"
        :key="`axis-${axisId}-tick-${i}`"
        class="tick"
        :class="{
          first: i === 0,
          last: i === magnitudeTicks.length - 1
        }"
      >
        <line
          :x1="t.scaled.x"
          :x2="t.scaled.x + vectorToPoint(startAngle - 90, tickLengthPx).x"
          :y1="t.scaled.y"
          :y2="t.scaled.y - vectorToPoint(startAngle - 90, tickLengthPx).y"
        />
        <text
          :x="
            t.scaled.x +
            vectorToPoint(startAngle - 90, tickLengthPx + labelMarginPx).x
          "
          :y="
            t.scaled.y -
            vectorToPoint(startAngle - 90, tickLengthPx + labelMarginPx).y
          "
          v-text="t.label"
        />
      </g>
    </g>
  </svg>
</template>

<style lang="scss" scoped>
.polar-axis {
  overflow: visible;
  &.alternate {
    .bg {
      fill: var(--plotSurfaceAlternate);
    }
  }

  .debug {
    stroke: magenta;
    stroke-opacity: 0.4;
    fill: none;
  }

  .bg {
    fill: var(--plotSurface);
    stroke: none;
  }
  .border {
    fill: none;
    stroke-miterlimit: 2;
    stroke: var(--boundaryStroke);
    stroke-width: var(--boundaryStrokeWidth);
    stroke-linecap: square;
    stroke-linejoin: round;
  }

  .gridlines {
    g {
      path,
      circle,
      line {
        fill: none;
        stroke: var(--gridStroke);
        stroke-width: var(--gridStrokeWidth);
        stroke-dasharray: var(--gridStrokeDashArray);
      }
    }
  }

  .magnitude-ticks,
  .angle-ticks {
    text {
      stroke: none;
      fill: var(--axisTickFontColor);
      font-family: var(--axisTickFontFamily);
      font-size: var(--axisTickFontSize);
      font-weight: var(--axisTickFontWeight);
      dominant-baseline: middle;
      text-anchor: middle;
    }
  }

  .angle-ticks {
    .tick {
      line {
        stroke: var(--tickStroke);
        stroke-width: var(--tickStrokeWidth);
      }
    }
  }

  .magnitude-ticks {
    .tick {
      line,
      path,
      circle {
        fill: none;
        stroke: var(--tickStroke);
        stroke-width: var(--tickStrokeWidth);
      }
    }
  }

  .axis-label {
    font-family: var(--axisLabelFontFamily);
    font-size: 13px;
    font-weight: 400;
    fill: var(--axisLabelColor, #333);
    stroke: none;
    dominant-baseline: hanging;
    &.isVertical {
      text-anchor: start;
      transform: rotate(90deg);
    }
  }
  &.isVertical {
    .tick text {
      text-anchor: end;
      dominant-baseline: middle;
    }
  }
}
</style>
../composables/useSpaceReservations
