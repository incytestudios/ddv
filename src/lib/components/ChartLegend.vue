<script setup lang="ts">
import { watch, computed, onMounted, ref } from "vue";
import type {
  Layout,
  DataSet,
  ToolTipInfo,
  NumericPoint,
  Margins
} from "../common";
import { getDefaultValueFormatter } from "../formatting";

export interface ChartLegendProps {
  layout: Layout;
  /**
   * You can pass a ChartLegend a v-model of a list of plot ids and the legend can toggle them on and off by adding/removing from this list and emitting the changes
   */
  modelValue: Array<number>;
  /**
   * The dataSets to draw legend entries for
   */
  dataSets: Array<DataSet>;
  /**
   * margin around legend in pixels
   */
  marginPx?: number;
  /**
   * A mapping of data series id to the nearestPoint for that series (if the mouse is close enough to one)
   */
  toolTipInfo?: ToolTipInfo;
}

const props = withDefaults(defineProps<ChartLegendProps>(), {
  marginPx: 10,
  toolTipInfo: () => []
});

const emit = defineEmits<{
  (e: "activate-series", dataSeriesNumber: number): void;
  (e: "deactivate-series", dataSeriesNumber: number): void;
  (e: "update:space", id: string, margins: Margins): void;
}>();

const legendId = Math.round(Math.random() * 100000);

const isVertical = computed(() => {
  return props.layout.elementWidth > 600;
});

const formatter = getDefaultValueFormatter();

// css binding
const maxDataSetWidth = ref("0px");
const width = computed(() => {
  if (isVertical.value) {
    if (props.layout.elementWidth > 1000) {
      return 350;
    } else if (props.layout.elementWidth > 750) {
      return 250;
    }
    return 150;
  } else {
    return props.layout.plotWidth;
  }
});
watch(width, newWidth => {
  if (newWidth >= 220) {
    maxDataSetWidth.value = "100px";
  } else {
    maxDataSetWidth.value = "140px";
  }
});

const height = computed(() => {
  if (isVertical.value) {
    return props.layout.plotHeight;
  } else {
    return 128;
  }
});

const topLeft = computed(
  (): NumericPoint => ({
    x: isVertical.value
      ? props.layout.plotWidth +
        +props.layout.margins.left +
        props.layout.margins.right -
        width.value
      : props.layout.margins.left,
    y: isVertical.value
      ? props.layout.margins.top
      : props.layout.margins.top +
        props.layout.plotHeight +
        props.layout.margins.bottom -
        height.value
  })
);

const updateReservation = () => {
  // console.log(
  //   "setting space reservation for legend",
  //   legendId,
  //   "w",
  //   width.value,
  //   "h",
  //   height.value,
  //   "topLeft",
  //   topLeft.value
  // );
  emit("update:space", `legend-${legendId}`, {
    top: 0,
    bottom: isVertical.value ? 0 : 128,
    left: 0,
    right: isVertical.value ? props.marginPx + width.value : 0
  });
};

watch(topLeft, (newTL, oldTL) => {
  if (!oldTL || newTL.x !== oldTL.x || newTL.y !== oldTL.y) {
    updateReservation();
  }
});
onMounted(updateReservation);
</script>

<template>
  <svg
    v-if="props.dataSets.length && width > 0"
    :id="legendId.toFixed()"
    class="legend"
    :class="{ isVertical }"
  >
    <foreignObject
      :x="topLeft.x"
      :y="topLeft.y"
      :width="width"
      :height="height"
    >
      <div
        class="wrapper"
        :style="`width: ${width}px; height: ${height}px`"
        :class="{ isVertical }"
      >
        <slot name="beforeDatasets" />
        <div class="datasets" :class="{ isVertical }">
          <template
            v-for="(ds, i) in dataSets"
            :key="`legend-${legendId}-dataset-${i}`"
          >
            <slot
              name="perSeries"
              :data-series-number="i"
              :data-set="ds"
              :nearest-point="props.toolTipInfo[i]?.nearestPoint"
              :activate-series="() => emit('activate-series', i)"
              :deactivate-series="() => emit('deactivate-series', i)"
              :series-is-active="modelValue && !modelValue.includes(i)"
            >
              <div class="dataset" :data-series-number="i">
                <template v-if="ds.points.length < 1">
                  <span class="warning">&#x26D4;</span>
                  <s>
                    {{ ds.title || `Series ${i}` }}
                  </s>
                </template>

                <template v-else-if="modelValue !== null">
                  <input
                    :id="`legend-${legendId}-toggle-${i}`"
                    type="checkbox"
                    :checked="!modelValue.includes(i)"
                    @change="
                      modelValue.includes(i)
                        ? emit('activate-series', i)
                        : emit('deactivate-series', i)
                    "
                  />
                  <label :for="`legend-${legendId}-toggle-${i}`">
                    {{ ds.title || `Series ${i} ` }}
                  </label>
                </template>

                <label v-else class="static">
                  {{ ds.title || `Series ${i} ` }}
                </label>
              </div>
            </slot>
          </template>
        </div>
        <slot name="afterDatasets" />

        <div
          v-if="
            dataSets && dataSets.some((ds: DataSet) => ds && ds?.thresholds)
          "
          class="thresholds"
        >
          <slot name="beforeThresholds" />
          <template v-for="(ds, i) in dataSets" :key="`th-${i}`">
            <template v-for="(t, j) in ds.thresholds?.x" :key="`th-${i}-${j}`">
              <div
                v-if="typeof t.value === 'number'"
                class="threshold"
                :style="t.color ? `--manualColor: ${t.color}` : ''"
              >
                <span>{{ t.label }}</span>
                <em>X = {{ t.value }}</em>
              </div>

              <div
                v-else
                class="threshold"
                :style="t.color ? `--manualColor: ${t.color}` : ''"
              >
                <span>{{ t.label }}</span>
                <em>
                  {{ formatter(t.value.min) }} &ltlarr; X &ltlarr;
                  {{ formatter(t.value.max) }}
                </em>
              </div>
            </template>
            <template v-for="(t, j) in ds.thresholds?.y" :key="`th-${i}-${j}`">
              <div
                v-if="typeof t.value === 'number'"
                class="threshold"
                :style="t.color ? `--manualColor: ${t.color}` : ''"
              >
                <span>{{ t.label }}</span>
                <em>Y = {{ t.value }}</em>
              </div>
              <div
                v-else
                class="threshold"
                :style="t.color ? `--manualColor: ${t.color}` : ''"
              >
                <span>{{ t.label }}</span>
                <em>
                  {{ formatter(t.value.min) }} &ltlarr; Y &ltlarr;
                  {{ formatter(t.value.max) }}
                </em>
              </div>
            </template>
          </template>
          <slot name="afterThresholds" />
        </div>
      </div>
    </foreignObject>
  </svg>
</template>

<style lang="scss" scoped>
svg.legend {
  .wrapper {
    display: flex;
    flex-direction: column;
    align-items: stretch;
    justify-content: flex-start;
    overflow-x: hidden;
    overflow-y: auto;

    &:not(.isVertical) {
      :slotted(.dataset) {
        flex-grow: 1;
        width: auto;
        flex-basis: auto;
      }
    }

    &.isVertical {
      justify-content: space-between;
      :slotted(.dataset) {
        width: 100%;
        flex-basis: 100%;
      }
    }
  }

  .warning {
    color: var(--warningColor);
  }
  .datasets {
    margin-right: 0.25em;
    background: var(--legendDataSetsBackground);
    padding: 0.25em;
    border: 1px solid var(--legendBorderColor);
    // box-shadow: $shadow;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;

    :slotted(.dataset) {
      display: flex;
      flex-wrap: nowrap;
      justify-content: flex-start;
      align-items: center;
      margin: 0.25em 0;

      input[type="checkbox"] {
        flex: 0 0 auto;
        accent-color: var(--activePlotColor);
      }
      label {
        flex: 1 1 100%;
        display: inline-block;
        min-height: 0;
        //max-width: v-bind(maxDataSetWidth);
        font-family: var(--legendFontFamily);
        color: var(--manualColor, var(--legendFontColor));
        font-size: var(--legendFontSize);
        font-weight: var(--legendFontWeight);
        user-select: none;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
        text-decoration-color: var(--activePlotColor);

        &:hover {
          text-decoration: underline;
        }
        &.static {
          color: var(--activePlotColor);
        }
      }
    }
  }
  .thresholds {
    margin-top: 0.5em;
    margin-right: 0.25em;
    padding: 0.25em;
    background: var(--legendThresholdsBackground);
    color: var(--legendFontColor);
    font-family: var(--legendFontFamily);
    font-size: var(--legendFontSize);
    font-weight: var(--legendFontWeight);
    // box-shadow: $shadow;

    .threshold {
      text-transform: capitalize;
      font-variant-caps: titling-caps;
      display: flex;
      justify-content: space-between;
      align-items: center;
      padding: 0.25em;

      span {
        color: var(--manualColor, var(--legendFontColor));
      }
      :deep(em) {
        font-weight: 300;
      }
    }
  }
}
</style>
