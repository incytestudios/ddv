import type { ProjectedDataSet } from "./../common";
import type { MaybeRefOrGetter } from "vue";
import { computed, toValue } from "vue";
import type { MouseState, IndexSearchResult } from "../common";
import type { BTree } from "../BTree";
import { buildBTree, searchBTree } from "../BTree";
import type { KDTree } from "../KDTree";
import {
  buildIndex,
  nearestNeighbor
  //dumpIndex
} from "../KDTree";
import { useLogger } from "../logger";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("useBSP");

export type NearestPointByDataSet = (IndexSearchResult | null)[];

export const useBSP = (
  /**
   * The points in data-space over which the BSP index will be built
   */
  projectedDataSets: MaybeRefOrGetter<ProjectedDataSet[]>,
  /**
   * The current reactive mouse state of the chart
   */
  mouseState: MaybeRefOrGetter<MouseState>,
  /**
   * When true, builds a [`BTree`](/types/#btree) over Y axis values, and only searches for nearestPoints on the Y axis
   */
  ignoreXValues: MaybeRefOrGetter<boolean>,
  /**
   * When true, builds a [`BTree`](/types/#btree) over X axis values, and only searches for nearestPoints on the X axis
   */
  ignoreYValues: MaybeRefOrGetter<boolean>
) => {
  /**
   * indexType determines whether to use a 1 or N-dimensional index, if either X or Y is ignored, it will use a btree on the unignored dimension.
   */
  const indexType = computed((): "xy" | "x" | "y" | null => {
    const _ignoreX = toValue(ignoreXValues);
    const _ignoreY = toValue(ignoreYValues);
    if (!_ignoreX && !_ignoreY) {
      return "xy";
    } else if (_ignoreX && _ignoreY) {
      // they want to ignore both?
      // console.log("x and y both ignored, not building an index");
      return null;
    } else if (_ignoreX) {
      return "y";
    } else if (_ignoreY) {
      return "x";
    }
    return null;
  });

  /**
   * bTree is a binary tree index over a single dimension
   */
  const bTree = computed((): BTree | null => {
    let index = null;
    if (indexType.value === "x") {
      index = buildBTree(
        toValue(projectedDataSets).flatMap(
          (v, i) =>
            v.pointPairs.map((pair, j) => ({
              originalIndex: j,
              dataPoint: pair.dataPoint,
              screenValue: pair.screenPoint.x,
              screenPoint: pair.screenPoint,
              depth: 0,
              dataSeriesNumber: i
            })),
          0
        )
      );
    } else if (indexType.value === "y") {
      index = buildBTree(
        toValue(projectedDataSets).flatMap(
          (v, i) =>
            v.pointPairs.map((pair, j) => ({
              originalIndex: j,
              dataPoint: pair.dataPoint,
              screenValue: pair.screenPoint.y,
              screenPoint: pair.screenPoint,
              depth: 0,
              dataSeriesNumber: i
            })),
          0
        )
      );
    }
    return index;
  });

  /**
   * kdTree is a binary space partition (BSP) over X and Y, we make one for each dataSet
   */
  const kdTrees = computed(() =>
    toValue(projectedDataSets).reduce(
      (agg: (KDTree | null)[], ds, dataSeriesNumber) => {
        let index = null;
        if (indexType.value === "xy") {
          index = buildIndex(
            ds.pointPairs.map((pair, j) => ({
              originalIndex: j,
              dataPoint: pair.dataPoint,
              screenPoint: pair.screenPoint,
              depth: 0,
              dataSeriesNumber
            }))
          );
          // dumpIndex(index, 0);
        }
        agg.push(index);
        return agg;
      },
      []
    )
  );

  /**
   * The continously computed nearest point per data series
   */
  const nearestPoints = computed<NearestPointByDataSet>(() => {
    const ms = toValue(mouseState);
    const out: NearestPointByDataSet = toValue(projectedDataSets).reduce(
      (acc: NearestPointByDataSet, ds, dataSeriesNumber) => {
        let nearest: IndexSearchResult | null = null;
        if (!ms.isPlotHovered) {
          acc.push(null);
          return acc;
        }
        // console.time("nearestNeighbor");
        if (indexType.value === "xy" && kdTrees.value[dataSeriesNumber]) {
          nearest = nearestNeighbor(
            ms.mouse,
            kdTrees.value[dataSeriesNumber],
            32,
            0,
            null
          );
        } else if (indexType.value === "x" && bTree.value) {
          nearest = searchBTree(ms.mouse.x, bTree.value, 64);
        } else if (indexType.value === "y" && bTree.value) {
          nearest = searchBTree(ms.mouse.y, bTree.value, 64);
        }
        // console.timeEnd("nearestNeighbor");

        if (nearest?.node?.dataSeriesNumber === dataSeriesNumber) {
          acc.push(nearest);
        } else {
          acc.push(null);
        }
        return acc;
      },
      []
    );
    // log("nearest points", out);
    return out;
  });

  // const nearestPoint = computed((): IndexSearchResult | null => {
  //   const ms = toValue(mouseState);

  //   let out: IndexSearchResult | null = null;
  //   if (!ms.isPlotHovered) {
  //     return out;
  //   }
  //   // console.time("nearestNeighbor");
  //   if (indexType.value === "xy" && kdTree.value) {
  //     out = nearestNeighbor(ms.mouse, kdTree.value, 32, 0, null);
  //   } else if (indexType.value === "x" && bTree.value) {
  //     out = searchBTree(ms.mouse.x, bTree.value, 64);
  //   } else if (indexType.value === "y" && bTree.value) {
  //     out = searchBTree(ms.mouse.y, bTree.value, 64);
  //   }
  //   // console.timeEnd("nearestNeighbor");
  //   return out;
  // });

  // const allDataPoints = toValue(projectedDataSets).flatMap(ds =>
  //   ds.pointPairs.map(pair => pair.dataPoint)
  // );
  // const allScreenPoints = toValue(projectedDataSets).flatMap(ds =>
  //   ds.pointPairs.map(pair => pair.screenPoint)
  // );

  return {
    indexType,
    bTree,
    kdTrees,
    nearestPoints
    // allDataPoints,
    // allScreenPoints
  };
};
