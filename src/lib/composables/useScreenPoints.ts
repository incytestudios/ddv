import type { Point, NumericPoint, CartesianScaler } from "./../common";
import type { MaybeRefOrGetter } from "vue";
import { computed, toValue } from "vue";

export const useScreenPoints = (
  points: MaybeRefOrGetter<Point[]>,
  cartesianScaler: MaybeRefOrGetter<CartesianScaler>,
  offsetPoints?: MaybeRefOrGetter<NumericPoint[]>
) => {
  const screenPoints = computed((): NumericPoint[] => {
    const op = toValue(offsetPoints);
    return toValue(points).map((dataPoint, i) => {
      const offsetPoint = op?.at(i);
      // console.log({ dataPoint, offsetPoint });
      const scaled = toValue(cartesianScaler).scale(dataPoint);
      return {
        x: scaled.x + (offsetPoint?.x || 0),
        y: scaled.y + (offsetPoint?.y || 0)
      };
    });
  });

  return {
    screenPoints
  };
};
