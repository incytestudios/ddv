import type { ComputedRef, MaybeRefOrGetter } from "vue";
import { computed, ref, toValue } from "vue";
import { useLogger } from "../logger";
import type { MaybeElement, MaybeElementRef } from "@vueuse/core";
import { useResizeObserver, useThrottleFn } from "@vueuse/core";
import type { Layout, Margins, MouseState } from "../common";
import type { SpaceReserveFn } from "./useSpaceReservations";
import { useSpaceReservations } from "./useSpaceReservations";
import { useMouseState } from "./useMouseState";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("useChart");

/**
 * ChartSystem holds most of the DOM interface of DDV
 */
export type ChartSystem = {
  layout: ComputedRef<Layout>;
  margins: ComputedRef<Margins>;
  isFullScreen: ComputedRef<boolean>;
  mouseState: ComputedRef<MouseState>;
  reserveSpace: SpaceReserveFn;
};

export type UseChartOptions = {
  resizeThrottleMs?: number;
  isFullscreen?: MaybeRefOrGetter<boolean>;
};
/**
 * useChart rigs a DOM reference for DDV. It binds observers for setting up layout, mouse state, fullscreen controls and space reservations
 * @param domRef a vue ref to the DOM element to wrap
 * @param options optional paramters that control the details of DDV's events
 * @returns
 */
export const useChart = (
  domRef: MaybeElementRef<MaybeElement>,
  options?: UseChartOptions
): ChartSystem => {
  const { spaceReservations, reserveSpace } = useSpaceReservations();

  const elementWidth = ref(0);
  const elementHeight = ref(0);
  // log("setting resize observer, max rate", options?.resizeThrottleMs);
  const throttledSizeUpdater = useThrottleFn(
    entries => {
      if (entries[0]?.contentRect) {
        elementWidth.value = entries[0].contentRect.width;
        elementHeight.value = entries[0].contentRect.height;
      }
    },
    options?.resizeThrottleMs,
    true,
    true,
    true
  );
  // use resize observer vs useMouseInElement as it fires when resizing
  useResizeObserver(domRef, throttledSizeUpdater);

  const margins = computed((): Margins => {
    const out: Margins = {
      top: 0,
      bottom: 0,
      left: 0,
      right: 0
    };
    return Object.values(spaceReservations).reduce(
      (prev, cur): Margins => ({
        top: prev.top + cur.top,
        bottom: prev.bottom + cur.bottom,
        left: prev.left + cur.left,
        right: prev.right + cur.right
      }),
      out
    );
  });

  const isFullScreen = computed(() => !!toValue(options?.isFullscreen));

  const computedHeight = computed(() => {
    // log("computing height, el height:", elementHeight.value);
    let h = elementHeight.value;
    const minHeight = margins.value.top + margins.value.bottom + 32;
    if (h < minHeight) {
      // console.log("space reservations are larger than the plot");
      h += minHeight; // leave at least this much room for a plot
    }
    if (isFullScreen.value) {
      h = screen.availHeight;
    }
    return h;
  });
  // figure out where we're going to draw the actual plots
  const plotWidth = computed(
    () => elementWidth.value - margins.value.left - margins.value.right
  );
  const plotHeight = computed(
    () => computedHeight.value - margins.value.top - margins.value.bottom
  );
  const layout = computed(() => ({
    plotWidth: plotWidth.value,
    plotHeight: plotHeight.value,
    elementWidth: elementWidth.value,
    elementHeight: elementHeight.value,
    computedHeight: computedHeight.value,
    margins: margins.value
  }));

  const mouseState = useMouseState(domRef, layout, margins);

  return {
    layout,
    margins,
    isFullScreen,
    mouseState,
    reserveSpace
  };
};
