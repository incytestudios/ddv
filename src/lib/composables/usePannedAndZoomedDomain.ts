import { valueToNumber } from "./../common";
import {
  getRange,
  isDateDomain,
  scale,
  type Domain,
  isNumberDomain
} from "../domain";
import type { MaybeRefOrGetter } from "vue";
import { computed, ref, toValue, watch } from "vue";
import type { MouseState } from "../common";
import type { GestureTarget, WebKitGestureEvent } from "@vueuse/gesture";
import { type Handler, useWheel, useDrag, usePinch } from "@vueuse/gesture";
import { useLogger } from "../logger";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("usePanAndZoom");

/**
 * usePannedAndZoomedDomain returns a reactive domain based on the first argment, that adjusts
 * based on pointer drag, multi-touch pinch on touchscreens, and mousewheel. For the most common
 * charting cases you will want to call this twice, once for the X axis and once for Y
 *
 * @param domain The source domain, usually from the source-data itself
 * @param axis Only a single dimension is handled at a time, 'x' for horizontal, 'y' for vertical
 * @param axisIsReversed true for inverted axis
 * @param ddvRef A ref to the DDV component, ddv holds the event listeners
 * @param mouseState A a ref to DDV's mouseState
 * @param enablePan exists to support reactive enable/disable behavior, will also accept non-reactive booleans
 * @param enableZoom exists to support reactive enable/disable behavior, will also accept non-reactive booleans
 */
export const usePannedAndZoomedDomain = (
  domain: MaybeRefOrGetter<Domain>,
  axis: MaybeRefOrGetter<"x" | "y">,
  axisIsReversed: MaybeRefOrGetter<boolean>,
  domRef: MaybeRefOrGetter<GestureTarget>,
  mouseState: MaybeRefOrGetter<MouseState | undefined>,
  enablePan: MaybeRefOrGetter<boolean>,
  enableZoom: MaybeRefOrGetter<boolean>
) => {
  const adjustedDomain = ref<Domain>(toValue(domain));
  const isZooming = ref(false);
  const isPanning = ref(false);

  // log("setting up gesture handlers");

  const pinchHandler: Handler<
    "pinch",
    WheelEvent | TouchEvent | WebKitGestureEvent
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
  > = ({ pinching, event, delta: [d, a], vdva, origin }) => {
    if (!pinching) {
      isZooming.value = false;
    }
    if (toValue(enableZoom) && pinching && toValue(mouseState)) {
      event.preventDefault();
      isZooming.value = true;
      adjustedDomain.value = toValue(domain);

      const rect = toValue(domRef)?.getBoundingClientRect();
      if (rect) {
        const normalizedCenterOfPinch = {
          x: origin[0] / rect.width,
          y: origin[1] / rect.height
        };

        const percentOfPlot =
          toValue(axis) === "x" ? d / rect.width : d / rect.height;
        const zoom = Math.exp(percentOfPlot * 0.5);

        const oldRange = getRange(toValue(domain));
        const zoomed = scale(
          toValue(domain),
          normalizedCenterOfPinch[toValue(axis)],
          zoom
        );
        const newRange = getRange(zoomed);
        const rangeDiff = newRange - oldRange;
        const lowerShift = rangeDiff * normalizedCenterOfPinch[toValue(axis)];
        const upperShift =
          rangeDiff * (1 - normalizedCenterOfPinch[toValue(axis)]);
        if (isDateDomain(zoomed)) {
          adjustedDomain.value.min = new Date(
            zoomed.min.getTime() - lowerShift
          );
          adjustedDomain.value.max = new Date(
            zoomed.max.getTime() + upperShift
          );
        } else if (isNumberDomain(adjustedDomain.value)) {
          adjustedDomain.value.min -= lowerShift;
          adjustedDomain.value.max += upperShift;
        }
      }
      console.log("PINCH", { d });
    }
  };

  const panHandler: Handler<"drag", PointerEvent> = ({
    delta: [x, y],
    dragging
  }) => {
    if (!toValue(enablePan) || !toValue(mouseState)?.isPlotHovered)
      return false;

    if (!dragging) {
      // DRAG END
      isPanning.value = false;
      return;
    }
    if (x === 0 && y === 0) {
      // DRAG START
      isPanning.value = true;
      return;
    }

    const rect = toValue(domRef)?.getBoundingClientRect();
    if (rect?.height && rect?.width) {
      const range = getRange(toValue(domain));
      const d = toValue(domain);
      const unsignedShift =
        range * (toValue(axis) === "x" ? x / rect.width : y / rect.height);
      const shift =
        unsignedShift *
        (toValue(axis) === "x"
          ? toValue(axisIsReversed)
            ? -1
            : 1
          : toValue(axisIsReversed)
            ? -1
            : 1);
      if (isDateDomain(d)) {
        adjustedDomain.value = d;
        adjustedDomain.value.min = new Date(d.min.getTime() - shift);
        adjustedDomain.value.max = new Date(d.max.getTime() - shift);
      } else {
        adjustedDomain.value = d;
        adjustedDomain.value.min =
          valueToNumber(adjustedDomain.value.min, adjustedDomain.value) - shift;
        adjustedDomain.value.max =
          valueToNumber(adjustedDomain.value.max, adjustedDomain.value) - shift;
      }
    }
  };

  const wheelHandler: Handler<"wheel", WheelEvent> = ({
    delta: [, y],
    wheeling,
    event
  }) => {
    if (!wheeling) {
      isZooming.value = false;
    } else if (toValue(enableZoom) && toValue(mouseState)?.isPlotHovered) {
      event.preventDefault();

      isZooming.value = true;
      const rect = toValue(domRef)?.getBoundingClientRect();
      const ms = toValue(mouseState);
      if (rect && ms) {
        adjustedDomain.value = toValue(domain);
        const scrolledUp = y < 0 ? 1 : -1;
        const percentOfPlot = Math.abs(y / rect.height);
        const zoom = Math.exp(scrolledUp * percentOfPlot * 0.5);
        const oldRange = getRange(adjustedDomain.value);
        const zoomed = scale(
          toValue(domain),
          ms.normalizedMouse[toValue(axis)],
          zoom
        );
        const newRange = getRange(zoomed);
        const rangeDiff = newRange - oldRange;
        const lowerShift = rangeDiff * ms.normalizedMouse[toValue(axis)];
        const upperShift = rangeDiff * (1 - ms.normalizedMouse[toValue(axis)]);

        // log("wheel handler", { newRange, lowerShift, upperShift });

        if (isDateDomain(zoomed)) {
          adjustedDomain.value.min = new Date(
            zoomed.min.getTime() - lowerShift
          );
          adjustedDomain.value.max = new Date(
            zoomed.max.getTime() + upperShift
          );
        } else if (isNumberDomain(adjustedDomain.value)) {
          adjustedDomain.value.min -= lowerShift;
          adjustedDomain.value.max += upperShift;
        }
      }
    }
  };

  useWheel(wheelHandler, {
    domTarget: domRef,
    eventOptions: {
      capture: true,
      passive: false
    }
  });
  useDrag(panHandler, {
    domTarget: domRef,
    eventOptions: {
      capture: true
    }
  });
  usePinch(pinchHandler, {
    domTarget: domRef,
    eventOptions: {
      capture: true,
      passive: true
    }
  });
  watch(
    () => toValue(domain),
    () => {
      // log("firing new domain", adjustedDomain.value);
      adjustedDomain.value = toValue(domain);
    }
  );

  const out = computed(() => {
    return adjustedDomain.value || toValue(domain);
  });

  return {
    isZooming,
    isPanning,
    adjustedDomain: out
  };
};
