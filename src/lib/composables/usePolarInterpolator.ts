import type { ComputedRef } from "vue";
import { ref, computed, reactive } from "vue";
import type { MaybeRef } from "@vueuse/core";
import type { NumericPoint, LinearScaler, CartesianScaler } from "../common";
import type { NumericDomain, Domain } from "../domain";
import { useLinearScaler } from "./useLinearScaler";
import { useCartesianScaler } from "./useCartesianScaler";

export type PolarSystem = {
  /** the center of this system in screen-space pixels */
  center: NumericPoint;
  radiusMin: number;
  radiusMax: number;
  /** the length of the perimeter in screen-space pixels */
  perimeter: number;
  perimeterRatio: number;
  angleScaler: LinearScaler;
  magnitudeScaler: LinearScaler;
  cartesianScaler: CartesianScaler;
};

const polarToCartesian = (
  center: NumericPoint,
  magnitude: number,
  angleInDegrees: number
): NumericPoint => {
  const angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0;
  return {
    x: center.x + magnitude * Math.cos(angleInRadians),
    y: center.y + magnitude * Math.sin(angleInRadians)
  };
};

export const describeArc = (
  center: NumericPoint,
  magnitude: number,
  startAngleDegrees: number,
  endAngleDegrees: number,
  sweep = true,
  largeArc = true
): string => {
  const startPoint = polarToCartesian(center, magnitude, startAngleDegrees);
  const endPoint = polarToCartesian(center, magnitude, endAngleDegrees);
  // const startDegrees = startAngleDegrees % 360;
  // const endDegrees = endAngleDegrees % 360;

  const rotation = 0;
  // const largeArc = largeArc ? 1 : 0; //Math.abs(endDegrees - startDegrees) <= 180 ? 1 : 0;
  // const sweep = forceSweep ? 1 : 0; //endDegrees < startDegrees ? 1 : 0;

  return [
    "L",
    startPoint.x,
    startPoint.y,
    "A",
    magnitude,
    magnitude,
    rotation,
    largeArc ? 1 : 0,
    sweep ? 1 : 0,
    endPoint.x,
    endPoint.y
  ].join(" ");
};

export const usePolarInterpolator = (
  screenDomain: MaybeRef<NumericPoint>, // x=width y=height
  angleDomain: MaybeRef<Domain>,
  magnitudeDomain: MaybeRef<Domain>,
  startDistanceRatio: MaybeRef<number>,
  endDistanceRatio: MaybeRef<number>,
  perimeterRatio: MaybeRef<number>
): ComputedRef<PolarSystem> => {
  const _screen = ref(screenDomain);
  const _angle = ref(angleDomain);
  const _mag = ref(magnitudeDomain);
  const _start = ref(startDistanceRatio);
  const _end = ref(endDistanceRatio);
  const _perimeterRatio = ref(perimeterRatio);

  const outerPadding = 20;

  const center = computed<NumericPoint>(() => ({
    x: _screen.value.x / 2,
    y:
      _perimeterRatio.value <= 0.5
        ? _screen.value.y - outerPadding * 2
        : _screen.value.y / 2
  }));
  // the maximum radius on this plot becore taking start/end distance ratio into account
  const fullRadius = computed(() => {
    const out =
      _perimeterRatio.value <= 0.5
        ? center.value.y - outerPadding
        : Math.min(
            center.value.x - outerPadding,
            center.value.y - outerPadding
          );
    return out;
  });
  const radiusMin = computed(() => fullRadius.value * _start.value);
  const radiusMax = computed(() => fullRadius.value * _end.value);
  const perimeter = computed(
    () => radiusMax.value * 2 * Math.PI * _perimeterRatio.value
  );
  const startAngle = computed(() => 180 + (1 - _perimeterRatio.value) * 180);
  const angleTargetDomain = computed(() => ({
    min: 0,
    max: _perimeterRatio.value * 360
  }));

  const magnitudeTargetDomain = computed<NumericDomain>(() => ({
    min: radiusMin.value,
    max: radiusMax.value
  }));

  const magnitudeScaler = useLinearScaler(_mag, magnitudeTargetDomain, false);

  const angleScaler = useLinearScaler(
    _angle,
    angleTargetDomain,
    false,
    scaled => (startAngle.value + scaled) % 360
  );

  const cartesianScaler = useCartesianScaler(
    angleScaler,
    magnitudeScaler,
    scaled => polarToCartesian(center.value, scaled.y, scaled.x)
  );

  return computed(() =>
    reactive({
      center,
      radiusMin,
      radiusMax,
      perimeter,
      perimeterRatio,
      angleScaler,
      magnitudeScaler,
      cartesianScaler
    })
  );
};
