import { ref } from "vue";

export const useArrayToggler = () => {
  const inactiveItems = ref<number[]>([]);

  // watch the prop, not the processedDataSets, or it'll create a dependency cycle
  // watch(ref(props.dataSets), () => {
  //   // automatically deactivate any datasets present that dont have any points "NO SIGNAL"
  //   inactiveDataSets.value = props.dataSets
  //     .map(ds => ds.points.length > 0)
  //     .map((hasPoints, i) => (hasPoints ? null : i))
  //     .filter(val => val !== null) as number[];
  // });

  const isActive = (id: number): boolean =>
    inactiveItems.value.length > 0 && !inactiveItems.value.includes(id);

  const activate = (id: number) => {
    inactiveItems.value = inactiveItems.value.filter(i => i !== id);
  };

  const deactivate = (id: number) => {
    inactiveItems.value.push(id);
  };

  const toggle = (id: number) => {
    inactiveItems.value.includes(id) ? deactivate(id) : activate(id);
  };

  return {
    inactiveItems,
    isActive,
    activate,
    deactivate,
    toggle
  };
};
