import type { MaybeRefOrGetter } from "vue";
import { computed, toValue } from "vue";
import type {
  CartesianScaler,
  IdentifiedNumericPoint,
  Point,
  ProjectedDataSet,
  ProjectedStackedPointsByX,
  StackedPointsByX,
  StackedYVal
} from "../common";
import { isDiscretePoint, valueToNumber, type DataSet } from "../common";
import { useLogger } from "../logger";
import type { Domain } from "../domain";
import { isDateDomain } from "../domain";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("useStackedPoints");

export const getStackKey = (p: Point, domain?: Domain): string | number =>
  isDiscretePoint(p) ? p.x : valueToNumber(p.x, domain);

export const useStackedPoints = (
  dataSets: MaybeRefOrGetter<DataSet[]>,
  inactiveDataSets: MaybeRefOrGetter<number[]>,
  cartesianScaler: MaybeRefOrGetter<CartesianScaler>
) => {
  // sort all values by unique X coordinate to support "stacking" visually
  const yValuesByX = computed<StackedPointsByX>(() => {
    const xd = toValue(cartesianScaler).xScaler().sourceDomain();
    return toValue(dataSets).reduce(
      (out: StackedPointsByX, ds, dataSeriesNumber) => {
        if (toValue(inactiveDataSets).includes(dataSeriesNumber)) return out;

        ds.points.forEach((p, i) => {
          const xKey = getStackKey(p, xd);
          // log("indexing x value", xKey, "type", typeof xKey, "value", p.y);
          out.set(
            xKey,
            [
              ...(out.get(xKey) || []),
              {
                value: p.y,
                pointIndex: i,
                dataSeriesNumber
              }
            ].sort((a, b) => a.dataSeriesNumber - b.dataSeriesNumber)
          );
        });
        return out;
      },
      new Map()
    );
  });

  const stackedScreenPointsByXValue = computed<ProjectedStackedPointsByX>(
    () => {
      // log("recomputing stacked points");

      return Array.from(yValuesByX.value.values()).reduce(
        (
          acc: ProjectedStackedPointsByX,
          entries: StackedYVal[],
          xIndex: number
        ) => {
          // log("processing key", xValue, typeof xValue);
          const xValue = Array.from(yValuesByX.value.keys())[xIndex];
          const _cart = toValue(cartesianScaler);
          const scaleX = _cart.xScaler().scale;
          const scaleY = _cart.yScaler().scale;
          const yDomain = _cart.yScaler().sourceDomain();

          const offsetValues = (entries || []).map(
            (v, i): IdentifiedNumericPoint => {
              let x = scaleX(xValue);
              if (isDateDomain(_cart.xScaler().sourceDomain())) {
                // log("making stacked screen points for", xValue, typeof xValue);
                if (typeof xValue === "string") {
                  x = scaleX(parseInt(xValue));
                  // } else {
                  // throw new Error(`what is this thing? ${xValue} ${typeof xValue}`);
                }
              }
              if (i < 1) {
                // log("first point of series", xValue, "-X->", scaleX(xValue));
                return {
                  x,
                  y: scaleY(v.value),
                  pointIndex: v.pointIndex,
                  dataSeriesNumber: v.dataSeriesNumber,
                  stackIndex: 0
                };
              } else {
                const runningTotal = (yValuesByX.value.get(xValue) || [])
                  .slice(0, i)
                  .reduce(
                    (total, v) => total + valueToNumber(v.value, yDomain),
                    0
                  );
                const offset = scaleY(0) - scaleY(runningTotal);
                // log(
                //   "point",
                //   i,
                //   "of",
                //   xValue,
                //   "scaled x",
                //   scaleX(xValue),
                //   "running total",
                //   runningTotal,
                //   "offset",
                //   offset
                // );
                return {
                  x,
                  y: scaleY(v.value) - (offset || 0),
                  stackIndex: i,
                  pointIndex: v.pointIndex,
                  dataSeriesNumber: v.dataSeriesNumber
                };
              }
            }
          );
          acc.set(xValue, offsetValues);
          return acc;
        },
        new Map()
      );
    }
  );

  const getStackedPoint = (
    point: Point,
    dataSeriesNumber: number,
    pointIndex: number
  ) => {
    const xKey = getStackKey(
      point,
      toValue(cartesianScaler).xScaler().sourceDomain()
    );
    const stack = stackedScreenPointsByXValue.value.get(xKey) || [];
    // log("stack is", stack);
    const stackIndex = (stack || []).findIndex(
      sy =>
        sy.pointIndex === pointIndex && sy.dataSeriesNumber === dataSeriesNumber
    );
    const stackedPoint = stack[stackIndex];
    // log("found", out);
    return { stackIndex, stackedPoint };
  };

  const projectedDataSets = computed<ProjectedDataSet[]>(() => {
    const sets = toValue(dataSets);
    // const out = Array.from({ length: sets.length });
    const out = sets.reduce((agg: ProjectedDataSet[], ds, dataSeriesNumber) => {
      // const { screenPoints } = useScreenPoints(ds.points, xScaler, yScaler, []);
      agg.push({
        dataSeriesNumber,
        title: ds.title,
        xAxisLabel: ds.xAxisLabel,
        yAxisLabel: ds.yAxisLabel,
        pointsByXValue: stackedScreenPointsByXValue.value,
        pointPairs: ds.points.map((p, pointIndex) => {
          const { stackIndex, stackedPoint } = getStackedPoint(
            p,
            dataSeriesNumber,
            pointIndex
          );
          return {
            screenPoint: { x: stackedPoint?.x || 0, y: stackedPoint?.y || 0 },
            dataPoint: p,
            dataSeriesNumber,
            stackIndex
          };
        })
      });

      return agg;
    }, []);
    return out;
  });

  return {
    yValuesByX,
    projectedDataSets,
    stackedScreenPointsByXValue
  };
};
