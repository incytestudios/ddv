import type { MaybeElement, MaybeElementRef } from "@vueuse/core";
import { useMouseInElement } from "@vueuse/core";
import type { MaybeRefOrGetter } from "vue";
import { computed, toValue } from "vue";
import type { Layout, Margins, MouseState } from "../common";

export const useMouseState = (
  domRef: MaybeElementRef<MaybeElement>,
  layout: MaybeRefOrGetter<Layout>,
  margins: MaybeRefOrGetter<Margins>
) => {
  const {
    x: mousePageX,
    y: mousePageY,
    elementX: mouseElementX,
    elementY: mouseElementY,
    isOutside: mouseIsOutside
  } = useMouseInElement(domRef, {
    handleOutside: false
  });

  const mouseState = computed<MouseState>(() => {
    const l = toValue(layout) || { plotWidth: 0, plotHeight: 0 };
    const m = toValue(margins) || { top: 0, left: 0, right: 0, bottom: 0 };
    return {
      mouse: {
        x: mouseElementX.value - m.left,
        y: mouseElementY.value - m.top
      },
      normalizedMouse: {
        x: (mouseElementX.value - m.left) / (l.plotWidth || 1),
        y: (l.plotHeight - mouseElementY.value - m.top) / (l.plotHeight || 1)
      },
      globalMouse: { x: mousePageX.value, y: mousePageY.value },
      isHovered: !mouseIsOutside.value,
      isPlotHovered:
        !mouseIsOutside.value &&
        mouseElementX.value >= m.left &&
        mouseElementX.value <= m.left + l.plotWidth &&
        mouseElementY.value >= m.top &&
        mouseElementY.value <= m.top + l.plotHeight
    };
  });

  return mouseState;
};
