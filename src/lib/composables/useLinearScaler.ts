import type { MaybeRefOrGetter } from "vue";
import { toValue } from "vue";
import {
  getRange,
  isDateDomain,
  isDiscreteDomain,
  toNumericDomain,
  type Domain,
  type NumericDomain
} from "../domain";
import {
  valueToNumber,
  type Value,
  type LinearScaler,
  type ScaleFunc
} from "../common";
import { useLogger } from "../logger";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("useLinearScaler");

export const useInterpolator = (domain: MaybeRefOrGetter<Domain>) => {
  return {
    interpolate(normalizedValue: number) {
      const currentD = toValue(domain);
      const d = isDateDomain(currentD) ? toNumericDomain(currentD) : currentD;
      const interpolated = d.min + normalizedValue * getRange(toValue(domain));
      // log(
      //   "interpolating",
      //   normalizedValue,
      //   "between",
      //   [d.min, d.max],
      //   "=>",
      //   interpolated,
      //   "is date domain",
      //   isDateDomain(d)
      // );
      return interpolated;
    }
  };
};

export const useNormalizer = (domain: MaybeRefOrGetter<Domain>) => {
  return {
    normalize(value: Value) {
      const currentD = toValue(domain);
      const v = valueToNumber(value, currentD);
      const d = isDateDomain(currentD) ? toNumericDomain(currentD) : currentD;
      const normalized = (v - d.min) / getRange(d);
      // log("normalizing", v, "between", [d.min, d.max], "=>", normalized);
      return normalized;
    }
  };
};

export const useLinearScaler = (
  source: MaybeRefOrGetter<Domain>,
  target: MaybeRefOrGetter<NumericDomain>,
  reverseAxis: MaybeRefOrGetter<boolean>,
  adjustScale?: ScaleFunc<number>
): LinearScaler => {
  const { normalize } = useNormalizer(source);
  const { interpolate } = useInterpolator(target);

  return {
    scale: (dataValue: Value) => {
      const src = toValue(source);
      const tgt = toValue(target);
      let dv: Value = 0;
      if (isDateDomain(src)) {
        if (dataValue instanceof Date) {
          // log("IT WAS A DATE");
          dv = dataValue;
        } else if (typeof dataValue === "number") {
          // log("IT WAS A NUMBER");
          dv = new Date(dataValue);
        } else if (typeof dataValue === "string") {
          // log("IT WAS A STRING");
          dv = new Date(dataValue);
        }
        // log("scaling number in Date domain, interpreted as", {
        //   domain: toValue(src),
        //   dataValue,
        //   dv,
        //   normalized: normalize(dv),
        //   coord: interpolate(normalize(dataValue)),
        //   reversed: getRange(tgt) - normalize(dv) * getRange(tgt)
        // });
        // return tgt.max - normalize(dv) * getRange(tgt);
      } else if (isDiscreteDomain(src)) {
        dv = dataValue;
        // log("scaling string in domain, interpreted as", dv);
      } else {
        dv = valueToNumber(dataValue, tgt);
        // log("scaling UNKNOWN in domain, interpreted as", dv);
      }
      // log("in scale about to normalize dv", dv);
      const scaled = toValue(reverseAxis)
        ? getRange(tgt) - normalize(dv) * getRange(tgt)
        : interpolate(normalize(dv));
      // log("scaled", { dataValue, scaled });
      return adjustScale !== undefined ? adjustScale(scaled) : scaled;
    },
    invert: (screenValue: number) => {
      return interpolate(1 - normalize(screenValue));
    },
    normalize,
    interpolate,
    sourceDomain: () => toValue(source)
  };
};
