import type { MaybeRefOrGetter } from "vue";
import { computed, ref, toValue } from "vue";
import type { CartesianScaler, DataSet, ProjectedDataSet } from "../common";
import { useScreenPoints } from "./useScreenPoints";
import { useStackedPoints } from "./useStackedPoints";

export const useProjectedDataSets = (
  dataSets: MaybeRefOrGetter<DataSet[]>,
  inactiveDataSets: MaybeRefOrGetter<number[]>,
  cartesianScaler: MaybeRefOrGetter<CartesianScaler>,
  stackSeries: MaybeRefOrGetter<boolean> = false,
  shiftSeries: MaybeRefOrGetter<number> = 0
) => {
  const stackedPoints = computed(() => {
    if (toValue(stackSeries)) {
      return useStackedPoints(dataSets, inactiveDataSets, cartesianScaler);
    }
    return {
      stackedScreenPointsByXValue: ref([]),
      projectedDataSets: ref([])
    };
  });

  const projectedDataSets = computed<ProjectedDataSet[]>(() => {
    if (toValue(stackSeries)) {
      return stackedPoints.value.projectedDataSets.value;
    } else {
      return toValue(dataSets).reduce(
        (agg: ProjectedDataSet[], ds: DataSet, dataSeriesNumber) => {
          if (toValue(inactiveDataSets).includes(dataSeriesNumber)) return agg;
          // build shiftable offsets for discrete domains
          const offsetPoints = toValue(shiftSeries)
            ? ds.points.map(() => ({
                x: dataSeriesNumber * toValue(shiftSeries),
                y: 0
              }))
            : [];
          // console.log("offsetPoints", offsetPoints);
          const { screenPoints } = useScreenPoints(
            ds.points,
            cartesianScaler,
            offsetPoints
          );
          agg.push({
            dataSeriesNumber,
            title: ds.title || `Series ${dataSeriesNumber}`,
            xAxisLabel: ds.xAxisLabel || "X",
            yAxisLabel: ds.yAxisLabel || "Y",
            pointsByXValue: new Map(),
            // can't remember if this bit is needed for unstacked points?
            // pointsByXValue: useStackedPoints(
            //   dataSets,
            //   inactiveDataSets,
            //   xScaler,
            //   yScaler
            // ).stackedScreenPointsByXValue.value,
            pointPairs: ds.points.map((p, i) => ({
              dataSeriesNumber,
              dataPoint: p,
              screenPoint: screenPoints.value[i]
            }))
          });
          return agg;
        },
        []
      );
    }
  });

  return {
    projectedDataSets,
    stackedScreenPointsByXValue: stackedPoints.value.stackedScreenPointsByXValue
  };
};
