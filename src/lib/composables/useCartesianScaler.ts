import type { MaybeRefOrGetter } from "vue";
import { toValue } from "vue";
import { type Point, type NumericPoint, type LinearScaler } from "../common";

export const useCartesianScaler = (
  xScaler: MaybeRefOrGetter<LinearScaler>,
  yScaler: MaybeRefOrGetter<LinearScaler>,
  // eslint-disable-next-line no-unused-vars
  adjustScale?: (scaled: NumericPoint) => NumericPoint
) => {
  return {
    xScaler: () => toValue(xScaler),
    yScaler: () => toValue(yScaler),
    scale: (p: Point) => {
      const _x = toValue(xScaler);
      const _y = toValue(yScaler);
      const scaled = { x: _x.scale(p.x), y: _y.scale(p.y) };
      return adjustScale !== undefined ? adjustScale(scaled) : scaled;
    }
  };
};
