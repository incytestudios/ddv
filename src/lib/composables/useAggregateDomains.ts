import type { ComputedRef, MaybeRefOrGetter, Ref } from "vue";
import { computed, toValue } from "vue";
import { useLogger } from "../logger";
import type { DatePoint, Point } from "./../common";
import {
  type DataSet,
  getUniqueXValues,
  isDateSeries,
  isDiscreteSeries,
  isNumericSeries,
  type StackedPointsByX,
  valueToNumber
} from "./../common";
import type { DateDomain } from "./../domain";
import {
  type Domain,
  fromRange,
  fromValues,
  getAbsoluteExtents,
  getExtents
} from "./../domain";

export const ErrorDatasetDomainMismatch = new Error(
  "cannot aggregate domains of different types!"
);
ErrorDatasetDomainMismatch.name = "ErrorDatasetDomainMismatch";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("useAggregateDomains");

export const domainsAreCompatible = (dataSets: DataSet[]) => {
  const allPoints = dataSets.flatMap(ds => ds.points);

  return (
    allPoints.length === 0 ||
    isDateSeries(allPoints) ||
    isNumericSeries(allPoints) ||
    isDiscreteSeries(allPoints)
  );
};

export const getAdditiveExtents = (
  pointsByX: StackedPointsByX,
  xDomain: Domain
): Domain => {
  const additiveYMax = Array.from(pointsByX.values())
    .flatMap(entries => {
      const vals = entries.map(e => valueToNumber(e.value, xDomain));
      const sum = vals.reduce((total, v) => total + v, 0);
      return sum;
    })
    .sort((a, b) => b - a) // order by int value descending
    .shift();

  // console.log("getting additive aggregate", pointsByX, "xDomain", xDomain, {
  //   additiveYMax
  // });
  return { min: 0, max: additiveYMax ?? 0 };
};

// TODO: useAggregateDomains should be split into the various sub types of domains as different composables,
// is-is now, it complicates a lot of the process by using different access patterns to test the bounds
// of each provided dataSet's points, add some tests
export const useAggregateDomains = (
  dataSets: MaybeRefOrGetter<DataSet[]>,
  inactiveDataSets: MaybeRefOrGetter<number[]>,
  absoluteScale: MaybeRefOrGetter<boolean>,
  additiveValuesOnY: MaybeRefOrGetter<boolean>
): {
  aggregateX: Ref<Domain>;
  aggregateY: Ref<Domain>;
  aggregate: ComputedRef<{ x: Domain; y: Domain }>;
} => {
  const allPoints = computed(() =>
    toValue(dataSets)
      .filter((ds, i) => !toValue(inactiveDataSets).includes(i))
      .flatMap((ds, i) =>
        ds.points.map(p => ({
          x: p.x,
          y: p.y,
          dataSeriesNumber: i
        }))
      )
  );

  const aggregate = computed(() => {
    if (!domainsAreCompatible(toValue(dataSets))) {
      throw ErrorDatasetDomainMismatch;
    }

    const pointsByX = allPoints.value.reduce(
      (acc: StackedPointsByX, p: Point & { dataSeriesNumber: number }) => {
        if (!toValue(additiveValuesOnY)) return acc; // dont build pointsByX
        // console.log("making pointsByX", p);
        acc.set(p.x, [
          ...(acc.get(p.x) || []),
          { value: p.y, dataSeriesNumber: p.dataSeriesNumber, pointIndex: 0 }
        ]);
        return acc;
      },
      new Map()
    );
    // log("points by X", pointsByX);

    let stackedYMax = 0;
    if (isDiscreteSeries(allPoints.value)) {
      if (additiveValuesOnY) {
        // log("stacking discrete domain!!!!!!!!!");
        // now we need to go by each unique X value and find the largest sum of all Y values for that X,
        stackedYMax = Array.from(pointsByX.values()).reduce((acc, entries) => {
          const maxYThisX = entries.reduce(
            (total, e) => total + valueToNumber(e.value),
            0
          );
          return Math.max(acc, maxYThisX);
        }, 0);
        // log("sum of stacked values for", stackedYMax);
      }

      return {
        x: fromValues(getUniqueXValues(allPoints.value)),
        y: toValue(additiveValuesOnY)
          ? fromRange(stackedYMax)
          : toValue(absoluteScale)
            ? getAbsoluteExtents(allPoints.value.map(p => valueToNumber(p.y)))
            : getExtents(allPoints.value.map(p => valueToNumber(p.y)))
      };

      // return getAggregateDiscreteDomain(
      //   allPoints.value,
      //   toValue(absoluteScale),
      //   toValue(additiveValuesOnY)
      // );
    } else if (isDateSeries(allPoints.value)) {
      const allX = allPoints.value.map(p => (p as DatePoint).x.getTime());
      const allY = allPoints.value.map(p => p.y as number);
      const x: DateDomain = {
        min: new Date(Math.min(...allX)),
        max: new Date(Math.max(...allX))
      };
      return {
        x,
        y: toValue(additiveValuesOnY)
          ? getAdditiveExtents(pointsByX, x)
          : toValue(absoluteScale)
            ? getAbsoluteExtents(allY)
            : getExtents(allY)
      };
    } else if (isNumericSeries(allPoints.value)) {
      const allX = allPoints.value.map(p => p.x as number);
      const allY = allPoints.value.map(p => p.y as number);
      const x = toValue(absoluteScale)
        ? getAbsoluteExtents(allX)
        : getExtents(allX);
      return {
        x,
        y: toValue(additiveValuesOnY)
          ? getAdditiveExtents(pointsByX, x)
          : toValue(absoluteScale)
            ? getAbsoluteExtents(allY)
            : getExtents(allY)
      };
    } else {
      log("unknown series type");
    }
    return { x: fromRange(0), y: fromRange(0) };
  });

  const aggregateX = computed(() => aggregate.value.x);
  const aggregateY = computed(() => aggregate.value.y);

  return { aggregateX, aggregateY, aggregate };
};
