import { useLogger } from "./../logger";
import { isDateSeries, isNumericSeries } from "./../common";
import type { MaybeRefOrGetter } from "vue";
import { ref, computed, type ComputedRef, toValue } from "vue";
import { LTTB } from "downsample";
import { type DataSet, type NumericPoint } from "../common";
import type { MaybeRef } from "@vueuse/core";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("useDownSampler");

/**
 * returns a computed version of the passed in `dataSets` that reduces the total number of points. This is quite useful
 * when the number of data points isn't known ahead of time, or there are more data than pixels.
 */
export const useDownSampler = (
  /**
   * The datasets to be downsampled
   */
  dataSets: MaybeRefOrGetter<DataSet[]>,
  /**
   * The maximum number of points to allow in a single dataset
   * @note: it's often best to set this `domState.plotWidth` (the number of pixels)
   */
  maxPoints: MaybeRef<number> = ref(1_000)
): ComputedRef<DataSet[]> => {
  const _max = ref(maxPoints);

  return computed(() =>
    toValue(dataSets).map(ds => {
      // clone this dataset
      const out = { ...ds };
      if (isDateSeries(ds.points) || isNumericSeries(ds.points)) {
        out.points = LTTB(ds.points, _max.value) as NumericPoint[];
      }
      return out;
    })
  );
};
