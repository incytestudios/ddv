import { reactive } from "vue";
import { useLogger } from "../logger";
import type { Margins, SpaceReservations } from "../common";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("useSpaceReservations");

export type SpaceFreeFn = (id: string) => void;
export type SpaceReserveFn = (id: string, margins: Margins) => SpaceFreeFn;

export const useSpaceReservations = () => {
  const spaceReservations: SpaceReservations = reactive({});

  const reserveSpace: SpaceReserveFn = (id: string, margins: Margins) => {
    // log("reserving space for", id);
    spaceReservations[id] = margins;

    return () => {
      // log("freeing space for", id);
      // give back a method that frees this space
      delete spaceReservations[id];
    };
  };

  return {
    spaceReservations,
    reserveSpace
  };
};
