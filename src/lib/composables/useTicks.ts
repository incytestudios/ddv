import { clamp, measuredCSSDistance, type MeasuredCSSLength } from "./../utils";
import type { LinearScaler } from "../common";
import type { ComputedRef } from "vue";
import { ref, computed } from "vue";
import type { MaybeRef } from "@vueuse/core";
import { getDefaultTickFormatter, type AxisTickFormatter } from "../formatting";
import type { Domain } from "../domain";
import type { TickSolution } from "../ticks";
import { solveTicks } from "../ticks";
import { useLogger } from "../logger";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("useTicks");

export const useTicks = (
  axisLengthPx: MaybeRef<number>,
  domain: MaybeRef<Domain>,
  tickFrequency: MaybeRef<string>,
  scaler: MaybeRef<LinearScaler>,
  isVertical: MaybeRef<boolean>,
  measuredFontSizes: MaybeRef<MeasuredCSSLength[]>,
  tickLabeller?: MaybeRef<AxisTickFormatter>
): ComputedRef<TickSolution | null> => {
  const _axis = ref(axisLengthPx);
  const _domain = ref(domain);
  const _frequency = ref(tickFrequency);
  const _isVertical = ref(isVertical);
  const _measuredFontSizes = ref(measuredFontSizes);
  const _labeller = ref(tickLabeller || getDefaultTickFormatter());
  const _scaler = ref(scaler);

  const density = computed(() => {
    const size = clamp(
      measuredCSSDistance(_frequency.value),
      measuredCSSDistance("1.5em"),
      Infinity
    );
    return 1 / size;
  });

  return computed(() =>
    _axis.value > 0 && _domain.value && isFinite(density.value)
      ? solveTicks(
          _axis.value,
          density.value,
          _domain.value,
          _isVertical.value,
          _labeller.value,
          _scaler.value,
          _measuredFontSizes.value
        )
      : null
  );
};
