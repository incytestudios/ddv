export const useLogger = (prefix = "[DDV]") => {
  return (...args: unknown[]): void => {
    if (!process.env.NODE_ENV?.toUpperCase().startsWith("PROD")) {
      console.log(`[DDV${prefix ? ":" + prefix : ""}]`, ...args);
    }
  };
};
