<script setup lang="ts">
import { computed, ref, toRefs } from "vue";
import type {
  DataSet,
  IdentifiedNumericPoint,
  IndexSearchResult,
  Layout,
  LinearScaler,
  Margins,
  MouseState,
  NumericPoint,
  ProjectedDataSet,
  ProjectedPointPair,
  Value
} from "../common";
import Axis from "../components/Axis.vue";
import { fromRange, scale } from "../domain";
import type { AxisTickFormatter } from "../main";
import {
  useAggregateDomains,
  useArrayToggler,
  useCartesianScaler,
  useLinearScaler,
  usePannedAndZoomedDomain,
  valueToNumber
} from "../main";
// import Crosshairs from "../components/Crosshairs.vue";
import { useFullscreen } from "@vueuse/core";
import { formatDate, formatNumber } from "../../lib/formatting";
import BSP from "../components/BSP.vue";
import ChartLegend from "../components/ChartLegend.vue";
import DDV from "../components/DDV.vue";
import NearestPoint from "../components/NearestPoint.vue";
import type { NearestPointByDataSet } from "../composables/useBSP";
import { useProjectedDataSets } from "../composables/useProjectedDataSets";

const props = withDefaults(
  defineProps<{
    /**
     * The data to draw
     */
    dataSets: DataSet[];
    /**
     * The current chart's layout (from `useChart`)
     */
    layout: Layout;
    /**
     * The current chart's mouseState (from `useMouseState`)
     */
    mouseState: MouseState;
    /**
     * When `true` visually stacks all Y values that share a common X value into a single "stack"
     */
    stackSeries: boolean;
    /**
     * When `stackSeries` is `false` this parameter controls how much to shift each series value from each other
     */
    shiftSeries: number;
    /**
     * For pointtracking on mouse over, treats all x values as matching
     */
    ignoreXValues?: boolean;
    /**
     * For point tracking on mouse over, treats all y values as matching
     */
    ignoreYValues?: boolean;
    /**
     * When `true` the user can click and drag to move the chart left and right
     */
    enableXPanning?: boolean;
    /**
     * When `true` the user can click and drag to move the chart up and down
     */
    enableYPanning?: boolean;
    /**
     * When `true` the user can use the mouse wheel to scale the X axis (zoom)
     */
    enableXZooming?: boolean;
    /**
     * When `true` the user can use the mouse wheel to scale the Y axis (zoom)
     */
    enableYZooming?: boolean;
    /**
     * When `true` a fullscreen button will be presented on mouse over
     */
    allowFullScreen?: boolean;
  }>(),
  {
    shiftSeries: 0,
    stackSeries: false,
    allowFullScreen: true
  }
);

const emit = defineEmits<{
  (e: "update:space", id: string, margins: Margins): void;
}>();

defineSlots<{
  /**
   * When `stackSeries` is `true` this slot will be called once per unique x-value, giving
   * all the Y values with their stacking order in `entries`
   */
  perXValue(props: {
    /** These are the points */
    entries: IdentifiedNumericPoint[];
    xValue: Value;
    yScaler: LinearScaler;
  }): unknown;
  /**
   * When `stackSeries` is `false` this slot will be called once per DataSet, giving all
   * the dataPoints and their projected screenPoints in `pointPairs`
   */
  perSeries(props: {
    dataSeriesNumber: number;
    dataSet: ProjectedDataSet;
    pointPairs: ProjectedPointPair[];
  }): unknown;
  /**
   * When `stackSeries` is `false` this slot will be called once per point
   */
  perPoint(props: {
    dataSeriesNumber: number;
    inactiveDataSets: number[];
    screenPoint: NumericPoint;
    yScaler: LinearScaler;
  }): unknown;
  legend(): unknown;
  crosshairs(props: { mouseState: MouseState; layout: Layout }): unknown;
}>();

const {
  dataSets,
  layout,
  mouseState,
  shiftSeries,
  stackSeries,
  enableXPanning,
  enableXZooming,
  enableYPanning,
  enableYZooming
} = toRefs(props);

const wrapper = ref<InstanceType<typeof DDV>>();

const {
  isFullscreen: isFullScreen,
  enter: enterFullScreen,
  exit: exitFullScreen
} = useFullscreen(wrapper);

const xScreenDomain = computed(() => fromRange(props.layout.plotWidth || 0));
const yScreenDomain = computed(() => fromRange(props.layout.plotHeight || 0));

const {
  inactiveItems: inactiveDataSets,
  activate: activateSeries,
  deactivate: deactivateSeries
} = useArrayToggler();

const { aggregateX, aggregateY } = useAggregateDomains(
  dataSets,
  inactiveDataSets,
  true,
  stackSeries
);

const paddedX = computed(() => scale(aggregateX.value, 0.5, 0.92));
const paddedY = computed(() => scale(aggregateY.value, 0, 0.92));

const {
  adjustedDomain: adjustedXDomain,
  isPanning: isPanningX,
  isZooming: isZoomingX
} = usePannedAndZoomedDomain(
  paddedX,
  "x",
  false,
  computed(() => wrapper.value?.$el),
  mouseState,
  enableXPanning,
  enableXZooming
);

const {
  adjustedDomain: adjustedYDomain
  // isPanning: isPanningY,
  // isZooming: isZoomingY
} = usePannedAndZoomedDomain(
  paddedY,
  "y",
  true,
  computed(() => wrapper.value?.$el),
  mouseState,
  enableYPanning,
  enableYZooming
);

const xScaler = useLinearScaler(adjustedXDomain, xScreenDomain, false);
const yScaler = useLinearScaler(adjustedYDomain, yScreenDomain, true);
const cartesianScaler = useCartesianScaler(xScaler, yScaler);

const { projectedDataSets, stackedScreenPointsByXValue } = useProjectedDataSets(
  dataSets,
  inactiveDataSets,
  cartesianScaler,
  stackSeries,
  shiftSeries
);

const xAxisFormatter: AxisTickFormatter = (value: Value) =>
  value instanceof Date
    ? formatDate(value)
    : typeof value === "string"
      ? value ?? ""
      : typeof value === "number"
        ? formatNumber(value)
        : "??";
</script>

<template>
  <DDV
    ref="wrapper"
    class="basic-chart"
    :layout="layout"
    :mouse-state="mouseState"
    :enable-mouse-tracking="true"
    :enable-pan="enableXPanning || enableYPanning"
    :enable-zoom="enableYPanning || enableXPanning"
  >
    <template v-if="allowFullScreen" #hoverOptions>
      <button
        v-if="enterFullScreen && exitFullScreen"
        class="fullscreen"
        @click="
          () =>
            isFullScreen && exitFullScreen
              ? exitFullScreen()
              : enterFullScreen
                ? enterFullScreen()
                : () => undefined
        "
      >
        {{ isFullScreen ? "Exit Fullscreen" : "Enter Fullscreen" }}
      </button>
    </template>
    <template
      v-if="layout?.plotWidth"
      #default="{ chartId, onUpdateNearestPoint }"
    >
      <defs>
        <g id="point">
          <circle cx="0" cy="0" :r="4" />
        </g>
        <clipPath :id="`plotClip-${chartId}`">
          <rect
            :x="layout.margins.left"
            :y="layout.margins.top"
            :width="layout.plotWidth"
            :height="layout.plotHeight"
          />
        </clipPath>
      </defs>

      <g class="background">
        <rect
          fill="var(--plotSurface)"
          :class="{
            isPanning: isPanningX,
            isZooming: isZoomingX
          }"
          :x="layout.margins.left"
          :y="layout.margins.top"
          :width="layout.plotWidth"
          :height="layout.plotHeight"
        />
      </g>

      <Axis
        :layout="layout"
        :x="layout.margins.left"
        :y="layout.margins.top"
        :scaler="xScaler"
        :domain="adjustedXDomain"
        :draw-grid-lines="true"
        tick-frequency="6em"
        :tick-length-px="4"
        :axis-tick-formatter="xAxisFormatter"
        :label="dataSets.find((ds: DataSet) => !!ds.xAxisLabel)?.xAxisLabel"
        @update:space="(id: string, m: Margins) => emit('update:space', id, m)"
      />
      <Axis
        :layout="layout"
        :x="layout.margins.left"
        :y="layout.margins.top"
        :scaler="yScaler"
        :domain="adjustedYDomain"
        :draw-grid-lines="true"
        :tick-length-px="6"
        :label="dataSets.find((ds: DataSet) => !!ds.yAxisLabel)?.yAxisLabel"
        is-vertical
        @update:space="(id: string, m: Margins) => emit('update:space', id, m)"
      />

      <g
        :style="{
          clipPath: `url(#plotClip-${chartId})`
        }"
      >
        <g
          :transform="`translate(${layout.margins.left} ${layout.margins.top})`"
        >
          <BSP
            :ignore-x-values="ignoreXValues"
            :ignore-y-values="ignoreYValues"
            :projected-data-sets="projectedDataSets"
            :mouse-state="mouseState"
            @update:nearest-point="
              (nearestPoints: NearestPointByDataSet | null) => {
                nearestPoints?.forEach(
                  (np: IndexSearchResult | null, dataSeriesNumber: number) => {
                    onUpdateNearestPoint(
                      projectedDataSets.at(dataSeriesNumber)?.title ||
                        `Series ${dataSeriesNumber}`,
                      dataSeriesNumber,
                      !inactiveDataSets.includes(dataSeriesNumber),
                      np,
                      undefined,
                      undefined,
                      dataSets.find((ds: DataSet) => !!ds.xAxisLabel)
                        ?.xAxisLabel,
                      dataSets.find((ds: DataSet) => !!ds.yAxisLabel)
                        ?.yAxisLabel
                    );
                  }
                );
              }
            "
          >
            <template #default="{ nearestPoints }">
              <template v-if="stackSeries">
                <g
                  v-for="[
                    xValue,
                    entries
                  ] of stackedScreenPointsByXValue.entries()"
                  :key="valueToNumber(xValue, adjustedXDomain)"
                >
                  <slot
                    name="perXValue"
                    :entries="entries"
                    :x-value="xValue"
                    :y-scaler="yScaler"
                  >
                    <circle
                      v-for="(e, i) of entries.sort(
                        (
                          a: IdentifiedNumericPoint,
                          b: IdentifiedNumericPoint
                        ) => a.stackIndex - b.stackIndex
                      )"
                      :key="i"
                      :data-series-number="e.dataSeriesNumber"
                      :cx="e.x"
                      :cy="e.y"
                      fill="var(--activePlotColor)"
                      r="4"
                    />
                  </slot>
                </g>
              </template>
              <template v-else>
                <g
                  v-for="(ds, dataSeriesNumber) of projectedDataSets"
                  :key="dataSeriesNumber"
                  :data-series-number="dataSeriesNumber"
                >
                  <slot
                    name="perSeries"
                    :data-set="ds"
                    :data-series-number="dataSeriesNumber"
                    :point-pairs="ds.pointPairs"
                  >
                    <g v-for="(pair, i) of ds.pointPairs" :key="i">
                      <slot
                        name="perPoint"
                        :data-series-number="dataSeriesNumber"
                        :inactive-data-sets="inactiveDataSets"
                        :screen-point="pair.screenPoint"
                        :y-scaler="yScaler"
                      >
                        <circle
                          :cx="pair.screenPoint.x"
                          :cy="pair.screenPoint.y"
                          fill="var(--activePlotColor)"
                          r="4"
                        />
                      </slot>
                    </g>
                  </slot>
                </g>
              </template>
              <g class="nearest-points">
                <NearestPoint
                  v-for="(np, i) of nearestPoints"
                  :key="i"
                  :data-series-number="i"
                  :nearest-point="np"
                />
              </g>
            </template>
          </BSP>
        </g>
      </g>

      <slot name="legend">
        <ChartLegend
          :layout="layout"
          :model-value="inactiveDataSets"
          :data-sets="dataSets"
          @activate-series="activateSeries"
          @deactivate-series="deactivateSeries"
          @update:space="
            (id: string, m: Margins) => emit('update:space', id, m)
          "
        />
      </slot>
      <slot name="crosshairs" :mouse-state="mouseState" :layout="layout">
        <!-- <Crosshairs
          v-if="mouseState"
          :layout="layout"
          :x="layout.margins.left"
          :y="layout.margins.top"
          :mouse-state="mouseState"
        /> -->
      </slot>
    </template>
  </DDV>
</template>
