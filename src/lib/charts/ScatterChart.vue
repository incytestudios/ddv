<script setup lang="ts">
import { computed, ref, watch } from "vue";
import type { IndexSearchResult, ProjectedPointPair } from "../common";
import {
  valueToNumber,
  type DataSet,
  type ToolTipInfo,
  type Value
} from "../common";
import Axis from "../components/Axis.vue";
import BSP from "../components/BSP.vue";
import ChartLegend from "../components/ChartLegend.vue";
import ChartTitle from "../components/ChartTitle.vue";
import Crosshairs from "../components/Crosshairs.vue";
import DDV from "../components/DDV.vue";
import LinePlot from "../components/LinePlot.vue";
import NearestPoint from "../components/NearestPoint.vue";
import ThresholdArea from "../components/ThresholdArea.vue";
import ThresholdLine from "../components/ThresholdLine.vue";
import { useAggregateDomains } from "../composables/useAggregateDomains";
import { useArrayToggler } from "../composables/useArrayToggler";
import type { NearestPointByDataSet } from "../composables/useBSP";
import { useCartesianScaler } from "../composables/useCartesianScaler";
import { useChart } from "../composables/useChart";
import { useDownSampler } from "../composables/useDownSampler";
import { useLinearScaler } from "../composables/useLinearScaler";
import { usePannedAndZoomedDomain } from "../composables/usePannedAndZoomedDomain";
import { useProjectedDataSets } from "../composables/useProjectedDataSets";
import { fromRange, isDiscreteDomain } from "../domain";
import type { AxisTickFormatter } from "../formatting";
import { getDefaultTickFormatter } from "../formatting";
import { useLogger } from "../logger";
import { clamp } from "../utils";

const props = withDefaults(
  defineProps<{
    /**
     * The title of this graph, leave undefined to omit
     */
    title?: string;
    /**
     * The datasets to plot, they will be drawn in order one on top of the other "lowest" (first) to "highest" (last)
     */
    dataSets: DataSet[];
    /**
     * Draws a simple linear X Axis
     */
    drawXAxis?: boolean;
    /**
     * Sets density of ticks using 1-tick per distance where this prop controls the distance.
     * You can set any valid css length here as a string such as `10px`, `0.5em`, or `30vh`.
     */
    tickFrequencyX?: string;
    /**
     * Draws a simple linear Y Axis
     */
    drawYAxis?: boolean;
    /**
     * Sets density of ticks using 1-tick per distance where this prop controls the distance.
     * You can set any valid css length here as a string such as `10px`, `0.5em`, or `30vh`.
     */
    tickFrequencyY?: string;
    /**
     * How many unlabelled ticks to draw between each pair of labelled ticks on the X Axis
     */
    subTicksX?: number;
    /**
     * How many unlabelled ticks to draw between each pair of labelled ticks on the X Axis
     */
    subTicksY?: number;
    /**
     * Draws a legend with togglable data
     */
    drawLegend?: boolean;
    /**
     * When true, will force including 0 in both domains
     */
    absoluteScale?: boolean;
    /**
     * Draw a catmull-rom curve throught the provided points
     */
    drawCurve?: boolean;
    /**
     * Draw unconnected data points
     */
    drawPoints?: boolean;
    /**
     * Draw bar-chart bars from bottom to data points
     */
    drawBars?: boolean;
    /**
     * Show tooltip info when the mouse is near a point
     */
    drawToolTips?: boolean;
    /**
     * Draw a catmull-rom curve throught the provided points
     */
    enablePan?: boolean;
    /**
     * Draw a catmull-rom curve throught the provided points
     */
    enableZoom?: boolean;
    /**
     * When true, mouse over queries will ignore X axis values
     */
    mouseTrackingIgnoreX?: boolean;
    /**
     * When true, mouse over queries will ignore Y axis values
     */
    mouseTrackingIgnoreY?: boolean;
  }>(),
  {
    title: undefined,
    drawXAxis: false,
    tickFrequencyX: "0",
    drawYAxis: false,
    tickFrequencyY: "0",
    subTicksX: 0,
    subTicksY: 0,
    drawLegend: false,
    absoluteScale: false,
    drawCurve: false,
    drawPoints: false,
    drawBars: false,
    drawToolTips: false,
    enablePan: false,
    enableZoom: false,
    mouseTrackingIgnoreX: false,
    mouseTrackingIgnoreY: false
  }
);

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("ScatterChart");

const wrapper = ref<InstanceType<typeof DDV>>();
const { layout, mouseState, reserveSpace } = useChart(wrapper);
const chartId = Math.round(Math.random() * 100_000);

const {
  inactiveItems: inactiveDataSets,
  activate: activateSeries,
  deactivate: deactivateSeries
} = useArrayToggler();

const validIds = computed(() =>
  props.dataSets
    .filter(ds => ds.points && ds.points.length > 0)
    .map((_, i) => i)
);
const invalidIds = computed(() =>
  props.dataSets
    .filter(ds => !ds.points || ds.points.length < 1)
    .map((_, i) => i)
);
watch(validIds, newIds => newIds.forEach(activateSeries));
watch(invalidIds, newIds => {
  newIds.forEach(deactivateSeries);
});

// downsample the sets
const processedDataSets = useDownSampler(() => props.dataSets);

const longestSet = computed(() =>
  processedDataSets.value.reduce(
    (longest: number, ds: DataSet) =>
      ds.points.length > longest ? ds.points.length : longest,
    0
  )
);

// find common limits of X and Y
const { aggregateX, aggregateY } = useAggregateDomains(
  processedDataSets,
  inactiveDataSets,
  () => props.absoluteScale,
  false
);

const scatterPointRadius = ref(4);

const reverseX = computed(() => !!processedDataSets.value[0]?.reverseXAxis);
const reverseY = computed(() => !processedDataSets.value[0]?.reverseYAxis); // intentionally flip the y-axis
const xScreenDomain = computed(() => fromRange(layout.value?.plotWidth || 0));
const yScreenDomain = computed(() => fromRange(layout.value?.plotHeight || 0));

const {
  adjustedDomain: xDomain,
  isPanning: isPanningX,
  isZooming: isZoomingX
} = usePannedAndZoomedDomain(
  aggregateX,
  "x",
  reverseX,
  computed(() => wrapper.value?.$el),
  mouseState,
  computed(() => props.enablePan),
  computed(() => props.enableZoom)
);
const {
  adjustedDomain: yDomain,
  isPanning: isPanningY,
  isZooming: isZoomingY
} = usePannedAndZoomedDomain(
  aggregateY,
  "y",
  reverseY,
  computed(() => wrapper.value?.$el),
  mouseState,
  computed(() => props.enablePan),
  computed(() => props.enableZoom)
);
const xScaler = useLinearScaler(xDomain, xScreenDomain, reverseX);
const yScaler = useLinearScaler(yDomain, yScreenDomain, reverseY);
const cartesianScaler = useCartesianScaler(xScaler, yScaler);

const { projectedDataSets } = useProjectedDataSets(
  processedDataSets,
  inactiveDataSets,
  cartesianScaler
);

const isPanning = computed(() => isPanningX.value || isPanningY.value);
const isZooming = computed(() => isZoomingX.value || isZoomingY.value);

const barWidth = computed(() => {
  const segments = isDiscreteDomain(xDomain.value)
    ? xDomain.value.max + 2
    : longestSet.value;

  const pxPerSegment = (layout.value?.plotWidth || 10) / segments;
  log({ segments, pxPerSegment });
  return clamp(pxPerSegment * 0.25, 1, 60);
});

const defaultTickFormatter = getDefaultTickFormatter();
const xTickFormatter: AxisTickFormatter = (
  val: Value,
  tickIndex?: number
): string => {
  if (isDiscreteDomain(xDomain.value) && typeof val === "number") {
    return xDomain.value.values[val];
  }
  return defaultTickFormatter(val, tickIndex);
};
const yTickFormatter: AxisTickFormatter = (
  val: Value,
  tickIndex?: number
): string => {
  if (isDiscreteDomain(yDomain.value) && typeof val === "number") {
    return yDomain.value.values[val];
  }
  return defaultTickFormatter(val, tickIndex);
};
</script>

<template>
  <DDV ref="wrapper" :layout="layout" :mouse-state="mouseState" class="scatter">
    <template #default="{ onUpdateNearestPoint, toolTipInfo }">
      <defs>
        <g id="point">
          <circle cx="0" cy="0" :r="scatterPointRadius" />
        </g>
        <clipPath
          v-if="layout && layout?.plotWidth > 0 && layout?.plotHeight > 0"
          :id="`plotClip-${chartId}`"
        >
          <rect
            :x="layout.margins.left"
            :y="layout.margins.top"
            :width="layout?.plotWidth"
            :height="layout?.plotHeight"
          />
        </clipPath>
      </defs>

      <!-- background slot -->
      <slot
        v-if="layout && layout.plotWidth > 0 && layout.plotHeight > 0"
        name="background"
        :layout="layout"
        :x-scaler="xScaler"
        :y-scaler="yScaler"
        :tool-tip-info="toolTipInfo as ToolTipInfo"
      >
        <rect
          class="plotBg"
          :class="{ isPanning, isZooming }"
          :x="layout.margins.left"
          :y="layout.margins.top"
          :width="layout?.plotWidth"
          :height="layout?.plotHeight"
        />
        <template v-if="xScaler && yScaler">
          <Axis
            v-if="drawXAxis && xScaler"
            :layout="layout"
            draw-grid-lines
            :label="
              processedDataSets?.length
                ? processedDataSets[0]?.xAxisLabel || null
                : null
            "
            :x="layout.margins.left"
            :y="layout.margins.top"
            :scaler="xScaler"
            :domain="xDomain"
            :plot-margin-px="6"
            :axis-tick-formatter="
              processedDataSets.length
                ? processedDataSets[0].xAxisFormatter || xTickFormatter
                : xTickFormatter
            "
            :tick-frequency="tickFrequencyX"
            :sub-ticks="subTicksX"
            @update:space="reserveSpace"
          />
          <Axis
            v-if="drawYAxis && yScaler"
            :layout="layout"
            draw-grid-lines
            :label="
              processedDataSets?.length
                ? processedDataSets[0]?.yAxisLabel || null
                : null
            "
            :x="layout.margins.left"
            :y="layout.margins.top"
            :scaler="yScaler"
            :domain="yDomain"
            :plot-margin-px="6"
            :axis-tick-formatter="
              processedDataSets.length
                ? processedDataSets[0].yAxisFormatter || yTickFormatter
                : yTickFormatter
            "
            is-vertical
            :tick-frequency="tickFrequencyY"
            :sub-ticks="subTicksY"
            @update:space="reserveSpace"
          />
        </template>
      </slot>

      <!-- plot area-->
      <g
        v-if="xScaler && yScaler && xDomain && yDomain"
        :style="{
          clipPath: `url(#plotClip-${chartId})`
        }"
      >
        <g
          :transform="`translate(${layout.margins.left} ${layout.margins.top})`"
        >
          <BSP
            :mouse-state="mouseState"
            :projected-data-sets="projectedDataSets"
            :ignore-x-values="mouseTrackingIgnoreX"
            :ignore-y-values="mouseTrackingIgnoreY"
            @update:nearest-point="
              (nearestPoints: NearestPointByDataSet | null) => {
                nearestPoints?.forEach(
                  (np: IndexSearchResult | null, dataSeriesNumber: number) => {
                    onUpdateNearestPoint(
                      projectedDataSets.at(dataSeriesNumber)?.title ||
                        `Series ${dataSeriesNumber}`,
                      dataSeriesNumber,
                      !inactiveDataSets.includes(dataSeriesNumber),
                      np,
                      undefined,
                      undefined,
                      dataSets.find((ds: DataSet) => !!ds.xAxisLabel)
                        ?.xAxisLabel,
                      dataSets.find((ds: DataSet) => !!ds.yAxisLabel)
                        ?.yAxisLabel
                    );
                  }
                );
              }
            "
          >
            <template #default="{ nearestPoints }">
              <!-- perSeries slot-->
              <g
                v-for="(ds, dataSeriesNumber) of projectedDataSets"
                :key="ds.dataSeriesNumber"
                :class="{ isPanning, isZooming }"
              >
                <slot
                  name="perSeries"
                  :layout="layout"
                  :mouse-state="mouseState"
                  :point-pairs="ds.pointPairs"
                  :nearest-point="nearestPoints[dataSeriesNumber]"
                  :x-scaler="xScaler"
                  :y-scaler="yScaler"
                  :inactive-data-sets="inactiveDataSets"
                  :data-series-number="dataSeriesNumber"
                  :aggregate-x-domain="xDomain"
                  :aggregate-y-domain="yDomain"
                  :is-panning="isPanning"
                  :is-zooming="isZooming"
                >
                  <Component
                    :is="
                      typeof value === 'number' ? ThresholdLine : ThresholdArea
                    "
                    v-for="({ label, value, color }, j) in dataSets[
                      dataSeriesNumber
                    ].thresholds?.x"
                    :key="j"
                    :layout="layout"
                    :is-x="true"
                    :scaler="xScaler"
                    :domain="xDomain"
                    :label="label"
                    :value="typeof value === 'number' ? value : null"
                    :min="
                      typeof value !== 'number'
                        ? reverseX
                          ? value.max
                          : value.min
                        : null
                    "
                    :max="
                      typeof value !== 'number'
                        ? reverseX
                          ? value.min
                          : value.max
                        : null
                    "
                    :color="color"
                    :nearest-points="nearestPoints"
                  />

                  <Component
                    :is="
                      typeof value === 'number' ? ThresholdLine : ThresholdArea
                    "
                    v-for="({ label, value, color }, j) in dataSets[
                      dataSeriesNumber
                    ]?.thresholds?.y"
                    :key="j"
                    :layout="layout"
                    :is-x="false"
                    :scaler="yScaler"
                    :domain="yDomain"
                    :label="label"
                    :value="typeof value === 'number' ? value : null"
                    :min="
                      typeof value !== 'number'
                        ? reverseY
                          ? value.max
                          : value.min
                        : null
                    "
                    :max="
                      typeof value !== 'number'
                        ? reverseY
                          ? value.min
                          : value.max
                        : null
                    "
                    :color="color"
                    :nearest-points="nearestPoints"
                    :pattern-thickness="2"
                  />
                  <LinePlot
                    v-if="drawCurve"
                    :is-zooming="isZooming"
                    :is-panning="isPanning"
                    :is-active="!inactiveDataSets.includes(dataSeriesNumber)"
                    :data-series-number="dataSeriesNumber"
                    :x="layout.margins.left"
                    :y="layout.margins.top"
                    :screen-points="
                      ds.pointPairs.map(
                        (pair: ProjectedPointPair) => pair.screenPoint
                      )
                    "
                    :curve-interpolator="
                      processedDataSets[dataSeriesNumber].curveType
                    "
                  />
                  <g
                    v-if="
                      drawPoints && !inactiveDataSets.includes(dataSeriesNumber)
                    "
                    :data-series-number="dataSeriesNumber"
                    class="scatter-points"
                  >
                    <template
                      v-for="(p, j) in ds.pointPairs.map(
                        (pair: ProjectedPointPair) => pair.screenPoint
                      )"
                    >
                      <use
                        v-if="isFinite(p.x) && isFinite(p.y)"
                        :key="j"
                        href="#point"
                        :x="p.x"
                        :y="p.y"
                      />
                    </template>
                  </g>
                  <g
                    v-if="
                      drawBars && !inactiveDataSets.includes(dataSeriesNumber)
                    "
                    class="bars"
                    :data-series-number="dataSeriesNumber"
                  >
                    <rect
                      v-for="(p, j) of ds.pointPairs.map(
                        (pair: ProjectedPointPair) => pair.screenPoint
                      )"
                      :key="j"
                      class="bar"
                      :class="{
                        isNegative:
                          valueToNumber(ds.pointPairs[j].dataPoint.y, yDomain) <
                          0
                      }"
                      :x="p.x - barWidth / 2"
                      :width="barWidth"
                      :y="
                        valueToNumber(ds.pointPairs[j].dataPoint.y, yDomain) < 0
                          ? yScaler.scale(0)
                          : p.y
                      "
                      :height="
                        valueToNumber(ds.pointPairs[j].dataPoint.y, yDomain) < 0
                          ? p.y - yScaler.scale(0)
                          : yScaler.scale(0) - p.y
                      "
                    />
                  </g>

                  <NearestPoint
                    v-if="
                      nearestPoints[dataSeriesNumber]?.node &&
                      (drawPoints || drawCurve)
                    "
                    :data-series-number="dataSeriesNumber"
                    :nearest-point="nearestPoints[dataSeriesNumber]"
                  />
                </slot>
              </g>
            </template>
          </BSP>
        </g>
      </g>

      <!-- legend slot-->
      <slot
        v-if="drawLegend"
        name="legend"
        :tool-tip-info="toolTipInfo"
        :inactive-data-sets="inactiveDataSets"
        :data-sets="processedDataSets as DataSet[]"
        :activate-series="(seriesId: number) => activateSeries(seriesId)"
        :deactivate-series="(seriesId: number) => deactivateSeries(seriesId)"
        :layout="layout"
        :mouse-state="mouseState"
      >
        <ChartLegend
          v-if="layout && mouseState"
          :layout="layout"
          :tool-tip-info="toolTipInfo"
          :model-value="inactiveDataSets"
          :data-sets="processedDataSets"
          @activate-series="activateSeries($event)"
          @deactivate-series="deactivateSeries($event)"
          @update:space="reserveSpace"
        />
      </slot>

      <!-- title slot-->
      <slot v-if="title && title.length" :title="title" :layout="layout">
        <ChartTitle
          v-if="layout && title && title.length"
          :title="title"
          :layout="layout"
          @update:space="reserveSpace"
        />
      </slot>

      <!-- crosshairs slot-->
      <slot v-if="mouseState?.isHovered" name="crosshairs" :layout="layout">
        <Crosshairs
          v-if="layout && mouseState"
          :layout="layout"
          :mouse-state="mouseState"
          :x="layout.margins.left"
          :y="layout.margins.top"
          :ignore-x="mouseTrackingIgnoreX"
          :ignore-y="mouseTrackingIgnoreY"
        />
      </slot>
    </template>

    <template #tooltip="{ toolTipInfo }">
      <slot name="tooltip" v-bind="{ chartId, toolTipInfo }"></slot>
    </template>
  </DDV>
</template>

<style lang="scss" scoped>
.scatter {
  .scatter-points {
    use {
      fill: none;
      stroke-width: 0.5;
      stroke: var(--activePlotColor, orange);
    }
  }
  .bar {
    stroke-width: 1;
    stroke: var(--activePlotColor, orange);
    fill: var(--activePlotColor, orange);
    &.isNegative {
      stroke: var(--plotColor1);
      fill: var(--plotColor1);
    }
  }
}
</style>
