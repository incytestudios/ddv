import { createApp } from "vue";
import devRouter from "./routes";

import App from "@/dev/App.vue";
const app = createApp(App);

app.use(devRouter);
app.config.performance = true;
app.mount("#app");
