import { createRouter, createWebHistory } from "vue-router";
import Home from "./views/Home.vue";
import DemoSwitcher from "./views/DemoSwitcher.vue";
import Playground from "./views/Playground.vue";

export const routes = [
  { path: "/", component: Home },
  { path: "/demo", component: DemoSwitcher },
  { path: "/pg", component: Playground }
];

export default createRouter({
  routes,
  history: createWebHistory()
});
