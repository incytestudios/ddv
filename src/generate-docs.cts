/* eslint-disable @typescript-eslint/ban-ts-comment */
import { resolve, join, relative, basename, dirname, parse } from "path";
import { mkdirSync, writeFileSync } from "fs";
import { sync } from "fast-glob";
import * as MarkdownIt from "markdown-it";
import type {
  ComponentMeta,
  PropertyMeta,
  MetaCheckerOptions,
  PropertyMetaSchema,
  SlotMeta,
  EventMeta
} from "vue-component-meta";
import { createChecker } from "vue-component-meta";
import * as ts from "typescript/lib/tsserverlibrary";
import * as tsm from "ts-morph";
//import { format, type Options as FormatOptions } from "prettier";
import strip = require("strip-comments");

const COMPONENT_ROOT = "../src/lib";
const COMPONENT_GLOB = "**/*.vue";
const COMPOSABLE_ROOT = "../src/lib/";
const COMPOSABLE_GLOB = "**/*.ts";
const OUTPUT_PATH = "../generated/";

const md = MarkdownIt();

export interface ComponentApiProps {
  name: ComponentMeta["props"][number]["name"];
  description: ComponentMeta["props"][number]["description"];

  // required: ComponentMeta['props'][number]['required']
  type: ComponentMeta["props"][number]["type"];
  default: ComponentMeta["props"][number]["default"];
}

export interface ComponentApi {
  props: ComponentApiProps[];
  events: ComponentMeta["events"];
  slots: ComponentMeta["slots"];
}

const checkerOptions: MetaCheckerOptions = {
  forceUseTs: true,
  printer: { newLine: 1 },
  schema: true,
  rawType: true
};

const tsconfigChecker = createChecker(
  resolve(__dirname, "../tsconfig.dev.json"),
  checkerOptions
);

const visitedTypes: { [key: string]: string } = {};

const visitType = (schema: PropertyMetaSchema, resolvedType: string) => {
  if (typeof schema === "string") {
    if (schema in visitedTypes) {
      return visitedTypes[schema];
    }
    visitedTypes[schema] = resolvedType;
  } else {
    if (schema.type in visitedTypes) {
      return visitedTypes[schema.type];
    }
    visitedTypes[schema.type] = resolvedType;
  }
  return resolvedType;
};

const arrayEqual = (a1: unknown[], a2: unknown[]): boolean =>
  a1.length === a2.length && a1.every((e, i) => e === a2[i]);

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const resolveSchemaToString = (schema: PropertyMetaSchema): string => {
  let type = "?";
  if (typeof schema === "string") {
    // console.log("schema is a string", schema);
    type = schema;
  } else {
    switch (schema.kind) {
      case "enum":
        if (schema.schema) {
          if (schema.schema.every(s => typeof s === "string")) {
            const sorted = schema.schema.sort((a, b) => (a < b ? -1 : 1));
            const filtered = sorted.filter(
              s => !["undefined", "null"].includes(s as string)
            );
            if (arrayEqual(filtered, ["false", "true"])) {
              type = "boolean";
            } else if (filtered.length === 1) {
              type = String(filtered[0]);
            } else {
              type = schema.schema
                .map(s => "`" + resolveSchemaToString(s) + "`")
                .join(" \\| ");
            }
          } else {
            const filtered = schema.schema.filter(
              s => !["undefined", "null"].includes(s as string)
            );
            type = filtered.reduce(
              (p, c): string => p + resolveSchemaToString(c),
              ""
            ) as string;
          }
        }
        break;
      case "array":
        // console.log("found an array type", schema.schema);
        if (schema.schema && typeof schema.schema === "string") {
          type = "`" + schema.schema + "`";
        } else {
          type = schema.schema?.reduce(
            (p: string, c): string => p + resolveSchemaToString(c),
            ""
          ) as string;
        }
        type = `${type}[]`;
        break;
      case "object":
        type = schema.type;
        break;
      case "event":
        // TODO: this only gives the signature as a string, hard to find in the typeIndex
        type = schema.type;
        // console.log("found an event", schema);
        break;
    }
  }
  // console.log("leaving as", type);
  const cachedType = visitType(schema, type);
  // console.log({ type, cachedType });
  return cachedType;
};

const lookupTypeString = (typeToFind: string): string | undefined => {
  // console.log("looking up type", typeToFind);
  const found =
    typeIndex[typeToFind] ||
    typeIndex[typeToFind.replace(/MaybeRef<(.*)[\[\]]?>/, "$1")] ||
    typeIndex[typeToFind.replace(/ComputedRef<(.*)[\[\]]?>/, "$1")] ||
    typeIndex[typeToFind.replace(/Ref<(.*)[\[\]]?>/, "$1")] ||
    typeIndex[typeToFind.replace(/MaybeRefOrGetter<(.*)[\[\]]?>/, "$1")] ||
    typeIndex[typeToFind.replace("[]", "")];
  return found && typeToFind.includes("[]")
    ? found.replace(/<code>(.*?)<\/code>/, "<code>$1[]</code>") // clumsily re-attach the `[]` to the link
    : found;
};

const getDocumentationPathForComponent = (componentPath: string): string => {
  const baseName = basename(componentPath).replace(/\.vue$/, ".md");
  const outputRoot = join(__dirname, OUTPUT_PATH);
  const subPath = relative(join(__dirname, COMPONENT_ROOT), componentPath);
  const docPath = join(dirname(join(outputRoot, subPath)), baseName);
  return docPath;
};
const getDocumentationPathForComposable = (composablePath: string): string => {
  const baseName = basename(composablePath).replace(/\.ts$/, ".md");
  const outputRoot = join(__dirname, OUTPUT_PATH);
  const subPath = relative(join(__dirname, COMPOSABLE_ROOT), composablePath);
  const docPath = join(dirname(join(outputRoot, subPath)), baseName);
  return docPath;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const markdownProp = (prop: PropertyMeta, isSlotProp = false): string => {
  const indexedType = lookupTypeString(prop.type);
  const resolvedType = resolveSchemaToString(prop.schema);
  let t = indexedType || resolvedType || "?";
  if (!t.startsWith("<")) {
    t = `<code>${t}</code>`;
  }
  // console.log({
  //   name: prop.name,
  //   typeString: indexedType,
  //   resolvedType,
  //   t,
  //   default: prop.default
  // });
  return `<tr><td><strong>${prop.name}</strong></td><td>${t}</td>${
    isSlotProp
      ? "" // slotProps arent' required or have defaults
      : prop.required
        ? '<td><span class="required">Required</span></td>'
        : `<td><code>${prop.default}</code></td>`
  }<td>${md.renderInline(prop.description)}</td></tr>`;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const buildPropDoc = (props: PropertyMeta[]): string => {
  const componentProps = props
    .filter(p => !p.global) // ignore stuff like `ref`, `class`
    .sort((a, b) => (a.name < b.name ? -1 : 1)); // a-z by name

  const requiredProps = componentProps.filter(p => p.required);
  const optionalProps = componentProps.filter(p => !p.required);

  if (componentProps.length > 0) {
    const required = requiredProps.map(p => markdownProp(p)).join("\n");
    const optional = optionalProps.map(p => markdownProp(p)).join("\n");
    return `## Props
<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Type</th>
      <th>Default</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    ${required}
    ${optional}
  </tbody>
</table>`;
  } else {
    return "";
  }
};

const buildSlotDoc = (slots: SlotMeta[]): string => {
  const orderedSlots = slots.sort((a, b) => (a.name < b.name ? -1 : 1));
  const out: string[] = ["## Slots AUTO"];
  if (orderedSlots.length > 0) {
    orderedSlots.reduce((p, slot) => {
      p.push(`\n<h3>${slot.name}</h3>\n<em>${slot.description}</em>\n`);
      // console.log("SLOT", slot.name);
      if (
        slot.schema &&
        typeof slot.schema !== "string" &&
        slot.schema.kind === "object"
      ) {
        if (typeof slot.schema.schema === "string") {
          p.push(md.renderInline(slot.schema.schema));
        } else if (slot.schema.schema) {
          p.push(
            '<table class="slot-props">\n<thead>\n<tr>\n<th>Name</th>\n<th>Type</th>\n<th>Description</th>\n</tr>\n</thead>\n'
          );
          Object.values(slot.schema.schema).forEach(v => {
            p.push(markdownProp(v, true));
          });
          p.push("</table>\n");
        }
      }
      return p;
    }, out);
  } else {
    out.push("None\n");
  }
  return out.join("\n");
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const buildEmitDoc = (events: EventMeta[]): string => {
  /* {
    name: 'update:space',
    description: '',
    tags: [],
    type: '[id: string, margins: Margins]',
    rawType: TypeObject {
      flags: 524288,
      checker: [Object],
      id: 9228,
      symbol: undefined,
      objectFlags: 524292,
      members: [Map],
      properties: [Array],
      callSignatures: [],
      constructSignatures: [],
      indexInfos: [Array],
      target: [TypeObject],
      resolvedTypeArguments: [Array]
    },
    signature: '(event: "update:space", id: string, margins: Margins): void',
    declarations: [Getter],
    schema: [Getter]
  }*/

  // console.log("building emit doc", events);
  return "## Emits AUTO\nEMIT AUTODOC";
};

const processComponent = (componentPath: string) => {
  const componentExportName = parse(componentPath).name;
  // if (!componentExportName.includes("BasicChart")) return;
  console.log("processing component", componentExportName);

  const meta = tsconfigChecker.getComponentMeta(componentPath);
  // console.log("got meta", meta);

  const propDocs = buildPropDoc(meta.props);
  const emitDocs = buildEmitDoc(meta.events);
  const slotDocs = buildSlotDoc(meta.slots);
  const out = [propDocs, slotDocs, emitDocs].join("\n\n");

  // console.log("writing", docPath);
  const docPath = getDocumentationPathForComponent(componentPath);
  writeFileSync(docPath, out, { encoding: "utf-8" });
};

const wrap = (s: string) =>
  s.replace(/(?![^\n]{1,65}$)([^\n]{1,65})\s/g, "$1\n");

const markdownTS = (moduleName: string, docs: DocEntry[]) => {
  const out: string[] = [];
  out.push(`# module ${moduleName}`);
  docs.forEach(d => {
    out.push(`\n## ${d.mdPrefix || ""} \`${d.name}\``);
    if (d.type && d.type !== "any") {
      if (d.originalSource) {
        const src = strip(d.originalSource, {
          preserveNewlines: false
        });
        out.push(`\`\`\`ts\n${src}\n\`\`\``);
      } else {
        out.push(`\`\`\`ts\n${wrap(d.type)}\n\`\`\``);
      }
    }
    if (d.documentation) {
      out.push(`\n${md.render(d.documentation)}\n`);
    }
    if (d.members) {
      d.members.forEach(m => {
        out.push(
          `- **${m.name}** \`${m.type}\` ${
            m.documentation ? md.renderInline(m.documentation) : ""
          }`
        );
      });
    }
    if (d.parameters) {
      out.push("\n### Parameters");
      d.parameters.forEach(p => {
        let typeStr = `\`${p.type}\``;
        typeStr = p.type ? lookupTypeString(p.type) || typeStr : typeStr;
        out.push(`- **${p.name}** ${typeStr}`);
        if (p.documentation) {
          out.push(`  ${md.render(p.documentation)}`);
        }
      });
    }
    if (d.returnType) {
      let typeStr = `\`${d.returnType}\``;
      typeStr = d.returnType
        ? lookupTypeString(d.returnType) || typeStr
        : typeStr;
      out.push(`### Returns ${typeStr}`);
    }
  });
  return out.join("\n");
};

interface DocEntry {
  mdPrefix?: string;
  name?: string;
  fileName?: string;
  documentation?: string;
  type?: string;
  originalSource?: string;
  members?: DocEntry[];
  constructors?: DocEntry[];
  parameters?: DocEntry[];
  returnType?: string;
  docFile: string;
}

const typeIndex: { [key: string]: string } = {};

const mdSlugify = (name: string): string =>
  name.trim().toLocaleLowerCase().replace(/\s+/g, "-");

// const buildMDUrl = (entry: DocEntry): string => {
//   let relativePath = relative(join(__dirname, OUTPUT_PATH), entry.docFile);
//   // console.log("buildMDUrl", { doc: entry.docFile, rel: relativePath });
//   if (!relativePath.includes("/")) {
//     relativePath = "internals/" + relativePath;
//   }
//   const sluggedAnchor = mdSlugify(
//     entry.mdPrefix ? `${entry.mdPrefix}-${entry.name}` : entry.name + ""
//   );
//   return `[\`${entry.name}\`](/${relativePath}#${sluggedAnchor})`;
// };

const buildHTMLUrl = (entry: DocEntry): string => {
  let relativePath = relative(join(__dirname, OUTPUT_PATH), entry.docFile);
  // console.log("buildHTMLUrl", { doc: entry.docFile, rel: relativePath });
  if (!relativePath.includes("/")) {
    relativePath = "ddv/internals/" + relativePath.replace(".md", ".html");
  }
  const sluggedAnchor = mdSlugify(
    entry.mdPrefix ? `${entry.mdPrefix}-${entry.name}` : entry.name + ""
  );
  return `<a href="/${relativePath}#${sluggedAnchor}"><code>${entry.name}</code></a>`;
};

const extractTypesFromFile = (
  sourceFile: tsm.SourceFile,
  checker: tsm.TypeChecker
): DocEntry[] => {
  const serializeSymbol = (symbol: tsm.Symbol): DocEntry => {
    const origSym = symbol.compilerSymbol;
    return {
      name: symbol.getName(),
      documentation: ts.displayPartsToString(
        origSym.getDocumentationComment(checker.compilerObject)
      ),
      type: checker.compilerObject.typeToString(
        checker.compilerObject.getTypeOfSymbolAtLocation(
          origSym,
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          origSym.valueDeclaration!
        )
      ),
      members: symbol.getMembers().map(serializeSymbol),
      docFile: p
    };
  };
  const serializeSignature = (signature: tsm.Signature): DocEntry => {
    return {
      mdPrefix: "function",
      parameters: signature.getParameters().map(serializeSymbol),
      returnType: checker.compilerObject.typeToString(
        signature.getReturnType().compilerType
      ), //checker.getTypeText(signature.getReturnType()),
      documentation: ts.displayPartsToString(
        signature.getDocumentationComments().map(p => p.compilerObject)
      ),
      docFile: p
    };
  };
  const serializeInterface = (i: tsm.InterfaceDeclaration): DocEntry => {
    const out: DocEntry = {
      mdPrefix: "interface",
      name: i.getName(),
      type: checker.getTypeText(i.getType(), i),
      docFile: p
    };
    const s = i.getSymbol();
    if (s) {
      const symbolEntry = serializeSymbol(s);
      out.documentation = symbolEntry.documentation;
    }
    const callSigs = i.getCallSignatures();
    if (callSigs) {
      out.members = callSigs.map(cs => serializeSignature(cs.getSignature()));
    }
    return out;
  };
  const serializeTypeAlias = (t: tsm.TypeAliasDeclaration): DocEntry => {
    return {
      mdPrefix: "type",
      name: t.getName(),
      type: checker.getTypeText(t.getType(), t),
      documentation: ts.displayPartsToString(
        t
          .getSymbolOrThrow() // this was already filtered for
          .compilerSymbol.getDocumentationComment(checker.compilerObject)
      ),
      members: t.getType().isUnion()
        ? []
        : t.getType().getProperties().map(serializeSymbol),
      docFile: p
    };
  };

  // console.log("<-- visiting", sourceFile.getFilePath());
  const output: DocEntry[] = [];
  const p = getDocumentationPathForComposable(sourceFile.getFilePath());
  sourceFile
    .getTypeAliases()
    .filter(ta => ta.isExported() && ta.getType() && ta.getSymbol())
    .forEach(ta => {
      const docEntry = serializeTypeAlias(ta);
      docEntry.originalSource = ta.getText(false);
      output.push(docEntry);
    });

  sourceFile
    .getInterfaces()
    .filter(i => i.isExported())
    .forEach(i => {
      const docEntry = serializeInterface(i);
      docEntry.originalSource = i.getText(false);
      output.push(docEntry);
    });

  sourceFile
    .getVariableDeclarations()
    .filter(vd => vd.isExported() && vd.getSymbol() !== undefined)
    .forEach(vd => {
      //console.log("found a var", v.getName());
      const sym = vd.getSymbolOrThrow();
      const tmp = serializeSymbol(sym);
      const init = vd.getInitializer();
      if (
        init &&
        (init.isKind(tsm.ts.SyntaxKind.ArrowFunction) ||
          init.isKind(tsm.ts.SyntaxKind.FunctionExpression))
      ) {
        const sig = init.getSignature();
        const docEntry: DocEntry = {
          ...tmp,
          ...serializeSignature(sig),
          ...{ docFile: p }
        };
        output.push(docEntry);
      } else {
        console.warn(
          "couldn't find an initializer for",
          vd.getName(),
          init?.getKindName()
        );
      }
    });

  sourceFile
    .getFunctions()
    .filter(f => f.isExported())
    .forEach(f => {
      const sym = f.getSymbol();
      const sig = f.getSignature();
      if (sym && sig) {
        const docEntry = {
          ...serializeSymbol(sym),
          ...serializeSignature(sig)
        };
        // console.log("🚨", { docEntry });
        output.push(docEntry);
      }
    });
  return output;
};

const useTSMorph = (filenames: string[]) => {
  const project = new tsm.Project({
    compilerOptions: {
      target: tsm.ts.ScriptTarget.ESNext,
      module: ts.ModuleKind.CommonJS,
      skipAddingFilesFromTsConfig: true
    }
  });
  const checker = project.getTypeChecker();
  filenames.forEach(fn => project.addSourceFileAtPath(fn));
  const sourceFileDocEntries: { [key: string]: DocEntry[] } = {};
  for (const sourceFile of project.getSourceFiles()) {
    const entries = extractTypesFromFile(sourceFile, checker);
    sourceFileDocEntries[sourceFile.getBaseName()] = entries;
    entries.forEach(de => {
      if (de.name && de.docFile) {
        typeIndex[de.name] = buildHTMLUrl(de);
      } else {
        console.warn("trying to make url for unnamed DocEntry", de);
      }
    });
  }
  // console.log({ typeIndex });
  Object.entries(sourceFileDocEntries).forEach(([baseName, entries]) => {
    if (entries.length) {
      writeFileSync(entries[0].docFile, markdownTS(baseName, entries));
    }
  });
};

const prepFolders = () => {
  const folders = ["charts", "components", "composables", "internals", "ticks"];
  folders.forEach(p =>
    mkdirSync(join(__dirname, OUTPUT_PATH, p), { recursive: true })
  );
};

const main = () => {
  prepFolders();
  // Collect components
  const components = sync([COMPONENT_GLOB], {
    cwd: resolve(__dirname, COMPONENT_ROOT),
    absolute: true
  });
  // Collect composables
  const composables = sync([COMPOSABLE_GLOB], {
    cwd: resolve(__dirname, COMPOSABLE_ROOT),
    absolute: true
  });

  useTSMorph(composables);
  console.log("processing", components.length, "vue components");
  components.forEach(processComponent);
};
main();
