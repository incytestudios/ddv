# Things DDV component does
- assign unique chart id (easy)
- wrap a dom element(easy)
- observe resizes of dom element(easy)
- compute element dimensions (easy)
- observe mouse state in element (REF_0) (easy)
- setup fullscreen enter/exit with methods (easy)
- manage space reservations of the chart (hard)
- compute margins based on space reservations (hard)
- handle tooltip plumbing (hard)

## new composables?

useContainer(domRef) => { width, height}
useSpaceReservations()
plotArea = usePlotArea(dimensions, spaceReservations)
