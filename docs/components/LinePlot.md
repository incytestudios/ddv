---
aside: false
---

<script setup>
import LinePlotDemo from "../../src/dev/views/demos/LinePlotDemo.vue";
</script>

# LinePlot

The `LinePlot` draws a single line through the provided points. To control how the line is generated change the `curveInterpolator` prop.

<LinePlotDemo />

<!--@include: ../../generated/components/LinePlot.md-->
