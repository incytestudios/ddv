---
aside: false
---

# BSP

The `BSP` is a renderless component that creates a Binary Space Parition of the provided points that can be used for quick
querying such as needed by tooltips.

If either of `props.ignoreXValues` or `props.ignoreYValues` is `true`, then BSP will fall back to a 1-dimensional [`BTree`](/internals/BTree#type-btree) over whichever axis isn't ignored. Then the default slot will
receive points that are near the mouse, ignoring the correct dimension.

If neither axis is ignored, then a 2-dimensional [`KDTree`](/internals/KDTree#type-kdtree) will be used.

See the [`LinePlot` docs](/components/LinePlot) for an example

<!--@include: ../../generated/components/BSP.md-->