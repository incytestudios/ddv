---
aside: false
---

<script setup>
import ThresholdLineDemo from "../../src/dev/views/demos/ThresholdLineDemo.vue";
</script>

# ThresholdLine

`ThresholdLine` is an visual chart component, useful to indicate an important value on a given domain. For example, combustion point, store closing time, etc...

<ThresholdLineDemo />

<!--@include: ../../generated/components/ThresholdLine.md-->
