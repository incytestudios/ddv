# SeriesToggler

`SeriesToggler` accepts an array of DataSets and builds structure for "deactivating" each series. This can mean
simply visually hiding a series, or removing the series from the aggregate domains (replotting on hide)

::: info
any series with no points will be automatically deactivated
:::

```html
...
<DDV>
  <template #default="{ domState }">
    <SeriesToggler v-if="domState?.plotWidth > 0">
      <template #default="{ inactiveDataSets }">
        <SeriesReducer
          :data-sets="dataSets"
          :inactive-data-sets="inactiveDataSets"
          :absolute-scale="true"
          :max-points="domState.plotWidth > 0 ? domState.plotWidth * 3 : 500"
        >
          ...
        </SeriesReducer>
      </template>
    </SeriesToggler>
  </template>
</DDV>
```

<!--@include: ../../generated/components/SeriesToggler.md-->

## Emits

| signal              | args       | comment                                        |
| ------------------- | ---------- | ---------------------------------------------- |
| `seriesActivated`   | `seriesId` | fired when an inactive series becomes active   |
| `seriesDeactivated` | `seriesId` | fired when an inactive series becomes inactive |

## Slots

### `default`

| prop               | type               | comment                                                          |
| ------------------ | ------------------ | ---------------------------------------------------------------- |
| `inactiveDataSets` | `number[]`         | a list of series indexes that are inactive                       |
| `activateSeries`   | `(number) => void` | a function that accepts a series index number and activates it   |
| `deactivateSeries` | `(number) => void` | a function that accepts a series index number and deactivates it |
| `toggleSeries`     | `(number) => void` | calls either activate or deactivate depending on current state   |
