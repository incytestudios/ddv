# SeriesReducer

`SeriesReducer` can be used either as a composable or a renderless component. It is responsible for discovering
the the minimum and maximum values of X and Y given several data sets. It also provides `processedDataSets` which
injects the aggregate domains onto the individual series. Generally you can just use `processedDataSets` directly as
they will have the orignal plot information with the added aggregate domain information.

```html
...
<SeriesReducer data-sets="...">
  <template #default="{processedDataSets}">
    // processedDataSets has your original data with calculated // aggregate
    domains as well as decimated points ...
  </template>
</SeriesReducer>
...
```

<!--@include: ../../generated/components/SeriesReducer.md-->

## Slots

### `default`

| prop                | type                                          | comment                                                                                                     |
| ------------------- | --------------------------------------------- | ----------------------------------------------------------------------------------------------------------- |
| `aggregateX`        | [`Domain`](/internals/domain#type-domain)     | the aggregate x domain for all data in `dataSets`                                                           |
| `aggregateY`        | [`Domain`](/internals/domain#type-domain)     | the aggregate y domain for all data in `dataSets`                                                           |
| `processedDataSets` | [`DataSet`](/internals/common#type-dataset)[] | The same dataSets as provided in the prop above but with their x and y domains set to the aggregate domains |
