# NearestPoint

`Nearestpoint` is an alternative point component, useful to indicate an active or important point

<!--@include: ../../generated/components/NearestPoint.md-->
