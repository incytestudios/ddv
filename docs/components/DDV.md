---
aside: false
---

<script setup>
import DDVDemo from "../../src/dev/views/demos/DDVDemo.vue";
</script>

# DDV

DDV represents the top-level component of every chart type. It sets up a container and an SVG for later drawing. It holds information about the wrapper element size and position, the mouse state, and space reserved in the element by child components.

<DDVDemo />

<!-- @include: ../../generated/components/DDV.md -->
