---
aside: false
---

<script setup lang="ts">
import { ref } from "vue";
import ScatterChart from "../../src/lib/charts/ScatterChart.vue";
import type { DataSet } from "../../src/lib/common.ts";

const randomPoints = count => Array.from({length: count}).map((_, i) => ({
  x: i,
  y: Math.random() * 20
}));

const dataSets:DataSet[] = [{
  points:randomPoints(20),
}]
</script>

# ToolTip

`ToolTip` is an DOM-based component, that draws contextual information based on mouse location. It is meant to render outside
of the main DDV framework and is meant to be customized via non-svg display elements.

<ScatterChart
  title="Tooltip Demo (hover over a point)"
  :minimum-height-px="160"
  draw-points
  draw-x-axis
  draw-y-axis
  enable-mouse-tracking
  :data-sets="dataSets"
/>

<!--@include: ../../generated/components/ToolTip.md-->

## Slots

None
