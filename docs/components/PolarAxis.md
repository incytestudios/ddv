---
aside: false
---

<script setup>
import PolarAxisDemo from "../../src/dev/views/demos/PolarAxisDemo.vue";
</script>

# PolarAxis

The `PolarAxis` component can draw tick marks, tick labels, the axis iteself, and grid lines for a polar represenation.

<PolarAxisDemo />

<!--@include: ../../generated/components/PolarAxis.md-->
