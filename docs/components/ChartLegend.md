---
aside: false
---

# ChartLegend

`ChartLegend` is a DOM-based component that draws an entry for each data-series. It can be used with `v-model` as a graphical series toggler when used in combination with [`SeriesToggler`](/components/SeriesToggler).

<!--@include: ../../generated/components/ChartLegend.md-->

## Slots

### `perSeries`

This slot will draw for every dataset, activated or not

| prop               | type                                                  | comment                                        |
| ------------------ | ----------------------------------------------------- | ---------------------------------------------- |
| `dataSet`          | [`DataSet`](/internals/common#type-dataset)           | The dataSet for this series                    |
| `dataSeriesNumber` | `number`                                              | The index in `dataSets` this series represents |
| `nearestPoint`     | [`NearestPoint`](/internals/common#type-nearestpoint) | nearest point for this series                  |
| `activateSeries`   | `() => void`                                          | A callback function to activate this series    |
| `deactivateSeries` | `() => void`                                          | A callback function to deactivate this series  |
| `seriesIsActive`   | `boolean`                                             | `true` is the series is active                 |

### `beforeDatasets`

This slot defaults to empty and is directly before the loop of `perSeries`

### `afterDatasets`

This slot defaults to empty and is directly after the loop of `perSeries`

### `beforeThresholds`

This slot defaults to empty and is directly before the loop of thresholds

### `afterThresholds`

This slot defaults to empty and is directly after the loop of thresholds
