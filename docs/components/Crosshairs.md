---
aside: false
---

<script setup>
import CrosshairsDemo from "../../src/dev/views/demos/CrosshairsDemo.vue";
</script>

# Crosshairs

`ChartTitle` is a simple component for drawing a chart-wide title

<CrosshairsDemo />

<!--@include: ../../generated/components/Crosshairs.md-->
