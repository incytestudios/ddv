---
aside: false
---

<script setup>
import AxisDemo from "../../src/dev/views/demos/AxisDemo.vue";
</script>

# Axis

The `Axis` component can draw tick marks, tick labels, the axis iteself, and grid lines on the left, bottom, right, or top of the chart.

<AxisDemo />

<!--@include: ../../generated/components/Axis.md-->