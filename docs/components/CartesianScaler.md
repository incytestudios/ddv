---
aside: false
---

# CartesianScaler

`CartesianScaler` can be used either as a composable or a renderless component. It is responsible for
for coming up with the mapping of x/y data values into screen coordinates. It also creates tick solutions for the
X and Y axis, so the Axis can place ticks in the properly interpolated screen poisition.

<!--@include: ../../generated/components/CartesianScaler.md-->

## Slots

### `default`

| prop         | type                                                  | comment                                                                                                                                                      |
| ------------ | ----------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `scalePoint` | `(p: Point) => Point`                                 | a scale method turning dataspace points into screenspace points. This is the `scale` method of a [`CartesianScaler`](/internals/common#type-cartesianscaler) |
| `scaleX`     | [`LinearScaler`](/internals/common#type-linearscaler) | a scale method turning dataspace X coords into screenspace X coords                                                                                          |
| `scaleY`     | [`LinearScaler`](/internals/common#type-linearscaler) | a scale method turning dataspace Y coords into screenspace Y coords                                                                                          |
