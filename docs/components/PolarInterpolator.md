# PolarInterpolator

`PolarInterpolator` can be used either as a composable or a renderless component. It is responsible for
for coming up with the mapping of x/y data values into screen coordinates where the origin is a pole.

<!--@include: ../../generated/components/PolarInterpolator.md-->

## Slots

### `default`

| prop              | type                                                        | comment                                                               |
| ----------------- | ----------------------------------------------------------- | --------------------------------------------------------------------- |
| `center`          | [`Point`](/internals/common#type-point)                     | center of the pole in screen space                                    |
| `radiusMin`       | `number`                                                    | notes                                                                 |
| `radiusMax`       | `number`                                                    | notes                                                                 |
| `perimeter`       | `number`                                                    | length of perimeter in pixels                                         |
| `perimeterRatio`  | `number`                                                    | `1` = full circle, `.25` = quarter circle                             |
| `angleScaler`     | [`LinearScaler`](/internals/common#type-linearscaler)       | scaler interface for turning screen coords into angles on the plot    |
| `magnitudeScaler` | [`LinearScaler`](/internals/common#type-linearscaler)       | scaler interface for turning screen coords into magnitude on the plot |
| `cartesianScaler` | [`CartesianScaler`](/internals/common#type-cartesianscaler) | scaler that scales data-space points into `{x:angle, y:magnitude}`    |
