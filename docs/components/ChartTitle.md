---
aside: false
---

# ChartTitle

`ChartTitle` is a simple component for drawing a chart-wide title

<!--@include: ../../generated/components/ChartTitle.md-->
