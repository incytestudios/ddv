---
aside: false
---

<script setup>
import ThresholdAreaDemo from "../../src/dev/views/demos/ThresholdAreaDemo.vue";
</script>

# ThresholdArea

`ThresholdArea` is an visual chart component, useful to indicate an important range on a given domain. For example, business hours vs non, or some arbitrary known range prior to plotting.

<ThresholdAreaDemo />

<!--@include: ../../generated/components/ThresholdArea.md-->

## Slots

None
