---
layout: home

hero:
  name: DDV
  text: Data Driven Vue
  tagline: Vue3 + Typescript responsive SVG plots, charts, & graphs
  image:
    src: /logo.svg
    alt: DDV Logo
  actions:
    - theme: brand
      text: Get Started
      link: /getting-started
    - theme: alt
      text: View on Gitlab
      link: https://gitlab.com/incytestudios/ddv
    - theme: alt
      text: View on NPM
      link: https://www.npmjs.com/package/@incytestudios/ddv
features:
  - icon: 🦾
    title: Responsive SVG Charts
    details: Use familiar devtools for debugging and authoring custom visualizations that are mobile and printer friendly
  - icon: 🔩
    title: Vue + Typescript Powered
    details: Quickly prototype new visualizations using ddv's simple typed interfaces. Avoiding canvas tags means easier interoperability with existing DOM & style items
  - icon: ⏱
    title: Performant
    details: Plot, index and query hundreds of thousands of points using a fast BSP index
---