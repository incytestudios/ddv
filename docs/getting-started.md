---
sidebarDepth: 2
---

# Getting Started

Before using ddv in your application it must be installed.

## Install via package manager

- **Step 1:** Install

  ```bash
  $ npm install --save @incytestudios/ddv
  ```

- **Step 2** CSS
  Both the theme and style CSS need to be added to your project. If you're using vite, just add
  the following to your entry point file

  ```ts
  import "@incytestudios/ddv/theme.css";
  import "@incytestudios/ddv/style.css";
  ```

  - `theme.css` contains overridable [CSS variables](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties)
  - `style.css` contains scoped CSS rules for internal components

- **Step. 2:** Import the component into your Vue component's JS/TS section

  ```html
  <template>
    ...
    <ScatterChart
      :data-sets="dataSets"
      draw-curve
      draw-legend
      draw-x-axis
      draw-y-axis
      enable-mouse-tracking
    />
    ...
  </template>

  <script setup lang="ts">
    import "@incytestudios/ddv/"
    import { ScatterChart } from "@incytestudios/ddv";
    ...
    const dataSets = [{
      title: "optional title",
      points: [{x:0, y:0}, {x:1, y:1}, {x:2, y:2}, {x:3, y:3}, {x:4, y:4}]
    }];
  </script>
  ```
