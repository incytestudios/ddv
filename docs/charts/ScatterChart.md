---
aside: false
---

<script setup lang="ts">
import ScatterChartDemo from "../../src/dev/views/demos/ScatterChartDemo.vue";
</script>

# ScatterChart

<ScatterChartDemo />

<!--@include: ../../generated/charts/ScatterChart.md--> 