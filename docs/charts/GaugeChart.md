---
aside: false
---

<script  setup>
import GaugeChartDemo from "../../src/dev/views/demos/GaugeChartDemo.vue";
</script>

# GaugeChart

<GaugeChartDemo />

<!--@include: ../../generated/charts/GaugeChart.md-->

