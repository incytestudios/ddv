---
aside: false
---
<script  setup>
import BasicChartDemo from "../../src/dev/views/demos/BasicChartDemo.vue";
</script>

# BasicChart

<BasicChartDemo />

<!--@include: ../../generated/charts/BasicChart.md-->
