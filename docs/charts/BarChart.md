---
aside: false
---

<script  setup>
import BarChartDemo from "../../src/dev/views/demos/BarChartDemo.vue";
</script>

# BarChart

<BarChartDemo />

<!--@include: ../../generated/charts/BarChart.md-->
