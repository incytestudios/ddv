---
aside: false
---

<script setup lang="ts">
import PolarChartDemo from "../../src/dev/views/demos/PolarChartDemo.vue";
</script>

# PolarChart

<PolarChartDemo />

<!--@include: ../../generated/charts/PolarChart.md-->