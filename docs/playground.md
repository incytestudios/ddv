---
aside: false
---
<script setup>
import Playground from "../src/dev/views/Playground.vue";
</script>

<style lang="scss">
@import "../src/assets/theme.css";
</style>

# DDV Playground
<Playground />
