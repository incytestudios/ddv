# Changelog

## v0.7.8 (2023-11-12)
- Fix bug with PolarAxis unexpected text being printed on top of chart

## v0.7.7 (2023-10-19)
- Bump vue version to `3.3.4`

## v0.7.6 (2023-08-04)
- Add a `tooltip` slot to the ScatterChart that will proxy the DDV component's slot of the same name.

## v0.7.5 (2023-02-6)
- Updates for ScatterChart and Axis to be more defensive of DOM state before drawing (negative coordinates in console errors)

## v0.7.4 (2023-02-1)
- Fix issue where scroll wheel events were captured when zooming was disabled
- Remove animation on NearestPoint.vue

## v0.7.3 (2023-02-1)
- Fix issue with Axis clip paths that prevented ticks from appearing on right aligned axes
- Fix ChartTitle component from reserving space under wrong id

## v0.7.0 (2023-01-26)
Finally got around to a CHANGELOG

- Introduced [DateDomain](/internals/domain.html#type-datedomain) and [DiscreteDomain](/internals/domain.html#interface-discretedomain)
- Adapted [useLinearScaler](/composables/useLinearScaler#function-uselinearscaler) to be able to scale/project all 3 domain types
- Fully implemented legiblility scoring and density controls for both [Axis](/components/Axis) and [PolarAxis](/components/PolarAxis)
- composable introduced for maintining a domain during panning/zooming
- finally styled the dev/playground components (they also get used in prop dashboards in docs)
- refactored tick solvers

## v0.0.2 - v.0.6.2
Did a poor job of maintaining a changelog. Learned an awful lot about domains, projections and geometry. The first few versions were mainly API experiments to see what felt comfortable. 

By "comfortable" I mean developer experience, but I wanted to feel like I could "sculpt" data using very simple machines, and vue provided all the tools necessary to make very nice looking reactive SVG visuals. The interface matured to the point of feeling like maintaining a changelog was a good idea, so here we are. 

## v0.0.1 (2022-02-16)
First release, basic scatterchart sort of working