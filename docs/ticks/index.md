# Ticks

This module controls how a [Axis](/components/Axis) and [PolarAxis](/components/PolarAxis) choose how to draw tick marks.

<!--@include: ../../generated/ticks/index.md -->