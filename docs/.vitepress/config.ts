import { defineConfig } from "vitepress";
import pkg from "../../package.json";

const { DOCS_PORT = 3001 } = process.env;

export default defineConfig({
  title: "DDV " + pkg.version,
  description:
    "Data Driven Vue: Vue3+ TS responsive SVG plots, charts, & graphs",
  base: "/ddv/",
  srcDir: "./",
  outDir: "../generated/staticdocs",
  markdown: {
    toc: {
      level: [1, 2, 3],
    },
  },
  themeConfig: {
    nav: [
      { text: "Home", link: "/" },
      { text: "Docs", link: "/getting-started" },
      { text: "Gitlab", link: "https://gitlab.com/incytestudios/ddv" },
    ],
    sidebar: [
      {
        text: "The Basics",
        items: [
          { text: "Home", link: "/" },
          { text: "CHANGELOG", link: '/changelog'},
          { text: "Getting Started", link: "/getting-started" },
          { text: "Playground", link: "/playground" },
        ],
      },
      {
        text: "Charts",
        collapsible: true,
        items: [
          { text: "BasicChart", link: "/charts/BasicChart" },
          { text: "BarChart", link: "/charts/BarChart" },
          { text: "GaugeChart", link: "/charts/GaugeChart" },
          { text: "PolarChart", link: "/charts/PolarChart" },
          { text: "ScatterChart", link: "/charts/ScatterChart" },
        ],
      },
      {
        text: "Components",
        collapsible: true,
        items: [
          { text: "Axis", link: "/components/Axis" },
          { text: "BSP", link: "/components/BSP" },
          {
            text: "CartesianScaler",
            link: "/components/CartesianScaler",
          },
          { text: "ChartLegend", link: "/components/ChartLegend" },
          { text: "ChartTitle", link: "/components/ChartTitle" },
          { text: "Crosshairs", link: "/components/Crosshairs" },
          { text: "DDV", link: "/components/DDV" },
          { text: "LinePlot", link: "/components/LinePlot" },
          { text: "NearestPoint", link: "/components/NearestPoint" },
          { text: "PolarAxis", link: "/components/PolarAxis" },
          {
            text: "PolarInterpolator",
            link: "/components/PolarInterpolator",
          },
          { text: "SeriesReducer", link: "/components/SeriesReducer" },
          { text: "SeriesToggler", link: "/components/SeriesToggler" },
          { text: "ThresholdArea", link: "/components/ThresholdArea" },
          { text: "ThresholdLine", link: "/components/ThresholdLine" },
          { text: "ToolTip", link: "/components/ToolTip" },
        ],
      },
      {
        text: "Composables",
        items: [
          {text: "useAggregateDomains", link: "/composables/useAggregateDomains"},
          {text: "useArrayToggler", link: "/composables/useArrayToggler"},
          {text: "useBSP", link: "/composables/useBSP"},
          {text: "useCartesianScaler", link: "/composables/useCartesianScaler"},
          {text: "useChart", link:"/composables/useChart"},
          {text: "useDownSampler", link: "/composables/useDownSampler"},
          {text: "useLinearScaler", link: "/composables/useLinearScaler"},
          {text: "useMouseState", link: "/composables/useMouseState"},
          {text: "usePannedAndZoomedDomain", link: "/composables/usePannedAndZoomedDomain"},
          {text: "usePolarInterpolator", link: "/composables/usePolarInterpolator"},
          {text: "useProjectedDataSets", link: "/composables/useProjectedDataSets"},
          {text: "useScreenPoints", link: "/composables/useScreenPoints"},
          {text: "useSpaceReservations", link:"/composables/useSpaceReservations"},
          {text: "useStackedPoints", link: "/composables/useStackedPoints"},
          {text: "useTicks", link: "/composables/useTicks"},
        ]
      },
      {
        text: "Internals",
        items: [
          {text: "BTree", link: "/internals/BTree"},
          {text: "catmullRom", link: "/internals/catmullRom"},
          {text: "common", link: "/internals/common"},
          {text: "curveInterpolators", link: "/internals/curveInterpolators"},
          {text: "domain", link: "/internals/domain"},
          {text: "formatting", link: "/internals/formatting"},
          {text: "KDTree", link: "/internals/KDTree"},
          {text: "logger", link: "/internals/logger"},
          {text: "nicenum", link: "/internals/nicenum"},
          {text: "ticks", link: "/ticks/"},
          {text: "utils", link: "/internals/utils"},
        ]
      }
    ],
    footer: {
      message: "Released under the MIT License.",
      copyright: "Copyright © 2022-present Incyte Studios, LLC",
    },
  },
  vite: {
    server: {
      host: '0.0.0.0',
      port: Number(DOCS_PORT)
    }
  }
});
