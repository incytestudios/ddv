import DefaultTheme from "vitepress/theme";
import CustomLayout from "./CustomLayout.vue";
import HomeChart from "../../HomeChart.vue";
import "../../../src/assets/theme.css";

export default {
  ...DefaultTheme,
  enhanceApp({ app }) {
    app.component('HomeChart', HomeChart);
    console.log("overriden theme");
  },
  Layout: CustomLayout
}