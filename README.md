<h1 align="center">@incytestudios&#x2F;ddv</h1>

<p align="center">
  <img alt="build status" src="https://img.shields.io/gitlab/pipeline-status/incytestudios/ddv?branch=master">
</p>

<img src="docs/public/logo.svg" width="320" />

<strong>See docs site on [gitlab](https://incytestudios.gitlab.io/ddv)</strong>

## About ##

ddv is Data Driven Vue, a similar approach to plotting as d3 but using vue3 and typescript for DOM manipulation. ddv provides
premade commonly used chart types as well as the compositional tools to make horrors beyond human comprehension.

## Features ##

- :heavy_check_mark: Renders to SVG
- :heavy_check_mark: Multiple data series (plots) per graph
- :heavy_check_mark: Easy to style with CSS variables
- :heavy_check_mark: Responsive charts
- :heavy_check_mark: Indexed (fast) Mouse-over and tooltip support
- :heavy_check_mark: Printer friendly
- :heavy_check_mark: Documented

## Technologies ##

The following tools were used in this project:

- [Node.js](https://nodejs.org/en/)
- [TypeScript](https://www.typescriptlang.org/)
- [Vue.js](https://vuejs.org/)

## License ##

This project is under license from MIT. For more details, see the [LICENSE](LICENSE.md) file.


## Author ## 
Created by <a href="https://incytestudios.com" target="_blank">Incyte Studios</a>
