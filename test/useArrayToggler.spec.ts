import { useArrayToggler } from "../src/lib/composables/useArrayToggler";

describe("useArrayToggler", () => {
  test("inactiveItems starts empty", () => {
    const { inactiveItems } = useArrayToggler();
    expect(inactiveItems.value.length).toBe(0);
  });

  test("isActive with empty set", () => {
    const { isActive } = useArrayToggler();
    expect(isActive(0)).toBeFalsy;
  });

  test("deactivate(id) adds it to inactiveItems", () => {
    const { inactiveItems, deactivate, isActive } = useArrayToggler();
    deactivate(0);
    expect(inactiveItems.value).toHaveLength(1);
    expect(isActive(0)).toBeTruthy;
  });

  test("activate(id) removes it from inactiveItems", () => {
    const { inactiveItems, activate, deactivate, isActive } = useArrayToggler();
    deactivate(0);
    expect(inactiveItems.value).toHaveLength(1);
    expect(isActive(0)).toBeFalsy;
    activate(0);
    expect(isActive(0)).toBeTruthy;
  });

  test("inactiveItems are scoped per usage", () => {
    const { inactiveItems: A, deactivate } = useArrayToggler();
    const { inactiveItems: B, isActive } = useArrayToggler();
    deactivate(0);
    expect(A.value).not.toEqual(B.value);
    expect(isActive(0)).toBeFalsy;
  });
});
