import type { DataSet, Layout } from "@/lib/common";
import { useStackedPoints } from "@/lib/composables/useStackedPoints";
import { useLogger } from "@/lib/logger";
import {
  fromRange,
  useAggregateDomains,
  useCartesianScaler,
  useLinearScaler
} from "@/lib/main";

const log = useLogger("useAggregateDomains.spec");

// shared simple layout
const layout: Layout = {
  elementWidth: 800,
  elementHeight: 600,
  computedHeight: 600,
  plotWidth: 800,
  plotHeight: 600,
  margins: { top: 0, right: 0, bottom: 0, left: 0 }
};

const discreteSets: DataSet[] = [
  {
    points: [
      { x: "A", y: 0 },
      { x: "B", y: 5 }
    ]
  },
  {
    points: [
      { x: "A", y: 0 },
      { x: "B", y: 5 }
    ]
  },
  {
    points: [
      { x: "C", y: 5 }, // "C" is absent from the other sets, and out of order
      { x: "A", y: 0 },
      { x: "B", y: 5 }
    ]
  }
];

describe("useStackedPoints", () => {
  /**
   * Test stacking points that share X values from a potentially different DataSet
   */
  const { aggregateX, aggregateY } = useAggregateDomains(
    discreteSets,
    [],
    true, // must be on for projected bars to hit the 0 line
    true // must be on to test actual stacking
  );
  const xScaler = useLinearScaler(
    aggregateX,
    fromRange(layout.elementWidth),
    false
  );
  const yScaler = useLinearScaler(
    aggregateY,
    fromRange(layout.computedHeight),
    false
  );
  const cartesianScaler = useCartesianScaler(xScaler, yScaler);

  const { yValuesByX, stackedScreenPointsByXValue } = useStackedPoints(
    discreteSets,
    [],
    cartesianScaler
  );

  test("yValuesByX has all unique keys from all sets", () => {
    log("aggregate x:", aggregateX.value, "y:", aggregateY.value);
    log({
      yValuesByX: yValuesByX.value,
      stackedScreenPointsByXValue: stackedScreenPointsByXValue.value
    });
    expect(Array.from(yValuesByX.value.keys())).toEqual(["A", "B", "C"]);
  });

  test("yValuesByX has cross-series points lined up on X", () => {
    expect(yValuesByX.value.get("A")?.length).toEqual(3);
    expect(yValuesByX.value.get("B")?.length).toEqual(3);
    expect(yValuesByX.value.get("C")?.length).toEqual(1);
  });

  test("values are stacked in screen space", () => {
    // const foo = [
    //   [
    //     { x: 0, y: 0 },
    //     { x: 0, y: 0 },
    //     { x: 0, y: 0 }
    //   ],
    //   [
    //     { x: 400, y: 200 },
    //     { x: 400, y: 400 },
    //     { x: 400, y: 600 }
    //   ],
    //   [{ x: 800, y: 200 }]
    // ];

    const ssp = stackedScreenPointsByXValue.value;
    log(ssp);
    expect(Array.from(ssp.entries()).length).toBe(3);
    const [pointsA, pointsB, pointsC] = ssp.values();
    expect(pointsA.every(p => p.x === 0));
    expect(pointsB.every(p => p.x === 400));
    expect(pointsC.every(p => p.x === 800));
    expect(pointsA.length).toBe(3);
    expect(pointsB.length).toBe(3);
    expect(pointsC.length).toBe(1);
    expect(pointsB[0].y).toBe(200);
    expect(pointsB[1].y).toBe(400);
    expect(pointsB[2].y).toBe(600);
  });
});
