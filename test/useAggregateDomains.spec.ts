import { isDateDomain } from "./../src/lib/domain";
import type { DataSet } from "@/lib/common";
import {
  useAggregateDomains,
  ErrorDatasetDomainMismatch
} from "@/lib/composables/useAggregateDomains";
import { isDiscreteDomain } from "@/lib/domain";
import { useLogger } from "@/lib/logger";

const log = useLogger("useAggregateDomains.spec");

describe("useAggregateDomains", () => {
  const emptyDataSets: DataSet[] = [];
  const singleEmptyDataSet: DataSet[] = [{ points: [] }];
  const overlappingPoints: DataSet[] = [
    {
      points: Array.from({ length: 10 }).map((_, i) => ({
        x: i + 1,
        y: i + 1
      }))
    },
    {
      points: Array.from({ length: 10 }).map((_, i) => ({
        x: (i + 1) * 2,
        y: (i + 1) * 2
      }))
    }
  ];
  const discreteSets: DataSet[] = [
    {
      points: [
        { x: "A", y: 0 },
        { x: "B", y: 5 }
      ]
    }
  ];
  const mixedSets: DataSet[] = [...discreteSets, ...overlappingPoints];

  test("empty sets", () => {
    const { aggregateX, aggregateY } = useAggregateDomains(
      emptyDataSets,
      [],
      true,
      false
    );
    log({ x: aggregateX.value, y: aggregateY.value });
    expect(aggregateX.value.min).toBe(0);
    expect(aggregateX.value.max).toBe(0);
    expect(aggregateY.value.min).toBe(0);
    expect(aggregateY.value.max).toBe(0);
  });

  test("set with empty points", () => {
    const { aggregateX, aggregateY } = useAggregateDomains(
      singleEmptyDataSet,
      [],
      true,
      false
    );
    log({ x: aggregateX.value, y: aggregateY.value });
    expect(aggregateX.value.min).toBe(0);
    expect(aggregateX.value.max).toBe(0);
    expect(aggregateY.value.min).toBe(0);
    expect(aggregateY.value.max).toBe(0);
  });

  test("overlapping datasets", () => {
    const { aggregateX, aggregateY } = useAggregateDomains(
      overlappingPoints,
      [],
      true,
      false
    );
    log({ x: aggregateX.value, y: aggregateY.value });
    expect(aggregateX.value.min).toBe(0);
    expect(aggregateX.value.max).toBe(20);
    expect(aggregateY.value.min).toBe(0);
    expect(aggregateY.value.max).toBe(20);
  });

  /**
   * this is a test of some kind
   */
  test("discrete sets", () => {
    const { aggregateX, aggregateY } = useAggregateDomains(
      discreteSets,
      [],
      true,
      false
    );
    log({ x: aggregateX.value, y: aggregateY.value });
    expect(aggregateX.value.min).toBe(0);
    expect(aggregateX.value.max).toBe(1);
    expect(aggregateY.value.max).toBe(5);
    expect(
      isDiscreteDomain(aggregateX.value) && aggregateX.value.values
    ).toEqual(["A", "B"]);
  });

  test("date sets", () => {
    const smallestDate = new Date(Date.UTC(2023, 0, 1));
    const inbetweenDate = new Date(Date.UTC(2023, 0, 10));
    const largestDate = new Date(Date.UTC(2023, 5, 10));
    const dateSets: DataSet[] = [
      {
        points: [
          { x: smallestDate, y: 0 },
          { x: inbetweenDate, y: 1 }
        ]
      },
      {
        points: [{ x: largestDate, y: 2 }]
      }
    ];
    const { aggregateX, aggregateY } = useAggregateDomains(
      dateSets,
      [],
      false,
      false
    );
    log({ x: aggregateX.value, y: aggregateY.value });
    expect(isDateDomain(aggregateX.value) && aggregateX.value.min).toEqual(
      smallestDate
    );
    expect(isDateDomain(aggregateX.value) && aggregateX.value.max).toEqual(
      largestDate
    );
    expect(isDateDomain(aggregateX.value)).toBe(true);
  });

  test("mixed set types", () => {
    const doIt = () => {
      useAggregateDomains(mixedSets, [], false, false).aggregateX.value;
    };
    expect(doIt).toThrow(ErrorDatasetDomainMismatch);
  });
});
