import { useLogger } from "@/lib/logger";
import { useLinearScaler } from "@/lib/main";
import {
  type NumericDomain,
  type DateDomain,
  fromValues,
  getRange
} from "@/lib/domain";
import { add, startOfMonth } from "date-fns";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("useLinearScaler.spec");

describe("useLinearScaler", () => {
  const screenWidth = 600;
  const _source: NumericDomain = { min: 0, max: 120 };
  const _target: NumericDomain = { min: 0, max: screenWidth };
  const reverse = false;

  const { scale, normalize, interpolate } = useLinearScaler(
    _source,
    _target,
    reverse
  );

  test("internals", () => {
    if (normalize) {
      const norm = normalize(0);
      expect(norm).toBeGreaterThanOrEqual(0);
      expect(norm).toBeLessThanOrEqual(1);
    }
    if (normalize && interpolate) {
      const i = interpolate(normalize(_source.min));
      expect(i).toBe(_target.min);
      const i2 = interpolate(normalize(_source.max));
      expect(i2).toBe(_target.max);
      const i3 = interpolate(normalize(getRange(_source) / 2));
      expect(i3).toBe(getRange(_target) / 2);
    }
  });

  test("basic scaling", () => {
    expect(scale(_source.min)).toBe(_target.min);
    expect(scale(getRange(_source) / 2)).toBe(getRange(_target) / 2);
    expect(scale(_source.max)).toBe(_target.max);
  });

  test("crosses axis", () => {
    const src: NumericDomain = { min: -10, max: 10 };
    const s = useLinearScaler(src, _target, false);
    expect(s.scale(0)).toBe(getRange(_target) / 2);
  });

  test("dateDomain", () => {
    const src: DateDomain = {
      min: startOfMonth(new Date(2024, 0, 1)),
      max: add(startOfMonth(new Date(2024, 0, 1)), { days: 1 })
    };
    console.log("dateDomain", src);
    const s = useLinearScaler(src, _target, false);
    expect(s.scale(src.min)).toBe(_target.min);
    expect(s.scale(new Date(2024, 0, 1, 12))).toBe(getRange(_target) / 2);
    expect(s.scale(src.max)).toBe(_target.max);
  });

  test("discrete domain", () => {
    const src = fromValues(["A", "B", "C"]);
    const s = useLinearScaler(src, _target, false);
    expect(s.scale("A")).toBe(_target.min);
    expect(s.scale("C")).toBe(_target.max);
  });
});
