import { useLinearScaler } from "@/lib/main";
import { solveTicksDateDomain } from "../src/lib/ticks";
import { useLogger } from "@/lib/logger";
import { niceNum } from "@/lib/nicenum";
import { solveTicks, solveTicksExtended } from "../src/lib/ticks";
import { getDefaultTickFormatter } from "../src/lib/formatting";
import { fromRange } from "@/lib/domain";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = useLogger("ticks.spec");

const measuredFontSizes: [string, number][] = [
  ["10px", 10],
  ["20px", 20]
];

describe("ticks", () => {
  const domain = { min: 0, max: 10 };
  const screenWidth = 300;
  const desiredTicks = 5;
  const density = desiredTicks / screenWidth;
  const isVertical = false;
  const scaler = useLinearScaler(domain, fromRange(screenWidth), false);

  test("0-10", () => {
    const tickSolution = solveTicks(
      screenWidth,
      density,
      domain,
      isVertical,
      getDefaultTickFormatter(),
      scaler,
      measuredFontSizes
    );

    // it makes 2 more than it needs
    expect(tickSolution.ticks?.length).toBe(desiredTicks + 2);
  });

  test("Zero Domain", () => {
    const tickSolution = solveTicks(
      screenWidth,
      density,
      { min: 0, max: 0 },
      false,
      getDefaultTickFormatter(),
      scaler,
      measuredFontSizes
    );
    expect(tickSolution.ticks?.length).toBe(0);
  });

  test("Extended variable screen size", () => {
    const domain = { min: 0, max: 100 };

    for (let i = 50; i < 5000; i += 50) {
      const tickSolution = solveTicksExtended(
        i,
        density,
        domain,
        scaler,
        false,
        getDefaultTickFormatter(),
        measuredFontSizes
      );

      expect(tickSolution?.ticks).toBeDefined();
      expect(tickSolution?.niceDomain.min).toBeLessThanOrEqual(domain.min);
      expect(tickSolution?.niceDomain.max).toBeGreaterThanOrEqual(domain.max);
    }
  });

  test("Extended variable domain size", () => {
    for (let i = 1; i < 5000; i += 50) {
      const domain = { min: -i, max: i };
      const tickSolution = solveTicksExtended(
        screenWidth,
        density,
        domain,
        scaler,
        false,
        getDefaultTickFormatter(),
        measuredFontSizes
      );

      expect(tickSolution?.ticks).toBeDefined();
      expect(tickSolution?.niceDomain.min).toBeLessThanOrEqual(domain.min);
      expect(tickSolution?.niceDomain.max).toBeGreaterThanOrEqual(domain.max);
    }
  });
});

describe("niceNum", () => {
  test("niceNum no rounding", () => {
    expect(niceNum(1, false)).toBe(1);
    expect(niceNum(2.7, false)).toBe(5);
  });

  test("niceNum with rounding", () => {
    expect(niceNum(1, false)).toBe(1);
    expect(niceNum(2.7, true)).toBe(2);
  });
});

describe("solveTicksDateDomain", () => {
  test("5 year period", () => {
    const d = { min: new Date(2017, 1, 1), max: new Date(2022, 1, 1) };
    const scaler = useLinearScaler(d, { min: 0, max: 500 }, false);
    const s = solveTicksDateDomain(500, d, scaler, measuredFontSizes);
    console.log(s);
    expect(s.ticks).toBeDefined();
    if (s && s.ticks && s.ticks?.length > 2) {
      expect(s.ticks.shift()?.label).toEqual("2017");
      expect(s.ticks.pop()?.label).toEqual("2022");
    }
  });
});
