import { usePolarInterpolator } from "@/lib/main";
import { fromRange, getRange } from "@/lib/domain";

describe("usePolarInterpolator", () => {
  const screenDomain = { x: 500, y: 250 };
  const angleDomain = fromRange(100);
  const magnitudeDomain = fromRange(100);

  test("full circle", () => {
    const p = usePolarInterpolator(
      screenDomain,
      angleDomain,
      magnitudeDomain,
      0,
      1,
      1
    );
    expect(p.value.radiusMin).toBe(0);
    expect(p.value.angleScaler.scale(angleDomain.min)).toBe(180);
    expect(p.value.angleScaler.scale(getRange(angleDomain) / 2)).toBe(0);
    expect(p.value.angleScaler.scale(angleDomain.max)).toBe(180);
  });
});
